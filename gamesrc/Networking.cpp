//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"

/*
AppFramework::Networking(void)
{
	m_bIsConnected = false;
}


AppFramework::~Networking(void)
{
	if(m_bIsConnected)
		closesocket(conn);
}

*/
int extractInt(char** msg)
{
    int* value;
    value = (int*) *msg;
    *msg += sizeof(int);
    return *value;
}

uint8_t extractUnsignedInt8(char** msg)
{
    uint8_t* value;
    value = (uint8_t*) *msg;
    *msg += sizeof(uint8_t);
    return *value;
}

uint16_t extractUnsignedInt16(char** msg)
{
    uint16_t* value;
    value = (uint16_t*) *msg;
    *msg += sizeof(uint16_t);
    return *value;
}

int8_t extractInt8(char** msg)
{
    int8_t* value;
    value = (int8_t*) *msg;
    *msg += sizeof(int8_t);
    return *value;
}

int16_t extractInt16(char** msg)
{
    int16_t* value;
    value = (int16_t*) *msg;
    *msg += sizeof(int16_t);
    return *value;
}

void AppFramework::Init(string sIpAddress, int iPort)
{

	m_bIsConnected = false;

	m_sServerIPAddress = sIpAddress;
	m_iServerPort = iPort;
	struct hostent *hp;
	unsigned int addr;
	struct sockaddr_in server;
	

	WSADATA wsaData;

	int wsaret=WSAStartup(0x101,&wsaData);


	if(wsaret!=0)
	{
		return;
	}

	conn=socket(AF_INET,SOCK_STREAM,0);
	if(conn==INVALID_SOCKET)
		return;

	addr=inet_addr(m_sServerIPAddress.c_str());
	hp=gethostbyaddr((char*)&addr,sizeof(addr),AF_INET);
	
	if(hp==NULL)
	{
		closesocket(conn);
		return;
	}

	server.sin_addr.s_addr=*((unsigned long*)hp->h_addr);
	server.sin_family=AF_INET;
	server.sin_port=htons(m_iServerPort);
	if(connect(conn,(struct sockaddr*)&server,sizeof(server)))
	{
		closesocket(conn);
		return;	
	}
	m_bIsConnected = true;
	return;
}

int AppFramework::SendMessagePort( PortPacket p )
{
	int nBytesSent;
	//cout << static_cast<int>(p.msg);
	switch( p.msg )
	{
		case 0:
		{
			ClientPacketAttemptLogin cpacket;

			cpacket.msg = p.msg;

			strcpy( cpacket.username, loginbox->getText().c_str() );

			nBytesSent = send( conn, (const char*)&cpacket, ( sizeof(uint8_t) + sizeof(char)*20 ), 0);
		}break;

		case SEND_VELOCITY:
		{
			casting = false;

			ClientPacketMovement cpacket;

			cpacket.msg = SEND_VELOCITY;
			cpacket.id = player.client_id;
			cpacket.xvel = player.xvel;
			cpacket.yvel = player.yvel;
			
			playersdata.at( player.vetpos )->characterAttributes.xvel = player.xvel;
			playersdata.at( player.vetpos )->characterAttributes.yvel = player.yvel;

			nBytesSent = send( conn, (const char*)&cpacket, ( sizeof(uint8_t)*2 + sizeof(int8_t)*2 ), 0);
		}break;

		case 3:
		{
			ClientPacketAbility cpacket;

			cpacket.msg = 3;
			cpacket.id = player.client_id;
			cpacket.ability = p.ability;
			cpacket.target.id =1;

			nBytesSent = send( conn, (const char*)&cpacket, ( sizeof(uint8_t)*3 + sizeof(struct Target) ), 0);
		}break;

		case 4:
		{
			ClientPacketChat cpacket;

			cpacket.msg = 4;
			cpacket.id = player.client_id;
			strcpy( cpacket.message, chatbox->getText().c_str() );
			
			nBytesSent = send( conn, (const char*)&cpacket, ( sizeof(uint8_t)*2 + sizeof(cpacket.message) ), 0);
		}
	}

	return 0;
}

int AppFramework::RecMessagePort()
{
	char buf[4096];
	unsigned int i;

	int nBytes = recv(conn, buf, sizeof(buf), 0);

	//cout << "bytes recieved: " << nBytes << endl;

	if ((0 == nBytes) || (SOCKET_ERROR == nBytes))
	{
		if (0 != nBytes) //Some error occured, client didn't close the connection
		{
			printf("\nError occurred while recieving on the socket: %d.");
		}
	}
	else
	{
		char* messageToProcess = (char*) buf;
		uint8_t msg = extractUnsignedInt8( &messageToProcess );

		switch( msg )
		{

			case 0:
			{
				uint8_t id = extractUnsignedInt8( &messageToProcess );
				player.pclass = extractUnsignedInt8( &messageToProcess );
				player.client_id = id;

				player.PlayerNameStr = (string) messageToProcess;
				
				switch( player.pclass )
				{
					case 1:
						ability1->ChangeImage( _Graphics, DATA_GUI_BUTTON_ROGUE_ABILITY1_DEFAULT_PNG );
						ability2->ChangeImage( _Graphics, DATA_GUI_BUTTON_ROGUE_ABILITY2_DEFAULT_PNG );
						ability3->ChangeImage( _Graphics, DATA_GUI_BUTTON_ROGUE_ABILITY3_DEFAULT_PNG );
						ability4->ChangeImage( _Graphics, DATA_GUI_BUTTON_ROGUE_ABILITY4_DEFAULT_PNG );
					break;
					case 2:
						ability1->ChangeImage( _Graphics, DATA_GUI_BUTTON_MAGE_ABILITY1_DEFAULT_PNG );
						ability2->ChangeImage( _Graphics, DATA_GUI_BUTTON_MAGE_ABILITY2_DEFAULT_PNG );
						ability3->ChangeImage( _Graphics, DATA_GUI_BUTTON_MAGE_ABILITY3_DEFAULT_PNG );
						ability4->ChangeImage( _Graphics, DATA_GUI_BUTTON_MAGE_ABILITY4_DEFAULT_PNG );
					break;
				}

				login = true;
				uiMgr.setPanel( 1 );
			}break;

			case 1:
			{
				uint8_t num = extractUnsignedInt8( &messageToProcess );
				Update* playerArray = (Update*) messageToProcess;
				
				//cout << (int)num << " " << playersdata.size();
				//cout << (int)num << " " << playersdata.size() << endl;
				
				if( num > playersdata.size() )
				{
					int dif = num - playersdata.size();
					int size = playersdata.size() + dif;
					for( i = playersdata.size(); i<size; i++ )
					{
						std::unique_ptr<Character> c ( new Player() );
						c->Init( _Graphics, 0, 0 );
						playersdata.push_back( std::move(c) );
					}
				}
				

				for( i=0; i<num; i++ )
				{
					playersdata.at(i)->characterAttributes.id = playerArray[i].id;
					playersdata.at(i)->SetPosition( playerArray[i].x, playerArray[i].y );
					playersdata.at(i)->characterAttributes.plane = playerArray[i].plane;
					playersdata.at(i)->characterAttributes.xvel = playerArray[i].xvel;
					playersdata.at(i)->characterAttributes.yvel = playerArray[i].yvel;	
					playersdata.at(i)->characterAttributes.curhp = playerArray[i].curhp;
					playersdata.at(i)->characterAttributes.hp = playerArray[i].hp;
					playersdata.at(i)->characterAttributes.combat = playerArray[i].combat;
					playersdata.at(i)->characterAttributes.slowed = playerArray[i].slowed;
					playersdata.at(i)->characterAttributes.username = playerArray[i].username;
				}
			}break;

			case 2:
			{
				uint8_t num = extractUnsignedInt8( &messageToProcess );
				UpdatePartial* playerArray = (UpdatePartial*) messageToProcess;

				for( i=0; i<num; i++ )
				{
					playersdata.at(i)->characterAttributes.id = playerArray[i].id;
					playersdata.at(i)->SetPosition( playerArray[i].x, playerArray[i].y );
					playersdata.at(i)->characterAttributes.plane = playerArray[i].plane;
					playersdata.at(i)->characterAttributes.xvel = playerArray[i].xvel;
					playersdata.at(i)->characterAttributes.yvel = playerArray[i].yvel;	
					playersdata.at(i)->characterAttributes.curhp = playerArray[i].curhp;
					playersdata.at(i)->characterAttributes.hp = playerArray[i].hp;
					playersdata.at(i)->characterAttributes.combat = playerArray[i].combat;
					playersdata.at(i)->characterAttributes.slowed = playerArray[i].slowed;
				}
			}break;
			
			case 3:
			{
				uint8_t id = extractUnsignedInt8( &messageToProcess );
				uint16_t damage = extractUnsignedInt16( &messageToProcess );
			
				change_status = id;
				InsertValue( damage, 1, player.tarx, player.tary );			
			}break;

			case 4:
			{
				cout << nBytes;

				uint8_t id = extractUnsignedInt8( &messageToProcess );
				string text = (string) messageToProcess;

				for ( i=0 ; i<5; i++)
				{
					if( i!=4 )
					{
						log[i].msg = log[i+1].msg;
						log[i].color = log[i+1].color;
					}
				}

				string chat, name;
				
				for( i=0; i<playersdata.size(); i++ )
				{
					if( playersdata.at(i)->characterAttributes.id == id )
					{
						name = playersdata.at(i)->characterAttributes.username;
						break;
					}
				}

				chat = "[" + name + "]" + " " + text;
				
				log[4].msg = chat;
				log[4].color = 1;

			}break;
		}
	}

	return 0;
}



