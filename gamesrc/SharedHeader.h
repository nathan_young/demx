//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __SHAREDHEADER_H__
#define __SHAREDHEADER_H__

#if defined (WIN32) || defined (_WIN32)
	#define DXAPI __declspec(dllexport)
#endif

#endif //__SHAREDHEADER_H__