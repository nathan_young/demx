#include "AppFramework.h"

void AppFramework::ProcessInput( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	PortPacket p;

	switch( msg )
	{
		case WM_KEYUP :
			switch ( wParam )
			{
				case 'W':
					player.yvel += player.vel;
					p.msg = SEND_VELOCITY;
					SendMessagePort(p);
				return;
				case 'A':
					player.xvel += player.vel;
					p.msg = SEND_VELOCITY;
					SendMessagePort(p);
				return;
				case 'S':
					player.yvel -= player.vel;
					p.msg = SEND_VELOCITY;
					SendMessagePort(p);
				return;
				case 'D':
					player.xvel -= player.vel;
					p.msg = SEND_VELOCITY;
					SendMessagePort(p);
				return;
			};
		return;	

		case WM_KEYDOWN :
			switch ( wParam )
			{
				case 'W':
					if ( (lParam & 0x40000000)==0 )
					{
						player.yvel -= player.vel;
						p.msg = SEND_VELOCITY;
						SendMessagePort(p);
					}
				return;
				case 'A':
					if ( (lParam & 0x40000000)==0 ) 
					{
						player.xvel -= player.vel;
						p.msg = SEND_VELOCITY;
						SendMessagePort(p);
					}
				return;
				case 'S':
					if ( (lParam & 0x40000000)==0 ) 
					{
						player.yvel += player.vel;
						p.msg = SEND_VELOCITY;
						SendMessagePort(p);
					}
				return;
				case 'D':
					if ( (lParam & 0x40000000)==0 ) 
					{
						player.xvel += player.vel;
						p.msg = SEND_VELOCITY;
						SendMessagePort(p);
					}
				return;

				case '1':
					if ( (lParam & 0x40000000)==0 ) 
					{
						UseAbility(1);
					}
				return;
				case '2':
					if ( (lParam & 0x40000000)==0 ) 
					{
						UseAbility(2);
					}
				return;
				case '3':
					if ( (lParam & 0x40000000)==0 ) 
					{
						UseAbility(3);
					}
				return;
				case '4':
					if ( (lParam & 0x40000000)==0 ) 
					{
						UseAbility(4);
					}
				return;	
			};
		return; // CASE WM_KEYDOWN

		case WM_LBUTTONDOWN:
		{
			POINT mouse;
			GetCursorPos(&mouse);
			ScreenToClient(hWnd, &mouse); 

			if( setTarget( mouse.x, mouse.y ) )
			{ 
				return;
			}
		}
		return;
	};
}