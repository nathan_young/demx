//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Input.h"
#include "GlobalHeader.h"


/*=================================================
	Input::Input()
	----------------
	Constrcutor.
=================================================*/
Input::Input()
{
	dikeyboard = NULL;
	dimouse = NULL;
}


/*=================================================
	Input::~Input()
	----------------
	Destructor.
=================================================*/
Input::~Input()
{
}


/*=================================================
	int Input::Init(HWND hWnd)
	----------------
	Initialize directinput device.  Create pointers,
	handles, etc.
=================================================*/
int Input::Init(HWND winhandle)
{
	HRESULT hResult;

	//Initalize DirectInput device.
	hResult = DirectInput8Create(GetModuleHandle(NULL),
								 DIRECTINPUT_VERSION,
								 IID_IDirectInput8,
								 (void**)&dinput,
								 NULL);
	if(hResult != DI_OK)
		return 0;

	//Initialize mouse.
	hResult = dinput->CreateDevice(GUID_SysMouse, &dimouse, NULL);
	if(hResult != FSTATUS_HEALTHY) //Results check have been switched over from DI_OK, to framework FSTATUS_HEALTHY.
		return 0;

	//Initialize keyboard.
	hResult = dinput->CreateDevice(GUID_SysKeyboard, &dikeyboard, NULL);
	if(hResult != FSTATUS_HEALTHY)
		return 0;

	return 1;
}
