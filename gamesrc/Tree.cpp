#include "Tree.h"


Tree::Tree(void)
{
}


Tree::~Tree(void)
{
}

int Tree::Init( Renderer _Graphics, int x, int y )
{

	HRESULT hResult;
	lpd3ddev = _Graphics.ExposePrivateDevice();

	//Create sprite handler object.
	hResult = D3DXCreateSprite(lpd3ddev, &spriteHandler);
	if(hResult != D3D_OK)
	{
		DemxMsgBox("Failed to create sprite handler.");
		return 0;
	}//End if.

	lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_TREE_PNG, D3DCOLOR_XRGB(0, 255, 255));
	spriteAttributes.width	= 126;
	spriteAttributes.height	= 126;

	ObjectMaskValues.below.top =     110;
	ObjectMaskValues.below.left =    60;
	ObjectMaskValues.below.right =   ObjectMaskValues.below.left + 10;
	ObjectMaskValues.below.bottom =  ObjectMaskValues.below.top + 10;

	ObjectMaskValues.above.top =     85;
	ObjectMaskValues.above.left =    56;
	ObjectMaskValues.above.right =   ObjectMaskValues.above.left + 13;
	ObjectMaskValues.above.bottom =  ObjectMaskValues.above.top + 10;

	if(lpTexPlayer == NULL)
	{
		DemxMsgBox("(Data::PlayerSprite)Failed to load.");
	}//End if.

	this->SetPosition( x, y );

	return 1;
}