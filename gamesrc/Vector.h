//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __VECTOR_H__
#define __VECTOR_H__

typedef float component;

class Vector2
{
public:
	Vector2();
	explicit Vector2(float x, float y);

	//Convenience routines.
	void Set(float x, float y);
	void Zero(void);

	//Lengh of vector.
	//void Length();
	Vector2 Length();
	void Normalize();

	//Operator overloads.
	Vector2 operator+(const Vector2 &vector) const;
	Vector2 operator-() const;
	Vector2 operator/(float scalar) const;

	Vector2 &operator+=(const Vector2 &vector);
	Vector2 &operator-=(const Vector2 &vector);
	Vector2 &operator*=(float scalar);
	Vector2 &operator/=(float scalar);

	bool operator==(const Vector2 &vector) const;
	bool operator!=(const Vector2 &vector) const;

public:
	component x;
	component y;
};

class Vector3
{
public:
	Vector3(void);

	//Don't allow the the compiler to use constructor for implicit conversion.
	explicit Vector3(float x, float y, float z);

	//For convenience, but not required.
	void Set(float x, float y, float z);
	void Zero(void);

	//Operator overloads.
	Vector3 operator-() const;

	bool operator==(const Vector3 &vector) const;
	bool operator!=(const Vector3 &vector) const;

public:
	component x;
	component y;
	component z;
};

#endif /*__VECTOR_H__*/