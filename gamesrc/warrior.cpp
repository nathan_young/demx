//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "warrior.h"

/*
ClassWarrior::ClassWarrior()
{
	srand( (unsigned)time(NULL) ); 

	Active_Rampage = false;

	isSlamCD = false;
	isChargeCD = false;
	isRampageCD = false;
	isEnraged_StrikeCD = false;

	damage = 0;
	Ecrit = 1;

	SlamCD            = 1500;
	ChargeCD          = 10000;
	RampageCD         = 45000;
	Enraged_StrikeCD  = 6000;

	fcd=0;
}

	
int ClassWarrior::Slam( int critchance ,int MeleeDamage )
{
	if( !isSlamCD )
	{
		random = rand() % 100 + 1;
		if( random < critchance )
		{
			Ecrit = 2;
		}
		else
		{
			if( Active_Rampage )
				Ecrit = 2;
			else
				Ecrit = 1;
		}

		damage = ( 200 + MeleeDamage ) * Ecrit;
		ability_used = 21;

		TimerSlamCD.start();
		isSlamCD = true;

		if( Ecrit == 1 )
			return 1; 
		if( Ecrit == 2 )
			return 2; 
	}
	else
		return -1; // failed
}

int ClassWarrior::AutoAttack( int critchance ,int MeleeDamage )
{
		random = rand() % 100 + 1;
		if( random < critchance )
		{
			Ecrit = 2;
		}
		else
		{
			if( Active_Rampage )
				Ecrit = 2;
			else
				Ecrit = 1;
		}

		damage = ( MeleeDamage ) * Ecrit;

		if( Ecrit == 1 )
			return 1; 
		if( Ecrit == 2 )
			return 2; 
}

int ClassWarrior::Charge( int critchance ,int MeleeDamage )
{
	if( !isChargeCD )
	{
		random = rand() % 100 + 1;
		if( random < critchance )
		{
			Ecrit = 2;
		}
		else
		{
			if( Active_Rampage )
				Ecrit = 2;
			else
				Ecrit = 1;
		}

		damage = ( MeleeDamage * 0.1 ) * Ecrit;
		
		stun_duration = 30;

		Active_Charge = true;
		TimerChargeCD.start();
		isChargeCD = true;

		if( Ecrit == 1 )
			return 1; 
		if( Ecrit == 2 )
			return 2; 
	}
	else
		return -1;
}

bool ClassWarrior::Rampage()
{
	if( !isRampageCD )
	{
		Active_Rampage = true;
		TimerRampageDur.start();
		TimerRampageCD.start();
		isRampageCD = true;
		return true;
	}
	else
		return false;
}

int ClassWarrior::Enraged_Strike( int critchance ,int MeleeDamage )
{
	if( !isEnraged_StrikeCD )
	{
		random = rand() % 100 + 1;
		if( random < critchance )
		{
			Ecrit = 2;
		}
		else
		{
			if( Active_Rampage )
				Ecrit = 2;
			else
				Ecrit = 1;
		}

		damage = ( 500 + MeleeDamage ) * Ecrit;
		ability_used = 22;

		TimerEnraged_StrikeCD.start();
		isEnraged_StrikeCD = true;

		if( Ecrit == 1 )
			return 1; 
		if( Ecrit == 2 )
			return 2; 
	}
	else
		return -1; // failed
}

void ClassWarrior::CheckCooldowns()
{
	/// ability durations
	if( TimerRampageDur.get_ticks() >= 10000 )
	{
		Active_Rampage = false;
	}

	
	/// ability cooldowns
	if( TimerSlamCD.get_ticks() > SlamCD )
	{
		fcd=1;
		isSlamCD = false;
		TimerSlamCD.stop();
	}
	if( TimerChargeCD.get_ticks() > ChargeCD )
	{
		fcd=2;
		isChargeCD = false;
		TimerChargeCD.stop();
	}
	if( TimerEnraged_StrikeCD.get_ticks() > Enraged_StrikeCD )
	{
		fcd=3;
		isEnraged_StrikeCD = false;
		TimerEnraged_StrikeCD.stop();
	}
	if( TimerRampageCD.get_ticks() > RampageCD )
	{
		fcd=4;
		isRampageCD = false;
		TimerRampageCD.stop();
	}

}

float ClassWarrior::getDamage()
{
	return damage;
}
void ClassWarrior::ResetDamage()
{
	damage = 0;
}
void ClassWarrior::setDamage(float input)
{
	damage = input;
}


bool ClassWarrior::getSlamStatus()
{
	return isSlamCD;
}
bool ClassWarrior::getChargeStatus()
{
	return isChargeCD;
}
bool ClassWarrior::getRampageStatus()
{
	return isRampageCD;
}
bool ClassWarrior::getEnragedStrikeStatus()
{
	return isEnraged_StrikeCD;
}

string ClassWarrior::getCooldownRemaining(int skill)
{
	float remain;
	std::string s="";

	switch(skill)
	{
		case 1:
		{
			if( TimerSlamCD.get_ticks() > 0 )
			{
				remain = SlamCD - TimerSlamCD.get_ticks();
				s = FloatConversion( remain );
			}
		}break;

		case 2:
		{
			if( TimerChargeCD.get_ticks() > 0 )
			{
				remain = ChargeCD - TimerChargeCD.get_ticks();		
				s = FloatConversion( remain );
			}
		}break;

		case 3:
		{
			if( TimerEnraged_StrikeCD.get_ticks() > 0 )
			{
				remain = Enraged_StrikeCD - TimerEnraged_StrikeCD.get_ticks();
				s = FloatConversion( remain );
			}
		}break;

		case 4:
		{
			if( TimerRampageCD.get_ticks() > 0 )
			{
				remain = RampageCD - TimerRampageCD.get_ticks();
				s = FloatConversion( remain );
			}
		}break;
	
	
	}


	return s;
}

string ClassWarrior::FloatConversion(float remain)
{
	int m,aux,c;
	char value[20];
	std::string s;
	s="";

	if( remain < 10000 )
	{	
		m = remain / 1000;
		aux = remain - m * 1000;
		c = aux / 100;
		_itoa_s(m,value,10);
		s = s + value;
		s = s + ".";
		_itoa_s(c,value,10);
		s = s + value;
	}
	else
	{
		remain = remain / 1000;
		_itoa_s(remain,value,10);
		s = s + value;
	}

	return s;
}

int ClassWarrior::ABS(int input)
{
	if(input<0)
		input=input*-1;
	
	return input;
}

void ClassWarrior::CalcChargeVel(int *x, int *y,int plax,int play,int tarx,int tary)
{
	int DeltaX,DeltaY,intvel;
	int a;
	float xdis,ydis,distance,div,b;	

	DeltaX = plax - tarx;
	DeltaY = play - tary;

	if( ABS(DeltaX) > ABS(DeltaY) )
	{
		a = 5;

		div = ABS(DeltaX) / 5;
		b = ABS(DeltaY) / div;

		if( plax > tarx )
			a = -5;

		intvel = 0;
		if( b >= 0 && b < 0.83 )
			intvel = 0;
		if( b >= 0.83 && b < 1.66 )
			intvel = 1;
		if( b >= 1.66 && b < 2.5 )
			intvel = 2;
		if( b >= 2.5 && b < 3.32 )
			intvel = 3;
		if( b >= 3.32 && b < 4.15 )
			intvel = 4;	
		if( b >= 4.15 && b <= 5 )
			intvel = 5;	

		if( play > tary )
			intvel = intvel*-1;

		*x = a;
		*y = intvel;
	}
	else
	{
		a = 5;

		div = ABS(DeltaY) / 5;
		b = ABS(DeltaX) / div;

		if( play > tary )
			a = -5;
						
		intvel = 0;
		if( b >= 0 && b < 0.83 )
			intvel = 0;
		if( b >= 0.83 && b < 1.66 )
			intvel = 1;
		if( b >= 1.66 && b < 2.5 )
			intvel = 2;
		if( b >= 2.5 && b < 3.32 )
			intvel = 3;
		if( b >= 3.32 && b < 4.15 )
			intvel = 4;	
		if( b >= 4.15 && b <= 5 )
			intvel = 5;	

		if( plax > tarx ){
			intvel = intvel*-1;
		}

		*x = intvel;
		*y = a;
	}
	
}

*/