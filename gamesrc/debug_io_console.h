//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DEBUG_IO_CONSOLE_H__
#define __DEBUG_IO_CONSOLE_H__

#define CTC_WHITE 0x0001 | 0x0002 | 0x0004
#define CTC_BLUE 0x0001
#define CTC_GREEN 0x0002
#define CTC_RED 0x0004
#define CTC_INTENSITY 0x0008 //Bold color.
#define CBC_BLUE 0x0010
#define CBC_GREEN 0x0020
#define CBC_RED 0x0040
#define CBC_INTENSITY 0x0080

//Convience color codes.
#define CTC_BRIGHTRED 0x0004 | 0x0008
#define CTC_BRIGHTGREEN 0x002 | 0x0008
#define CTC_BRIGHTBLUE 0x0009 | 0x0008

#define CONTITLE_OVERRIDE
#define DEBUG_OVERRIDE //Use debug in release mode.

#if defined _DEBUG || defined DEBUG_OVERRIDE

#ifdef __cplusplus

#define EXPORT __declspec(dllexport)

extern "C"
{
#ifdef CONTITLE_OVERRIDE
EXPORT void RedirectIOToDebugConsole(const char *name);
#else
EXPORT void RedirectIOToDebugConsole();
#endif

EXPORT void ChangeConsoleTextColor(unsigned long colorcode);
};

#endif //__cplusplus
#endif

#endif /*__DEBUG_IO_CONSOLE_H__*/