#include "Rock.h"


Rock::Rock(void)
{
}


Rock::~Rock(void)
{
}

int Rock::Init( Renderer _Graphics, int x, int y )
{
	HRESULT hResult;
	lpd3ddev = _Graphics.ExposePrivateDevice();

	//Create sprite handler object.
	hResult = D3DXCreateSprite(lpd3ddev, &spriteHandler);
	if(hResult != D3D_OK)
	{
		DemxMsgBox("Failed to create sprite handler.");
		return 0;
	}//End if.

	lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_ROCK1_PNG, D3DCOLOR_XRGB(0, 255, 255));
	spriteAttributes.width	= 65;
	spriteAttributes.height	= 68;

	ObjectMaskValues.below.top =     40;
	ObjectMaskValues.below.left =    5;
	ObjectMaskValues.below.right =   ObjectMaskValues.below.left + 50;
	ObjectMaskValues.below.bottom =  ObjectMaskValues.below.top + 10;

	ObjectMaskValues.above.top =     30;
	ObjectMaskValues.above.left =    5;
	ObjectMaskValues.above.right =   ObjectMaskValues.above.left + 50;
	ObjectMaskValues.above.bottom =  ObjectMaskValues.above.top + 10;
	
	if(lpTexPlayer == NULL)
	{
		DemxMsgBox("(Data::PlayerSprite)Failed to load.");
	}//End if.

	this->SetPosition( x, y );

	return 1;
}