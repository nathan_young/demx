//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"

/*==============================================================
	void AppFramework::LOS(int,int,int,int)
	-----------------------------------------
	Manage basic line of sight.

==============================================================*/

bool AppFramework::LOS( int x1, int y1, int x2, int y2 )
{
	int i;
	int y,n,d;
	RECT c;

//	std::string s;
//	char buffer[20];

	x1 += 16;
	x2 += 16;
	y1 += 25;
	y2 += 25;

	if( x2 > x1 )
	{
		for( i=x1; i<x2; i++ )
		{
			n = ( -x1*y2 + i*y2 - i*y1 + x2*y1 ); 
			d = ( x2 - x1 );
			y = n / d;
			
			SetRect(&c, i,  y,  i+2,  y+2);
			if( cobj.LosObjectCollision(c) )
				return true;
		}
	}
	else
	{
		for( i=x2; i<x1; i++ )
		{
			n = ( -x1*y2 + i*y2 - i*y1 + x2*y1 ); 
			d = ( x2 - x1 );
			y = n / d;

			SetRect(&c, i,  y,  i+2,  y+2);
			if( cobj.LosObjectCollision(c) )
				return true;
		}	
	}

	return false;
}