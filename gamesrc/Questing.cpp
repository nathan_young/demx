//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Questing.h"


Questing::Questing()
{
	questnumber = 2;
}

void Questing::ResetQuests()
{
	int i;

	for(i=0;i<questnumber;i++)
	{
		Quests[i] = 0;
	}
}

void Questing::ResetKills()
{
	int i;

	for( i=0; i<questnumber; i++ )
	{
		Kills[i] = 0;
	}
}

void Questing::LoadQuests()
{

}

void Questing::StartQuest(int id)
{
	Quests[id] = 1;
}

void Questing::ConcludeQuest(int id)
{
	Quests[id] = 2;
}

int Questing::CheckKill(int id)
{
	unsigned int i;

	for( i=0; i<questnumber; i++ )
	{
		if( Quests[i] == 1 )
		{
			if( id == 1 )
			{
				Kills[i]++;

				if( Kills[i] == 5 )
				{
					ConcludeQuest(i);
					return i;
				}
			}
		}
	}

	return -1;
}

void Questing::Show(ID3DXFont *m_pFont)
{
	SetRect(&Text,        500,  300,  0,  0);


	// text

	char buffer[20];
	

	//itoa( Quests[0] , buffer, 10 );
	//m_pFont->DrawTextA(NULL, buffer , -1, &Text, DT_NOCLIP,
	//			D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
				
	int i;

	for(i=0;i<questnumber;i++)
	{
			

		if( Quests[i] == 1 )
		{
			std::string tmp;
			tmp = "Quest: \n";
			itoa( Kills[i] , buffer, 10 );
			tmp += buffer;
			tmp += "/5";
			
			m_pFont->DrawTextA(NULL, tmp.c_str() , -1, &Text, DT_NOCLIP,
				D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
		}

		Text.top += 50;
	}
}