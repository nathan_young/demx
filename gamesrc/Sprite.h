//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __SPRITE_H__
#define __SPRITE_H__

#include <Windows.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "GlobalHeader.h"
#include "Draw.h"
#include "GameData.h"


struct SPRITE
{
	int x;
	int y;
	int width;
	int height;
	int movex;
	int movey;
	int frame;
	int status;

	int columns;
	int curframe;
	int lastframe;
	int animdelay;
	int animcount;
	int scalex;
	int scaley;
	int rotation;
	int rotaterate;
};
/*
struct MASK_VALUES
{
	int top;
	int left;
	int right;
	int bottom;
};
*/
class Sprite
{

protected:
	//Direct3D device.
	
	LPDIRECT3DDEVICE9 lpd3ddev;
	
	int sprite_type;

	//Masks ObjectMask; 


public:

	//static 

	//Texture to hold sprite data.
	LPDIRECT3DTEXTURE9 lpTexPlayer;
	//Player sprite.
	SPRITE spriteAttributes;
	//Sprite handler.
	LPD3DXSPRITE spriteHandler;

	Masks ObjectMask; 
	Masks ObjectMaskValues; 

	/**
	* Constructor.
	**/
	Sprite();

	/**
	* Destructor.
	**/
	~Sprite();

	/**
	* Initialize sprites.
	**/
	virtual int Init( Renderer, int, int ) = 0;

	void Setup(int,int,int,int);

	void SetPosition( int, int );

	virtual void Show( RECT ) = 0;

	void PositionObjectMask();

	/**
	* Clean up. 
	**/
	void Cleanup();

	bool above;
};

#endif //__SPRITE_H__