//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Input.h"

/*==============================================================
	int Input::InitMouse(HWND winhandle.)
	---------------------
	Initialize mouse input device.
==============================================================*/
int Input::InitMouse(HWND winhandle)
{
	HRESULT hr;

	//Set data format for mouse input
	hr = dimouse->SetDataFormat(&c_dfDIMouse);
	if(hr != FSTATUS_HEALTHY)
		return 0;

	//Cooperative level.
	hr = dimouse->SetCooperativeLevel(winhandle, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	if(hr != FSTATUS_HEALTHY)
		return 0;

	//Aquire the mouse.
	hr = dimouse->Acquire();
	if(hr != FSTATUS_HEALTHY)
		return 0;

	return 1;
}


/*==============================================================
	int Input::MouseX()
	---------------------
	Return the mouse state for the X position of the mouse.
==============================================================*/
int Input::MouseX()
{
	return mouse_state.lX;
}


/*==============================================================
	int Input::MouseY()
	---------------------
	Return the mouse state for the Y position of the mouse
==============================================================*/
int Input::MouseY()
{
	return mouse_state.lY;
}


/*==============================================================
	int Input::MouseButton(int button)
	---------------------
	Check to see if a mouse button has been pressed.
==============================================================*/
int Input::MouseButton(int button)
{
	return BUTTON_DOWN(mouse_state, button);
}

/*==============================================================
	void Input::PollMouse()
	---------------------
	Poll for mouse input.
==============================================================*/
void Input::PollMouse()
{
	dimouse->GetDeviceState(sizeof(mouse_state), (LPVOID)&mouse_state);
}


/*==============================================================
	void Input::KillMouse()
	---------------------
	Kill mouse input.
==============================================================*/
void Input::KillMouse()
{
	if(dimouse != NULL)
	{
		dimouse->Unacquire();
		dimouse->Release();
		dimouse = NULL;
	}//End if.
}