//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "Platform.h"

#ifdef assert
	#undef assert
#endif

#if defined (DEBUG)
	#define USE_ASSERTIONS
#endif

extern __declspec(dllexport) void _Assert(const char *_filename, int _linenumber, const char *_expression);

#ifdef USE_ASSERTIONS
	#if defined (COMPILER_MSVC)
		#define Assert(x){if(x){}else{_Assert(__FILE__, __LINE__, #x);}}
	#else
		//For now just defined assert.
		#define Assert(x)	
	#endif
#else
	#define Assert(x)
#endif

#endif /*__DEBUG_H__*/