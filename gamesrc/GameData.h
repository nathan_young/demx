//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DATACONFIG_H__
#define __DATACONFIG_H__

//We should implement a real data managing system.



/*==============================================================
	Sprites
==============================================================*/
#define DATA_PAWN_PLAYERSPRITE_PNG "../../textures/player/BODY_male.png"
#define DATA_MOB_PLAYERSPRITE_PNG  "../../textures/monster.png"

/*==============================================================
	Textures
==============================================================*/
#define DATA_TEXTURE_CHAR_PNG      "../../textures/char.png"
#define DATA_TEXTURE_TREE_PNG      "../../textures/tree.png"
#define DATA_TEXTURE_PINKTREE_PNG  "../../textures/treepink.png"
#define DATA_TEXTURE_WALL_PNG      "../../textures/wall.png"
#define DATA_TEXTURE_GATE_PNG      "../../textures/gate.png"
#define DATA_TEXTURE_STATUE_PNG    "../../textures/statue.png"
#define DATA_TEXTURE_ROCK1_PNG     "../../textures/rock1.png"

#define DATA_TEXTURE_SHIELD_PNG    "../../textures/shield.png"
#define DATA_TEXTURE_SCORCH_PNG    "../../textures/scorch_spell.png"
#define DATA_TEXTURE_CONFLAG_PNG   "../../textures/conflag.png"

#define DATA_TEXTURE_CASTBAR_PNG   "../../textures/cast.png"

/*==============================================================
	Map
==============================================================*/
#define DATA_MAP_MAP01_BMP "../../maps/Map01.png" //"../../../Data/Map/test.bmp"

/*==============================================================
	Audio.
==============================================================*/
#define DATA_AUDIO_BACKGROUND_WAV "../../sounds/MysticHarpLoop01.wav"


/*==============================================================
	UI
==============================================================*/
#define __DATA_UI_DEFAULT_SCHEME__ "TaharezLook.scheme"
#define __DATA_UI_DEFAULT_FONT__  "DejaVuSans-HD-10.font"
//#define __DATA_UI_DEFAULT_MOUSECURSOR "./data/GUI/"


#define DATA_GUI_INVENTORY_PNG		"../../textures/gui/inventory.png"
#define DATA_GUI_ICONS_PNG			"../../textures/gui/icons.png"
#define DATA_GUI_CHARSTATS_PNG	    "../../textures/gui/char-stats.png"
#define DATA_GUI_CHAREQUIP_PNG      "../../textures/gui/char-equip.png"

#define DATA_GUI_TEXTBOX_PNG                       "../../textures/gui/textbox.png"
#define DATA_GUI_TEXTBOX_DEACTIVE_PNG              "../../textures/gui/textboxdeactive.png"

#define DATA_GUI_HPBAR_PNG	"../../textures/gui/hud/hpbar.png"
#define DATA_GUI_HP_PNG		"../../textures/gui/hud/hp.png"

#define DATA_GUI_CASTBAR_PNG	"../../textures/gui/hud/castbar.png"
#define DATA_GUI_CAST_PNG		"../../textures/gui/hud/cast.png"

#define DATA_GUI_BUTTON_DEFAULT_PNG                "../../textures/gui/default.png"
#define DATA_GUI_BUTTON_DEFAULT_PRESSED_PNG        "../../textures/gui/pressed.png"

#define DATA_GUI_BUTTON_ROGUE_ABILITY1_DEFAULT_PNG "../../textures/gui/rogue/default/lacerate_button.png"
#define DATA_GUI_BUTTON_ROGUE_ABILITY1_PRESSED_PNG "../../textures/gui/rogue/pressed/lacerate_button_pressed.png"
#define DATA_GUI_BUTTON_ROGUE_ABILITY1_CD_PNG      "../../textures/gui/rogue/cooldown/lacerate_button_cd.png"

#define DATA_GUI_BUTTON_ROGUE_ABILITY2_DEFAULT_PNG "../../textures/gui/rogue/default/coldblood_button.png"
#define DATA_GUI_BUTTON_ROGUE_ABILITY2_PRESSED_PNG "../../textures/gui/rogue/pressed/coldblood_button_pressed.png"
#define DATA_GUI_BUTTON_ROGUE_ABILITY2_CD_PNG      "../../textures/gui/rogue/cooldown/coldblood_button_cd.png"

#define DATA_GUI_BUTTON_ROGUE_ABILITY3_DEFAULT_PNG "../../textures/gui/rogue/default/stun_button.png"
#define DATA_GUI_BUTTON_ROGUE_ABILITY3_PRESSED_PNG "../../textures/gui/rogue/pressed/stun_button_pressed.png"
#define DATA_GUI_BUTTON_ROGUE_ABILITY3_CD_PNG      "../../textures/gui/rogue/cooldown/stun_button_cd.png"

#define DATA_GUI_BUTTON_ROGUE_ABILITY4_DEFAULT_PNG "../../textures/gui/rogue/default/stealth_button.png"
#define DATA_GUI_BUTTON_ROGUE_ABILITY4_PRESSED_PNG "../../textures/gui/rogue/pressed/stealth_button_pressed.png"
#define DATA_GUI_BUTTON_ROGUE_ABILITY4_CD_PNG      "../../textures/gui/rogue/cooldown/stealth_button_cd.png"

#define DATA_GUI_BUTTON_MAGE_ABILITY1_DEFAULT_PNG "../../textures/gui/mage/default/scorch_button.png"
#define DATA_GUI_BUTTON_MAGE_ABILITY1_PRESSED_PNG "../../textures/gui/mage/pressed/scorch_button_pressed.png"
#define DATA_GUI_BUTTON_MAGE_ABILITY1_CD_PNG      "../../textures/gui/mage/cooldown/scorch_button_cd.png"

#define DATA_GUI_BUTTON_MAGE_ABILITY2_DEFAULT_PNG "../../textures/gui/mage/default/shield_button.png"
#define DATA_GUI_BUTTON_MAGE_ABILITY2_PRESSED_PNG "../../textures/gui/mage/pressed/shield_button_pressed.png"
#define DATA_GUI_BUTTON_MAGE_ABILITY2_CD_PNG      "../../textures/gui/mage/cooldown/shield_button_cd.png"

#define DATA_GUI_BUTTON_MAGE_ABILITY3_DEFAULT_PNG "../../textures/gui/mage/default/slow_button.png"
#define DATA_GUI_BUTTON_MAGE_ABILITY3_PRESSED_PNG "../../textures/gui/mage/pressed/slow_button_pressed.png"
#define DATA_GUI_BUTTON_MAGE_ABILITY3_CD_PNG      "../../textures/gui/mage/cooldown/slow_button_cd.png"

#define DATA_GUI_BUTTON_MAGE_ABILITY4_DEFAULT_PNG "../../textures/gui/mage/default/conflag_button.png"
#define DATA_GUI_BUTTON_MAGE_ABILITY4_PRESSED_PNG "../../textures/gui/mage/pressed/conflag_button_pressed.png"
#define DATA_GUI_BUTTON_MAGE_ABILITY4_CD_PNG      "../../textures/gui/mage/cooldown/conflag_button_cd.png"


#define DATA_GUI_BUTTON_WARRIOR_ABILITY1_DEFAULT_PNG "../../textures/gui/warrior/default/slam_button.png"
#define DATA_GUI_BUTTON_WARRIOR_ABILITY1_PRESSED_PNG "../../textures/gui/warrior/pressed/slam_button_pressed.png"
#define DATA_GUI_BUTTON_WARRIOR_ABILITY1_CD_PNG      "../../textures/gui/warrior/cooldown/slam_button_cd.png"

#define DATA_GUI_BUTTON_WARRIOR_ABILITY2_DEFAULT_PNG "../../textures/gui/warrior/default/charge_button.png"
#define DATA_GUI_BUTTON_WARRIOR_ABILITY2_PRESSED_PNG "../../textures/gui/warrior/pressed/charge_button_pressed.png"
#define DATA_GUI_BUTTON_WARRIOR_ABILITY2_CD_PNG      "../../textures/gui/warrior/cooldown/charge_button_cd.png"

#define DATA_GUI_BUTTON_WARRIOR_ABILITY3_DEFAULT_PNG "../../textures/gui/warrior/default/enraged_strike_button.png"
#define DATA_GUI_BUTTON_WARRIOR_ABILITY3_PRESSED_PNG "../../textures/gui/warrior/pressed/enraged_strike_button_pressed.png"
#define DATA_GUI_BUTTON_WARRIOR_ABILITY3_CD_PNG      "../../textures/gui/warrior/cooldown/enraged_strike_button_cd.png"

#define DATA_GUI_BUTTON_WARRIOR_ABILITY4_DEFAULT_PNG "../../textures/gui/warrior/default/rampage_button.png"
#define DATA_GUI_BUTTON_WARRIOR_ABILITY4_PRESSED_PNG "../../textures/gui/warrior/pressed/rampage_button_pressed.png"
#define DATA_GUI_BUTTON_WARRIOR_ABILITY4_CD_PNG      "../../textures/gui/warrior/cooldown/rampage_button_cd.png"

#define DATA_INTROSCREEN_PNG "../../textures/gui/introscreen.png"


#endif //__DATACONFIG_H__
