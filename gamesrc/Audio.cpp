//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Audio.h"

//Declare for pointer access


/*==============================================================
	AudioEntity::AudioEntity()
	---------------------
	Constrcutor.
==============================================================*/
AudioEntity::AudioEntity()
{
	dsound = NULL;
}


/*==============================================================
	AudioEntity::~AudioEntity()
	---------------------
	Destrcutor.
==============================================================*/
AudioEntity::~AudioEntity()
{
}


/*==============================================================
	int AudioEntity::Init(HWND winhandle)
	---------------------
	Initialize sound system.
==============================================================*/
int AudioEntity::Init(HWND winhandle)
{
	HRESULT hr;

	dsound = new CSoundManager();

	//Initialize DirectSound.
	hr = dsound->Initialize(winhandle, DSSCL_PRIORITY);
	if(hr != DS_OK)
		return 0;

	//Set primary buffer format.		//2 changles, 22050 Freq, 16-Bit
	hr = dsound->SetPrimaryBufferFormat(2, 22050, 16);
	if(hr != DS_OK)
		return 0;

	return 1;

}


/*==============================================================
	CSound *AudioEntity::LoadSound(char *Filename)
	---------------------
	Load a sound from a file.
==============================================================*/
CSound *AudioEntity::LoadSound(char *Filename)
{
	HRESULT hr;
	CSound *wave;

	hr = dsound->Create(&wave, Filename);
	if(hr != DS_OK)
		return NULL;

	return wave;
}





/*=================================================
	void AudioEntity::PlaySound(CSound *sound)
	----------------
	Play a sound.
=================================================*/
void AudioEntity::PlaySound(CSound *sound)
{
	sound->Play();
}


/*=================================================
	void AudioEntity::LoopSound(CSound *sound)
	----------------
	Loop a sound.
=================================================*/
void AudioEntity::LoopSound(CSound *sound)
{
	sound->Play(0, DSBPLAY_LOOPING);
}


/*=================================================
	void AudioEntity::StopSound(CSound *sound)
	----------------
	Stop a sound.
=================================================*/
void AudioEntity::StopSound(CSound *sound)
{
	sound->Stop();
}