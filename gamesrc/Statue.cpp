#include "Statue.h"


Statue::Statue(void)
{
}


Statue::~Statue(void)
{
}


int Statue::Init( Renderer _Graphics, int x, int y )
{
	HRESULT hResult;
	lpd3ddev = _Graphics.ExposePrivateDevice();

	//Create sprite handler object.
	hResult = D3DXCreateSprite(lpd3ddev, &spriteHandler);
	if(hResult != D3D_OK)
	{
		DemxMsgBox("Failed to create sprite handler.");
		return 0;
	}//End if.

	lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_STATUE_PNG, D3DCOLOR_XRGB(0, 255, 255));
	spriteAttributes.width	= 96;
	spriteAttributes.height	= 128;

	ObjectMaskValues.below.top =     90;
	ObjectMaskValues.below.left =    13;
	ObjectMaskValues.below.right =   ObjectMaskValues.below.left + 70;
	ObjectMaskValues.below.bottom =  ObjectMaskValues.below.top + 35;

	ObjectMaskValues.above.top =     75;
	ObjectMaskValues.above.left =    13;
	ObjectMaskValues.above.right =   ObjectMaskValues.above.left + 70;
	ObjectMaskValues.above.bottom =  ObjectMaskValues.above.top + 10;
	
	if(lpTexPlayer == NULL)
	{
		DemxMsgBox("(Data::PlayerSprite)Failed to load.");
	}//End if.

	this->SetPosition( x, y );

	return 1;
}