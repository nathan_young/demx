//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Mage.h"

ClassMage::ClassMage()
{
	srand( time(NULL) ); 

	isFireBlastCD = false;

    FireBlastCD    = 3000;
	FireBlastCastTime = 1000;

	SlowCD = 30000;
}

void ClassMage::StartFireBlastCD()
{
	isFireBlastCD = true;
	TimerFireBlastCD.start();
}
void ClassMage::StartSlowCD()
{
	isSlowCD = true;
	TimerSlowCD.start();
}

int ClassMage::getCurrentCastTime()
{
	return FireBlastCastTime;
}
	
/*=====================
	base abilities 	
=====================*/

int ClassMage::CheckCooldowns()
{	
	if( TimerFireBlastCD.get_ticks() > FireBlastCD )
	{
		isFireBlastCD = false;
		TimerFireBlastCD.stop();
		return 1;
	}

	if( TimerSlowCD.get_ticks() > FireBlastCD )
	{
		isSlowCD = false;
		TimerSlowCD.stop();
		return 3;
	}

	return 0;
}

string ClassMage::getCooldownRemaining( int skill )
{
	float remain;
	std::string s="";

	switch( skill )
	{
		case 1:
		{
			if( TimerFireBlastCD.get_ticks() > 0 )
			{
				remain = FireBlastCD - TimerFireBlastCD.get_ticks();
				s = FloatConversion( remain );
			}
		}break;

		case 2:
		{

		}break;

		case 3:
		{
			if( TimerSlowCD.get_ticks() > 0 )
			{
				remain = SlowCD - TimerSlowCD.get_ticks();
				s = FloatConversion( remain );
			}
		}break;

		case 4:
		{

		}break;	
	}

	return s;
}

string ClassMage::FloatConversion(float remain)
{
	int m,aux,c;
	char value[20];
	std::string s;
	s="";

	if( remain < 10000 )
	{	
		m = remain / 1000;
		aux = remain - m * 1000;
		c = aux / 100;
		_itoa_s(m,value,10);
		s = s + value;
		s = s + ".";
		_itoa_s(c,value,10);
		s = s + value;
	}
	else
	{
		remain = remain / 1000;
		_itoa_s(remain,value,10);
		s = s + value;
	}

	return s;
}

char* ClassMage::DefaultButton(int ability)
{
	switch( ability )
	{
		case 1:
			return DATA_GUI_BUTTON_MAGE_ABILITY1_DEFAULT_PNG;
		break;
	}

	return "";
}