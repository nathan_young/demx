//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DEMXGUI_HUDFRAME_H__
#define __DEMXGUI_HUDFRAME_H__

#include "Draw.h"

namespace DemxGUI
{

class HudFrame
{

	//Direct3D device.
	LPDIRECT3DDEVICE9 lpd3ddev;
	LPDIRECT3DTEXTURE9 lpTexHud;
	LPD3DXSPRITE spriteHandler;

	int x;
	int y;
	int width;
	int height;

public:
	HudFrame(void);
	~HudFrame(void);

	int Init( IDirect3DDevice9*, Renderer, char*, int, int );
	void SetPosition (int, int );
	void Show ( RECT );
};

}

#endif //__DEMXGUI_PROCESSEVENTS_H__
