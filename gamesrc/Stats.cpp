//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Stats.h"

Stats::Stats()
{

}

void Stats::Init()
{
	Endurance=0;
	Strength=0;
	Intelligence=0;
	Agility=0;
}

void Stats::SetAttributes()
{
	Hp = (float)Endurance * 1000;

	MeleePower = Strength * 2;
	MagicPower = Intelligence * 10;

	MeleeCritChance = 5 + (float)Agility * 10;
	MagicCritChance = 5 + (float)Intelligence * 0.4;

	MeleeDamage = WeaponDamage + MeleePower;
}