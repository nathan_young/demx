//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "GameSprites.h"

#include "Vector.h"

void GameObjectSprites::Init()
{
}

void GameObjectSprites::LoadWorld( Renderer _Graphics )
{
	typedef std::unique_ptr<Entity> Eptr;

	Eptr e;
	
	e.reset( new Tree() );
	e->Init( _Graphics, 100, 150 );
	this->sprites.push_back( std::move(e) );
	
	e.reset( new Tree() );
	e->Init( _Graphics, 220, 100 );
	this->sprites.push_back( std::move(e) );
	
	e.reset( new Tree() );
	e->Init( _Graphics, 400, 500 );
	this->sprites.push_back( std::move(e) );
	
	e.reset( new Wall() );
	e->Init( _Graphics, 250, 250 );
	sprites.push_back( std::move(e) );

	e.reset( new Wall() );
	e->Init( _Graphics, 350, 250 );
	sprites.push_back( std::move(e) );

	e.reset( new Statue() );
	e->Init( _Graphics, 400, 60 );
	sprites.push_back( std::move(e) );

	e.reset( new Rock() );
	e->Init( _Graphics, 200, 500 );
	sprites.push_back( std::move(e) );

	e.reset( new Rock() );
	e->Init( _Graphics, 300, 550 );
	sprites.push_back( std::move(e) );
}

void GameObjectSprites::LoadArena( Renderer _Graphics )
{
	int i;
	LPDIRECT3DDEVICE9 tempDevice = _Graphics.ExposePrivateDevice();

}

void GameObjectSprites::PrintSprites( RECT camera )
{
	for( int i=0; i<this->sprites.size(); i++ )
	{
		this->sprites.at( i )->Show( camera );
	}
}

void GameObjectSprites::CleanUp()
{
}