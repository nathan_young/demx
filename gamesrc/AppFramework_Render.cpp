//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"

void AppFramework::GameRender()
{
	unsigned int i,j;
	int k;
	std::string s;
	RECT col;

	LPDIRECT3DDEVICE9 tempDevice = _Graphics.ExposePrivateDevice();


	_Graphics.BeginRendering();


	tempDevice->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 0, 255, 0), 1.0f, 0);

	if ( login )
	{

		ScrollScreen();

		typedef std::vector<std::shared_ptr<Entity>> EntityVector;

		EntityVector allSprites;
		EntityVector orderedSprites;
		EntityVector Stmp;
		
		allSprites.reserve( gameSprites.sprites.size() + playersdata.size() );
		allSprites.insert( allSprites.end(), gameSprites.sprites.begin(), gameSprites.sprites.end() );
		allSprites.insert( allSprites.end(), playersdata.begin(), playersdata.end() );

		int ypos;
		while( allSprites.size() > 0 )
		{
			Stmp.clear();
			ypos = allSprites.front()->ObjectMask.below.top;

			for( i = 0; i<allSprites.size(); i++ )
			{
				if( allSprites.at(i)->ObjectMask.below.top < ypos )
					ypos = allSprites.at(i)->ObjectMask.below.top;
			}

			for( i = 0; i<allSprites.size(); i++ )
			{
				if( allSprites.at(i)->ObjectMask.below.top == ypos )
				{
					Stmp.push_back( allSprites.at(i) );
					allSprites.erase( allSprites.begin() + i );
				}
			}
			orderedSprites.insert( orderedSprites.end(), Stmp.begin(), Stmp.end() );
		}

		for( i = 0; i<orderedSprites.size(); i++ )
			orderedSprites.at(i)->Show( camera );
		


		




		//Render font.

		std::string tmp;

		if( player.combat )
		{
			m_pFont->DrawTextA(NULL, player.PlayerNameStr.c_str(), -1, &PlayerNamePos, DT_NOCLIP,
					D3DXCOLOR(255, 0, 0, 1.0f));
		}
		else
		{
			m_pFont->DrawTextA(NULL, player.PlayerNameStr.c_str(), -1, &PlayerNamePos, DT_NOCLIP,
					D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));	
		}

		gui_hpbar.Show( camera );

		long  prophp;
		prophp = ( player.client_curhp * 123 ) / player.client_hp;

		RECT i1 = {0, 0, prophp, 13};
		RECT i2 = {10+5, 30+5, 10+5 + prophp , 30+5+13 };
		
		_Graphics.DemxStrechRect( gui_hp, &i1, _Graphics.backbuffer, &i2, D3DTEXF_NONE);

		if ( player.pclass == 1 )
			m_pFont->DrawTextA(NULL, "rogue", -1, &ClassPos, DT_NOCLIP,
					D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
		if ( player.pclass == 2 )
			m_pFont->DrawTextA(NULL, "mage",  -1, &ClassPos, DT_NOCLIP,
					D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
		
		if ( player.target.id != 0 )
		{
			m_pFont->DrawTextA(NULL, player.TargetNameStr.c_str(), -1, &TargetNamePos, DT_NOCLIP,
				D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
				

//			gui_target_hpbar.Show( camera );

			long  prophp;
			prophp = ( player.target_curhp * 123 ) / player.target_hp;

			RECT i1 = {0, 0, prophp, 13};
			RECT i2 = {200+5, 30+5, 200+5+prophp , 30+5+13 };
		
			_Graphics.DemxStrechRect( gui_hp, &i1, _Graphics.backbuffer, &i2, D3DTEXF_NONE);
			
		}
		
		//show chat log
		for( k=4; k>=0; k-- )
		{
			if( log[k].color == 1 )
				m_pFont->DrawTextA(NULL, log[k].msg.c_str(), -1, &chat[k], DT_NOCLIP, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
			else
				m_pFont->DrawTextA(NULL, log[k].msg.c_str(), -1, &chat[k], DT_NOCLIP, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
		}
		

		for( i=0; i<5; i++ )
		{
			RECT pos;

			pos.left = dam[i].pos.left - camera.left;
			pos.top  = dam[i].pos.top  - camera.top;

			if( dam[i].color == 1 )
				m_pFont->DrawTextA(NULL, dam[i].msg.c_str(), -1, &pos, DT_NOCLIP,
					D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
			else
				m_pFont->DrawTextA(NULL, dam[i].msg.c_str(), -1, &pos, DT_NOCLIP,
					D3DXCOLOR(255, 255, 0, 1.0f));
		}


/*		if( casting )
		{
			gui_castbar.Setup( 300, 400, 0, 0 );
			gui_castbar.Show( camera );

			long  propcast;
			propcast = ( cast.get_ticks() * 170 ) / player.Mage.getCurrentCastTime();
			if( propcast > 160 )
				propcast = 170;
			cout << propcast << endl;
			RECT i1 = {0, 0, propcast, 13};
			RECT i2 = {300 + 5, 400 + 5, 300 + 5 + propcast , 400 + 5 + 13 };
		
			_Graphics.DemxStrechRect( gui_cast, &i1, _Graphics.backbuffer, &i2, D3DTEXF_NONE);
		}*/

		// call DemxGUI function to render gui elements
		uiMgr.ShowGuiElements();

		RECT showcd = {320 + 5, 500 + 10, 0 , 0 };
		for( i=1; i<=4; i++ )
		{
			s="";
			switch( player.pclass )
			{
				case 1:
					s = player.Rogue.getCooldownRemaining(i);
					//cout << s;
				break;

				case 2:
					s = player.Mage.getCooldownRemaining(i);
				break;
			}

			m_pFont->DrawTextA(NULL, s.c_str(), -1, &showcd, DT_NOCLIP,
					D3DXCOLOR(255, 255, 255, 1.0f));

			//m_pFont->DrawTextA( NULL, s.c_str(), -1, &showcd, DT_LEFT | DT_NOCLIP, D3DXCOLOR(255, 255, 255, 1.0f) );
			showcd.left += 45;
		}

	} // if login
	else
	{
		RECT c1 = {0, 0, 768, 576};
		RECT c2 = {0, 0, 768, 576};

		
		_Graphics.DemxStrechRect( introscreen, &c1, _Graphics.backbuffer, &c2, D3DTEXF_NONE);

		// call DemxGUI function to render gui elements
		uiMgr.ShowGuiElements();
	}

	
	

	_Graphics.EndRendering();
}