//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __GAMESPRITES_H__
#define __GAMESPRITES_H__


#include "Entity.h"
#include "Tree.h"
#include "Wall.h"
#include "Statue.h"
#include "Rock.h"
#include <vector>

class GameObjectSprites
{

public:

	std::vector<std::shared_ptr<Entity>> sprites;
	

	void Init();

	void LoadWorld( Renderer rptr );
	void LoadArena( Renderer rptr );

	void PrintSprites( RECT );

	void CleanUp();
};


#endif //__GAMESPRITES_H__