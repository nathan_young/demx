//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Inventory.h"

void Inventory::Init()
{
	for( unsigned int i=0; i<5; i++ )
	{
		inventory[i]=0;
	}
}

void Inventory::ItemInsert( int item )
{
	for( unsigned int i = 0; i < 5; i++ )
	{
		if( inventory[i] == 0 )
		{
			inventory[i] = item;
			return;
		}
	}
}

void Inventory::ItemDelete( int slot )
{
	inventory[slot]=0;
}

void Inventory::ItemUse( int slot )
{
	//use item can be potion, food, other
}

int Inventory::getSlot(int i)
{
	return inventory[i];
}