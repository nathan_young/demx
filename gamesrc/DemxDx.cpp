//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"



/*==============================================================
	void DemxDx::ConstructGameWorld()
	---------------------
	Use the tile map to build the game world.
==============================================================*/
HRESULT AppFramework::ConstructWorld()
{
	HRESULT hr;
	int x=0, y=0;
	LPDIRECT3DSURFACE9 tiles = NULL;

	//Load bitmap image containing the tiles.
	//tiles = _Graphics.LoadSurface(DATA_SPRITE_MAPTILES_BMP, D3DCOLOR_XRGB(0, 0, 0));
	tiles = _Graphics.LoadSurface(DATA_MAP_MAP01_BMP, D3DCOLOR_XRGB(0, 0, 0));


	//Create scrolling game wrold bitmap.
	hr = _Graphics.CreateOffScreenPlainSurface(WORLD_WIDTH,
											   WORLD_HEIGHT,
											   D3DFMT_X8R8G8B8,
											   D3DPOOL_DEFAULT,
											   &gameworld, //Function returns pointer to surface which is shoved here.
											   NULL);
	if(hr != FSTATUS_HEALTHY)
	{
		MessageBox(NULL, "Error creating working surface!", "ERROR::Internal", MB_OK);
		return E_FAIL;
	}//End if.


	int Grass = 0;
	int Pavement = 1;
	int Skeleton = 2;


	RECT ClipMap[3];

	ClipMap[Grass].left = TILEWIDTH*3;
	ClipMap[Grass].top = 0;
	ClipMap[Grass].right = TILEWIDTH + TILEWIDTH*3;
	ClipMap[Grass].bottom = 0 + TILEHEIGHT;

	ClipMap[Pavement].left = 0;
	ClipMap[Pavement].top = TILEHEIGHT * 5;
	ClipMap[Pavement].right = TILEWIDTH;
	ClipMap[Pavement].bottom = TILEHEIGHT * 5 + TILEHEIGHT;

	ClipMap[Skeleton].left = TILEWIDTH * 2;
	ClipMap[Skeleton].top = TILEHEIGHT;
	ClipMap[Skeleton].right = TILEWIDTH * 2 + TILEWIDTH;
	ClipMap[Skeleton].bottom = TILEHEIGHT + TILEHEIGHT;

	std::ifstream map;
	map.open("../../maps/map.txt");

	for ( int t = 0; t < WORLD_TILE_WIDTH * WORLD_TILE_HEIGHT ; t++ )
	{
		int tileType = -1;
		map >> tileType;

		if ( tileType >= 0 )
		{
			_Graphics.DrawTile2D(tiles, ClipMap[tileType] , TILEWIDTH, TILEHEIGHT, 8, gameworld, x , y );
		}
        //Move to next tile spot
        x += TILEWIDTH;

        //If we've gone too far
        if( x >= WORLD_WIDTH )
        {
            //Move back
            x = 0;
            //Move to the next row
            y += TILEHEIGHT;
        }
	}

	/*
	//Fill the gameworld bitmap with tiles.
	for(y = 0; y < MAPHEIGHT; y++)
		for(x = 0; x < MAPWIDTH; x++)
 			_Graphics.DrawTile2D(tiles, MAPDATA[y * MAPWIDTH + x], 32, 32, 16, gameworld, x * 32, y * 32);
	*/

		//Release the tiles after they have been loaded.
	tiles->Release();

	return FSTATUS_HEALTHY;
}





/*==============================================================
	void DemxDx::ConstructGameWorld()
	---------------------
	Use the tile map to build the arena world.
==============================================================*/
HRESULT AppFramework::ConstructArena()
{
	HRESULT hr;
	int x=0, y=0;
	LPDIRECT3DSURFACE9 tiles = NULL;

	//Load bitmap image containing the tiles.
	//tiles = _Graphics.LoadSurface(DATA_SPRITE_MAPTILES_BMP, D3DCOLOR_XRGB(0, 0, 0));
	tiles = _Graphics.LoadSurface(DATA_MAP_MAP01_BMP, D3DCOLOR_XRGB(0, 0, 0));

	//This is here for temp problem solving.
	if(!tiles)
		MessageBox(NULL, "Failed to load tilemap", "Damn", MB_OK);

	//Create scrolling game wrold bitmap.
	hr = _Graphics.CreateOffScreenPlainSurface(ARENA_WIDTH,
											   ARENA_HEIGHT,
											   D3DFMT_X8R8G8B8,
											   D3DPOOL_DEFAULT,
											   &arenaworld, //Function returns pointer to surface which is shoved here.
											   NULL);
	if(hr != FSTATUS_HEALTHY)
	{
		MessageBox(NULL, "Error creating working surface!", "ERROR::Internal", MB_OK);
		return E_FAIL;
	}//End if.


	int Grass = 0;
	int Pavement = 1;
	int Skeleton = 2;

	int part1 = 2;
	int part2 = 3;
	int part3 = 4;
	int part4 = 5;

	RECT ClipMap[12];

	ClipMap[Grass].left = TILEWIDTH*3;
	ClipMap[Grass].top = 0;
	ClipMap[Grass].right = TILEWIDTH *4;
	ClipMap[Grass].bottom = TILEHEIGHT;

	ClipMap[Pavement].left = 0;
	ClipMap[Pavement].top = TILEHEIGHT * 5;
	ClipMap[Pavement].right = TILEWIDTH;
	ClipMap[Pavement].bottom = TILEHEIGHT * 6;

	ClipMap[Skeleton].left = TILEWIDTH * 2;
	ClipMap[Skeleton].top = TILEHEIGHT;
	ClipMap[Skeleton].right = TILEWIDTH * 3;
	ClipMap[Skeleton].bottom = TILEHEIGHT * 2 ;

	int j,k;

	j=4;
	k=5;
	for(int i=2;i<=10;i++)
	{
		ClipMap[i].left = TILEWIDTH * j;
		ClipMap[i].top = TILEHEIGHT * k;
		ClipMap[i].right = ClipMap[i].left + TILEWIDTH;
		ClipMap[i].bottom = ClipMap[i].top + TILEHEIGHT;
		
		j++;
		if( j>6 )
		{
			j=4;
			k++;
		}
	}

	std::ifstream map;
	map.open("../../maps/arena.txt");

	for ( int t = 0; t < ARENA_TILE_WIDTH * ARENA_TILE_HEIGHT ; t++ )
	{
		int tileType = -1;
		map >> tileType;

		if ( tileType>=0 )
		{
			_Graphics.DrawTile2D(tiles, ClipMap[tileType] , TILEWIDTH, TILEHEIGHT, 8, arenaworld, x , y );
		}

        //Move to next tile spot
        x += TILEWIDTH;

        //If we've gone too far
        if( x >= ARENA_WIDTH )
        {
            //Move back
            x = 0;
		
            //Move to the next row
            y += TILEHEIGHT;
        }
	
	
	}

	/*
	//Fill the gameworld bitmap with tiles.
	for(y = 0; y < MAPHEIGHT; y++)
		for(x = 0; x < MAPWIDTH; x++)
 			_Graphics.DrawTile2D(tiles, MAPDATA[y * MAPWIDTH + x], 32, 32, 16, gameworld, x * 32, y * 32);
	*/

		//Release the tiles after they have been loaded.
	tiles->Release();

	return FSTATUS_HEALTHY;
}












/*==============================================================
	void DemxDx::ScrollScreen()
	---------------------
	Handle the x and y scrolling of the screen.
==============================================================*/
void AppFramework::ScrollScreen()
{
	
	RECT r2 = {0, 0, WINDOW_WIDTH , WINDOW_HEIGHT };
	
	
	
	RECT r1;

	r1.left = camera.left;
	r1.top = camera.top;
	r1.right = camera.left + WINDOW_WIDTH ;
	r1.bottom = camera.top + WINDOW_HEIGHT ;


	//Draw the current game world view.
	//d3ddev->StretchRect(gameworld, &r1, backbuffer, &r2, D3DTEXF_NONE);
	//if(FAILED(_Graphics.DemxStrechRect(gameworld, &r2, backbuffer, &r2, D3DTEXF_NONE)))
		//MessageBox(NULL, "I am broken!", "Erorr", MB_OK);
	
	
	if( player.plane == 0 )
	{
		_Graphics.DemxStrechRect(gameworld, &r1, _Graphics.backbuffer, &r2, D3DTEXF_NONE);
	}
	else
	{
		_Graphics.DemxStrechRect(arenaworld, &r1, _Graphics.backbuffer, &r2, D3DTEXF_NONE);
	}
	
	
	

}
