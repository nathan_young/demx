//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Collision.h"

#include <iostream>
using namespace std;


Collision::Collision()
{
//	ObjectMasks.clear();
}

void Collision::Init( GameObjectSprites sprites )
{
	this->sprites = sprites.sprites;
}

bool Collision::ObjectCollision( int pos, RECT entity )
{
	unsigned int i;

	for( i=0; i< sprites.size(); i++ )
	{	
		if( entity.bottom > sprites.at(i)->ObjectMask.below.bottom /*ObjectMasks[i].below.bottom*/ )
		{
			if( CheckCollision(entity, sprites.at(i)->ObjectMask.above) )
				return true;
		}
		else
		{
			if( CheckCollision(entity, sprites.at(i)->ObjectMask.below) )
				return true;
		}
	}

	return false;
}

/*
	LINE OF SIGHT
*/
bool Collision::LosObjectCollision( RECT entity )
{
	/*
	unsigned int i;

	for( i=0; i<totalobj; i++ )
	{	
		if( CheckCollision( entity, ObjectMasks[i].above ) || CheckCollision( entity, ObjectMasks[i].below ) )
		{
			return true;
		}
	}
	*/
	return false;
}

bool Collision::CheckCollision(RECT r1, RECT r2)
{
    RECT dest;
    return IntersectRect(&dest, &r1, &r2);
}

void Collision::Cleanup()
{
//	delete ObjectMasks;
}