//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef _STATS_H
#define	_STATS_H

class Stats
{

public:

	/*=================
		base stats
	=================*/
	int Endurance;
	int Strength;
	int Intelligence;
	int Agility;
//	int Dexterity;
//	int Wisdom;

	int WeaponDamage;
	int MeleeDamage;

	/*=================
		attributes
	=================*/
	float Hp;
//	int Mana;

	int MeleePower;
	int MagicPower;
	
	int MeleeCritChance;
	int MagicCritChance;
	
	Stats();

	void Init();
	void SetAttributes();
};

#endif	/* _STATS_H */