#include "Wall.h"


Wall::Wall(void)
{
}


Wall::~Wall(void)
{
}

int Wall::Init( Renderer _Graphics, int x, int y )
{

	HRESULT hResult;
	lpd3ddev = _Graphics.ExposePrivateDevice();

	//Create sprite handler object.
	hResult = D3DXCreateSprite(lpd3ddev, &spriteHandler);
	if(hResult != D3D_OK)
	{
		DemxMsgBox("Failed to create sprite handler.");
		return 0;
	}//End if.


	lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_WALL_PNG, D3DCOLOR_XRGB(0, 255, 255));
	spriteAttributes.width	= 94;
	spriteAttributes.height	= 39;


	ObjectMaskValues.below.top =     15;
	ObjectMaskValues.below.left =    0;
	ObjectMaskValues.below.right =   ObjectMaskValues.below.left + 94;
	ObjectMaskValues.below.bottom =  ObjectMaskValues.below.top + 24;

	ObjectMaskValues.above.top =     0;
	ObjectMaskValues.above.left =    0;
	ObjectMaskValues.above.right =   ObjectMaskValues.above.left + 94;
	ObjectMaskValues.above.bottom =  ObjectMaskValues.above.top + 10;
	
	
	if(lpTexPlayer == NULL)
	{
		DemxMsgBox("(Data::PlayerSprite)Failed to load.");
	}//End if.

	this->SetPosition( x, y );

	return 1;
}
