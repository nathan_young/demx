//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef _INVENTORY_H
#define	_INVENTORY_H

#include "Stats.h"
#include "Gear.h"

class Inventory
{
	int inventory[5];

public:

	void Init();

	//
	void ItemInsert( int );

	//
	//void ItemEquip( int, Gear&, Stats& );
	
	//
	void ItemUse( int );
	
	//
	void ItemDelete( int );

	int getSlot( int );
};

#endif	/* _PLAYER_H */