//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __ROGUE_H__
#define __ROGUE_H__

#include <Windows.h>
#include <time.h>
#include "Timer.h"
#include "GlobalHeader.h"
#include <stdio.h>
#include <stdlib.h>
#include "GameData.h"

#include <string>
using namespace std;

class ClassRogue
{

public:

	bool isLacerateCD;
	bool isColdBloodCD;
	bool isStunCD;
	bool isStealthCD;

	int LacerateCD;
	int ColdBloodCD;
	int StunCD;
	int StealthCD;

	Timer TimerLacerateCD;
	Timer TimerColdBloodCD;
	Timer TimerStunCD;
	Timer TimerStealthCD;

	void StartLacerateCD();
	void StartColdBloodCD();

	ClassRogue();
	
	int CheckCooldowns();

	string getCooldownRemaining(int skill);
	string FloatConversion(float);

	char* DefaultButton(int);
};
#endif //__ROGUE_H__