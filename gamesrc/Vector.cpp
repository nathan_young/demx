//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Vector.h"
#include "MathLib.h"

Vector2::Vector2()
{
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Explicit constructor.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Convenience function to set the x and y
//>> component values.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Vector2::Set(float x, float y)
{
	this->x = x;
	this->y = y;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Convenience function to zero the values of
//>> the x and y components.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Vector2::Zero(void)
{
	x = y = 0.0f;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Find length (norm) of vector.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//void Vector2::Length()
//{
//	Vector2 norm;
//	norm.x = Sqr(this->x);
//	norm.y = Sqr(this->y);
//
//	float sum = Sqrt(norm.x + norm.y);
//
//	this->x = sum;
//	this->y = sum;
//}
Vector2 Vector2::Length()
{
	Vector2 lvec;
	lvec.x = Sqr(this->x);
	lvec.y = Sqr(this->y);

	float lsum = Sqrt(lvec.x + lvec.y);

	return(Vector2(lsum, lsum));
}

/*********************************
* Begin operator implementations.
**********************************/
bool Vector2::operator==(const Vector2 &vector) const
{
	return(x == vector.x && y == vector.y);
}


bool Vector2::operator!=(const Vector2 &vector) const
{
	return(x != vector.x || y != vector.y);
}


Vector2 Vector2::operator-()const
{
	return(Vector2(-x, -y));
}


Vector2 Vector2::operator/(float scalar) const
{
	return(Vector2(x / scalar, y / scalar));
}


Vector2 Vector2::operator+(const Vector2 &vector) const
{
	return(Vector2(x + vector.x, y + vector.y));
}


Vector2 &Vector2::operator+=(const Vector2 &vector)
{
	this->x += vector.x;
	this->y += vector.y;

	return(*this);
}


Vector2 &Vector2::operator-=(const Vector2 &vector)
{
	this->x -= vector.x;
	this->y -= vector.y;

	return(*this);
}


Vector2 &Vector2::operator*=(float scalar)
{
	this->x *= scalar;
	this->y *= scalar;

	return(*this);
}


Vector2 &Vector2::operator/=(float scalar)
{
	this->x /= scalar;
	this->y /= scalar;

	return(*this);
}

/*********************************
* End operator implementations.
**********************************/

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Normalize a vector.
//>> @Note: This normalizes the current vector
//>>		calling the function.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Vector2::Normalize()
{
	Vector2 vec = Length() / 1;
	this->x = vec.x;
	this->y = vec.y;
}

//--------------------------------------------------------------------------------------------------------------------
//------------------------------------------------ VECTOR 3 ----------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Constructor.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Vector3::Vector3(void)
{
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Explicit constructor for setting components.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Convenience function to set the values
//>> of the x, y, and z components.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Vector3::Set(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Convenience function to zero out the values
//>> of the x, y, and z components.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
void Vector3::Zero(void)
{
	x = y = z = 0.0f;
}


Vector3 Vector3::operator-() const
{
	return (Vector3(-x, -y, -z));
}


bool Vector3::operator==(const Vector3 &vector) const
{
	return(x == vector.x && y == vector.y && z == vector.z);
}


bool Vector3::operator!=(const Vector3 &vector) const
{
	return(x != vector.x || y != vector.y || z != vector.z);
}