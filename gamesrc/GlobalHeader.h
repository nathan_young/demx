//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include <iostream>
#include <Windows.h>
#include <stdint.h>

#ifndef __APPLICATIONDEFINITIONS_H__
#define __APPLICATIONDEFINITIONS_H__

//Deprecated input macros.
#define KEY_DOWN(vk_code)((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define KEY_UP(vk_code)((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)

#define DEMX_SAFE_DELETE(p) {if(p){delete[](p); (p)=NULL;}}
#define DEMX_SAFE_RELEASE(p) {if(p){(p)->Release(); (p)=NULL;}}


#define DemxMsgBox(Msg) MessageBox(NULL, Msg, "Error::Internal", MB_OK | MB_ICONERROR)

/**
* Settings for developer
* specific stuff
* @NOTE: I use a diffrent
* SQL schematic, so this allows
* me to switch them.
**/
#define DEV_PROFILE NATHAN

/**
* Name of the application
**/
#define GAME_NAME "DemxRpg::(Build)Development"

/**
* Determine the window window width
* for the game.
**/
#define WINDOW_WIDTH 768

/**
* Determine the window height for
* the game.
**/
#define WINDOW_HEIGHT 576

/**
* Determine if the game is 
* to run in fullscreen.
**/
#define WINDOWEDMODE true

/**
* Definitions for tiles.
**/
#define TILEWIDTH 32
#define TILEHEIGHT 32


#define MAPWIDTH 25
#define MAPHEIGHT 25
#define GAMEWORLD_WIDTH (TILEWIDTH * MAPWIDTH)
#define GAMEWORLD_HEIGHT (TILEHEIGHT * MAPHEIGHT)


#define WORLD_TILE_WIDTH 25
#define WORLD_TILE_HEIGHT 25
#define WORLD_WIDTH  ( TILEWIDTH  * WORLD_TILE_WIDTH )
#define WORLD_HEIGHT ( TILEHEIGHT * WORLD_TILE_HEIGHT )

#define ARENA_TILE_WIDTH 25
#define ARENA_TILE_HEIGHT 18
#define ARENA_WIDTH  ( TILEWIDTH  * ARENA_TILE_WIDTH )
#define ARENA_HEIGHT ( TILEHEIGHT * ARENA_TILE_HEIGHT )


/**
* The Frame rate the game runs
**/
#define FRAME_RATE 15

//@NOTE: Every directX module has its own types for error checking: D3D_OK, DI_OK, etc.  These are all the same thing,
//		 though they have diffrent names.  Because I am sick of having to keep track of all of them define our own for the
//		 framework.  Doing to makes things A:) Eaiser, and B:) Ensures some level of compatibility.
//@NOTE: Instead of defining these to the type def counterparts, IE: #define <var> D3D_OK, these get defined to the actual
//		 system calls IE: #define <var> ((HRESULT)0L).  Because the DirectX librarys are ever changing, this saves many future
//		 build errors.
#define FSTATUS_HEALTHY ((HRESULT)0L)
#define FSTATUS_ILL		((HRESULT)1L)
#define FSTATUS_EMPTY NULL


/*
	STRUCT DUMP
*/

struct Masks
{
	RECT above;
	RECT below;
};

struct ChatLog
{
	std::string msg;
	int color;
};

struct DamageDisplay
{
	std::string msg;
	int color;
	RECT pos;
	int time;
};

struct Target
{
	uint8_t type;
	uint8_t id;
};

#define SpriteTreeNum    4
#define SpriteWallNum    2 
#define SpriteStatueNum  1  
#define SpriteRock1Num   3

#define ATTEMPT_LOGIN 0
#define SEND_CHAT 1
#define SEND_VELOCITY 2
#define SEND_USEABILITY 3
#define SEND_STATS 4




#endif //__APPLICATIONDEFINITIONS_H__