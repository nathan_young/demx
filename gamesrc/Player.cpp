#include "Player.h"


Player::Player(void)
{
}


Player::~Player(void)
{
}

int Player::Init( Renderer _Graphics, int x, int y )
{

	HRESULT hResult;
	lpd3ddev = _Graphics.ExposePrivateDevice();

	//Create sprite handler object.
	hResult = D3DXCreateSprite(lpd3ddev, &spriteHandler);
	if(hResult != D3D_OK)
	{
		DemxMsgBox("Failed to create sprite handler.");
		return 0;
	}//End if.


	lpTexPlayer = _Graphics.LoadTexture(DATA_PAWN_PLAYERSPRITE_PNG, D3DCOLOR_XRGB(0, 255, 255));
			
	spriteAttributes.width	= 40;
	spriteAttributes.height	= 50;

	ObjectMaskValues.above.top =     0;
	ObjectMaskValues.above.left =    0;
	ObjectMaskValues.above.right =   0;
	ObjectMaskValues.above.bottom =  0;

	ObjectMaskValues.below.top =     45;
	ObjectMaskValues.below.left =    10;
	ObjectMaskValues.below.right =   ObjectMaskValues.below.left + 20;
	ObjectMaskValues.below.bottom =  ObjectMaskValues.below.top + 5;

	
	if(lpTexPlayer == NULL)
	{
		DemxMsgBox("(Data::PlayerSprite)Failed to load.");
	}//End if.

	this->SetPosition( x, y );

	return 1;
}
