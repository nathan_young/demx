//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"


/*==============================================================
	void AppFramework::GameLoop()
	---------------------
	Main game loop.
==============================================================*/
void AppFramework::GameLoop()
{
	unsigned int i;
	
		
	

		for( i=0; i<playersdata.size(); i++ )
		{
			if( playersdata.at(i)->characterAttributes.id != 0 )
			{
				playersdata.at(i)->SetPosition( 
					playersdata.at(i)->spriteAttributes.x += playersdata.at(i)->characterAttributes.xvel,
					playersdata.at(i)->spriteAttributes.y += playersdata.at(i)->characterAttributes.yvel );

				RECT col;
				col.left = playersdata.at(i)->spriteAttributes.x;
				col.top  = playersdata.at(i)->spriteAttributes.y;
				col.right = col.left + 32;
				col.bottom = col.top + 50;
			
				if ( cobj.ObjectCollision( i, col ) )
				{			
					playersdata.at(i)->SetPosition( 
						playersdata.at(i)->spriteAttributes.x -= playersdata.at(i)->characterAttributes.xvel,
						playersdata.at(i)->spriteAttributes.y -= playersdata.at(i)->characterAttributes.yvel );
				}
				
				

				if( playersdata.at(i)->characterAttributes.yvel > 0 )
				{
					playersdata.at(i)->spriteAttributes.status = 0;
					playersdata.at(i)->spriteAttributes.frame++;
				}
				else if ( playersdata.at(i)->characterAttributes.yvel < 0 )
				{
					playersdata.at(i)->spriteAttributes.status = 3;
					playersdata.at(i)->spriteAttributes.frame++;
				}
				else if ( playersdata.at(i)->characterAttributes.xvel < 0 )
				{
					playersdata.at(i)->spriteAttributes.status = 1;
					playersdata.at(i)->spriteAttributes.frame++;
				}
				else if ( playersdata.at(i)->characterAttributes.xvel > 0 )
				{
					playersdata.at(i)->spriteAttributes.status = 2;
					playersdata.at(i)->spriteAttributes.frame++;
				}
				else
					playersdata.at(i)->spriteAttributes.frame=0;

				if( playersdata.at(i)->spriteAttributes.frame >= 9 )
					playersdata.at(i)->spriteAttributes.frame=0;

		


				if( playersdata.at(i)->characterAttributes.id == player.client_id )
				{
					if( player.client_curhp != playersdata.at(i)->characterAttributes.curhp )
					{
						player.client_curhp = playersdata.at(i)->characterAttributes.curhp;
						stringstream buffer(player.PlayerCurHpStr);
						buffer >> player.client_curhp;
					}
					if( player.client_hp != playersdata.at(i)->characterAttributes.hp )
					{
						player.client_hp = playersdata.at(i)->characterAttributes.hp;
						stringstream buffer(player.PlayerHpStr);
						buffer >> player.client_hp;
					}

					if( player.plane != playersdata.at(i)->characterAttributes.plane )
					{
						player.plane = playersdata.at(i)->characterAttributes.plane;

						if( player.plane == 0 )
						{
							gameSprites.LoadWorld( _Graphics );
							cobj.Init ( gameSprites );
						}
						else
						{
							gameSprites.LoadArena( _Graphics );
							cobj.Init ( gameSprites );
						}
					}

					player.combat = playersdata.at(i)->characterAttributes.combat;

					player.vetpos=i;
				}

				// if target is a player
				if( playersdata.at(i)->characterAttributes.id == player.target.id && player.target.type == 1 )
				{
					if( player.target_curhp != playersdata.at(i)->characterAttributes.curhp )
					{
						player.target_curhp = playersdata.at(i)->characterAttributes.curhp;
						stringstream buffer(player.TargetCurHpStr);
						buffer >> player.target_curhp;			
					}
					if( player.target_hp != playersdata.at(i)->characterAttributes.hp )
					{
						player.target_hp    = playersdata.at(i)->characterAttributes.hp;
						stringstream buffer(player.TargetHpStr);
						buffer >> player.target_hp;
					}
				}
			}
		}

	/*
	for(i=0;i<2;i++)
	{
		//if( !creeps[i].slowed )
		//{
			creeps[i].x += creeps[i].xvel;
			creeps[i].y += creeps[i].yvel;
		//}
		//else
		//{
			creeps[i].x += (creeps[i].xvel*0.4);
			creeps[i].y += (creeps[i].yvel*0.4);
		//}

		if( creeps[i].yvel > 0 ){
			creeps[i].status = 0;
			creeps[i].frame++;
		}
		else if ( creeps[i].yvel < 0 ){
			creeps[i].status = 3;
			creeps[i].frame++;
		}
		else if ( creeps[i].xvel < 0 ){
			creeps[i].status = 1;
			creeps[i].frame++;
		}
		else if ( creeps[i].xvel > 0 ){
			creeps[i].status = 2;
			creeps[i].frame++;
		}
		else
			creeps[i].frame=0;

		if( creeps[i].frame >=4 )
			creeps[i].frame=0;

		if( (creeps[i].id == target.id) && target.type==2 )
		{
			if( target_curhp != creeps[i].curhp )
			{
				target_curhp = creeps[i].curhp;
				_itoa_s(target_curhp,TargetCurHpStr,10);
						
			}
			if( target_hp != creeps[i].hp )
			{
				target_hp    = creeps[i].hp;
				_itoa_s(target_hp,TargetHpStr,10);
			}
		}

	}
	*/

		


	//sets camera values
	if( playersdata.size() > 0 )
	{
		camera.left = playersdata.at( player.vetpos )->spriteAttributes.x + ( 50/2 ) - ( WINDOW_WIDTH/2  );
		camera.top  = playersdata.at( player.vetpos )->spriteAttributes.y + ( 32/2) -  ( WINDOW_HEIGHT/2 );
	}
    //Keep the camera in bounds.
    if( camera.left < 0 )
        camera.left = 0;

    if( camera.top < 0 )
        camera.top = 0;


	if( player.plane == 0 )
	{
		if( camera.left > WORLD_WIDTH - WINDOW_WIDTH )
			camera.left = WORLD_WIDTH - WINDOW_WIDTH;
 
		if( camera.top > WORLD_HEIGHT - WINDOW_HEIGHT )
			camera.top = WORLD_HEIGHT - WINDOW_HEIGHT;
	}
	else
	{
		if( camera.left > ARENA_WIDTH - WINDOW_WIDTH )
			camera.left = ARENA_WIDTH - WINDOW_WIDTH;
 
		if( camera.top > ARENA_HEIGHT - WINDOW_HEIGHT )
			camera.top = ARENA_HEIGHT - WINDOW_HEIGHT;
	}
	
	for( i=0; i<5; i++ )
	{
		dam[i].time ++;
		if( dam[i].time > 20 )
			dam[i].msg = "";

		dam[i].pos.top-=2;
	}

	for( i=0; i<playersdata.size(); i++ )
	{
		if( playersdata.at(i)->characterAttributes.id == player.target.id && player.target.type==1 )
		{
			player.tarx = playersdata.at(i)->spriteAttributes.x;
			player.tary = playersdata.at(i)->spriteAttributes.y;
		}
	}

	if( casting )
	{
		if( cast.get_ticks() > player.Mage.getCurrentCastTime() )
		{
			casting = false;
			cast_finished = true;
		}
	}
	
	if( cast_finished )
	{
		switch(cast_spell)
		{
			case 20:
			{
				PortPacket p;
				player.Mage.StartFireBlastCD();
				ability1->ChangeImage( _Graphics, DATA_GUI_BUTTON_MAGE_ABILITY1_CD_PNG );
				p.msg = SEND_USEABILITY;
				p.ability = cast_spell;
				SendMessagePort( p );
			}break;
		}
		cast_finished = false;
	}
	
	switch( player.pclass )
	{
		case 1:
		{
			int ability = player.Rogue.CheckCooldowns();
			if( ability != 0 )
			{
				switch( ability )
				{
					case 1:
						ability1->ChangeImage( _Graphics, player.Rogue.DefaultButton(ability) );
					break;
					case 2:
						ability2->ChangeImage( _Graphics, player.Rogue.DefaultButton(ability) );
					break;
					case 3:
						ability3->ChangeImage( _Graphics, player.Rogue.DefaultButton(ability) );
					break;
					case 4:
						ability4->ChangeImage( _Graphics, player.Rogue.DefaultButton(ability) );
					break;
				}
			}
		}break;
		case 2:
		{
			int ability = player.Mage.CheckCooldowns();
			if( ability != 0 )
			{
				switch( ability )
				{
					case 1:
						ability1->ChangeImage( _Graphics, player.Mage.DefaultButton(ability) );
					break;
					case 2:
						ability2->ChangeImage( _Graphics, player.Mage.DefaultButton(ability) );
					break;
					case 3:
						ability3->ChangeImage( _Graphics, player.Mage.DefaultButton(ability) );
					break;
					case 4:
						ability4->ChangeImage( _Graphics, player.Mage.DefaultButton(ability) );
					break;
				}
			}
		}break;
	}
	
	//BackgroundLoop01
	//_Audio.LoopSound(BackgroundLoop01);
}
