//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Draw.h"

/*==============================================================
	Renderer::Renderer()
	---------------------
	Constructor.
==============================================================*/
Renderer::Renderer()
{
}


/*==============================================================
	Renderer::~Renderer()
	---------------------
	Destructor.
==============================================================*/
Renderer::~Renderer()
{
}


/*==============================================================
	HRESULT Renderer::Initialize(HWND winHandle, int iWidth,
		int iHeight, bool bIsFullscreen)
	---------------------
	Initialize Direct3D9, and get it ready for use.
==============================================================*/
HRESULT Renderer::Initialize(HWND winHandle, int iWidth, int iHeight, bool bIsFullscreen)
{
	//Initialize DirectD3D.
	if(NULL == (d3d = Direct3DCreate9(D3D_SDK_VERSION)))
	{
		MessageBox(NULL, "Failed to initialize Direct3D9", "ERROR::Internal", MB_OK);
		return E_FAIL;
	}//End if.

	ZeroMemory(&d3dpp, sizeof(d3dpp));

	//Fill in our device creation parameters.
	d3dpp.Windowed				= bIsFullscreen;
	d3dpp.SwapEffect			= D3DSWAPEFFECT_COPY; //Copy the back buffer.
	d3dpp.BackBufferFormat		= D3DFMT_X8R8G8B8;
	d3dpp.BackBufferCount		= 1;
	d3dpp.BackBufferWidth		= iWidth;
	d3dpp.BackBufferHeight		= iHeight;

	//Create our d3d9 device.
	d3d->CreateDevice(D3DADAPTER_DEFAULT,
					  D3DDEVTYPE_HAL,
					  winHandle,
					  D3DCREATE_SOFTWARE_VERTEXPROCESSING,
					  &d3dpp,
					  &d3ddev);
	//Check to see if device creation went OK.
	if(d3ddev == NULL)	
	{
		MessageBox(NULL, "Failed to create d3d9 device.", "ERROR::Internal", MB_OK);
		return E_FAIL;
	}//End if.

	//Clear the back buffer to a color. Dx9 works on 1 - 255 color values.
	d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(180, 170, 100), 1.0f, NULL); //Weird yellow/green color.
	//Grab pointer to backbuffer.
	d3ddev->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backbuffer);

	return S_OK;
}


/*==============================================================
	void Renderer::SetClearColorTo()
	---------------------
	Set the clear color of the back buffer.
==============================================================*/
void Renderer::SetClearColorTo(float fRed, float fGreen, float fBlue, float fAlpha)
{
	//
}


/*==============================================================
	HRESULT Renderer::BeginRendering()
	---------------------
	Begin the rendering of a scene.
==============================================================*/
void Renderer::BeginRendering()
{
	d3ddev->BeginScene();
}


/*==============================================================
	void Renderer::EndRendering()
	---------------------
	End the rendering of a scene.
==============================================================*/
void Renderer::EndRendering()
{
	d3ddev->EndScene();
	//Take rendered scene and flip back buffers to present.
	d3ddev->Present(NULL, NULL, NULL, NULL);
}


/*==============================================================
	void Renderer::Cleanup()
	---------------------
	Release and cleanup all Direct3D objects when program exits.
==============================================================*/
void Renderer::Cleanup()
{
	if(d3d != NULL)
		d3d->Release();
	if(d3ddev != NULL)
		d3ddev->Release();

	if(backSurface != NULL)
		backSurface->Release();
}


/*==============================================================
	LPDIRECT3DSURFACE9 Renderer::LoadSurface(char *Filename,
		D3DCOLOR transcolor)
	---------------------
	Load a surface from a file.
==============================================================*/
LPDIRECT3DSURFACE9 Renderer::LoadSurface(std::string Filename, D3DCOLOR transcolor)
{
	LPDIRECT3DSURFACE9 image = NULL;
	D3DXIMAGE_INFO info;
	HRESULT hResult;

	//Get width and height from bitmap file.
	hResult = D3DXGetImageInfoFromFile(Filename.c_str(), &info);
	if(hResult != D3D_OK)
		return NULL;

	//Create the surface.
	hResult = d3ddev->CreateOffscreenPlainSurface(info.Width,
												  info.Height,
												  D3DFMT_X8R8G8B8,
												  D3DPOOL_DEFAULT,
												  &image,
												  NULL);
	if(hResult != D3D_OK)
		return NULL;

	//Load the file surface to new surface.
	hResult = D3DXLoadSurfaceFromFile(image,
									  NULL,
									  NULL,
									  Filename.c_str(),
									  NULL,
									  D3DX_DEFAULT,
									  transcolor,
									  NULL);
	if(hResult != D3D_OK)
		return NULL;

	return image;
}


/*==============================================================
	LPDIRECT3DTEXTURE9 Renderer::LoadTexture(char *Filename,
		D3DCOLOR transcolor)
	---------------------
	Load a texture from a file.
==============================================================*/
LPDIRECT3DTEXTURE9 Renderer::LoadTexture(char *Filename, D3DCOLOR transcolor)
{
	//Texture pointer.
	LPDIRECT3DTEXTURE9 texture = NULL;
	//Texture info.
	D3DXIMAGE_INFO info;
	HRESULT hr;

	//Get width and height info from file.
	hr = D3DXGetImageInfoFromFile(Filename,
								  &info);
	if(hr != D3D_OK)
		return NULL;

	//Create new texture from bitmap file.
	D3DXCreateTextureFromFileEx(d3ddev, //Direct3D device.
								Filename, //Name of file to load.
								info.Width, //Width of file.
								info.Height, //Height of file.
								1, //Mipmaps - 1 = no chaining.
								D3DPOOL_DEFAULT, //Surface type.
								D3DFMT_UNKNOWN, //Surface format.
								D3DPOOL_DEFAULT, //Memory class for texture.
								D3DX_DEFAULT, //Image filter.
								D3DX_DEFAULT, //Mipmap filter.
								transcolor, //Colorkey for transparency.
								&info, //Bitmap info from file.
								NULL, //Color palette.
								&texture); //Destination texture.
	if(hr != D3D_OK)
		return NULL;

	return texture;
}


/*==============================================================
	void Renderer::DrawSprite(LPDIRECT3DSURFACE9 _inSprite,
		RECT *_inSourceRect, LPDIRECT3DSURFACE8 _backbuffer,
		RECT *_inDestRect, _D3DTEXTUREFILTERTYE _inTextureFilter)
	---------------------
	Copy the contents of the source rectangle to the destination
	rectangle.  The source rectangle can be strecthed and 
	filtered by the copy.

	@NOTE: This is often used to change the aspect ratio of
		   a video stream, but it works great for copying
		   stuff directly to the back buffer.

	@NOTE: Be carefull with this call.  Also note that not alot
		   of time went into to this function, thus it is 
		   experemental, and could cause problems.
==============================================================*/
HRESULT Renderer::DemxStrechRect(IDirect3DSurface9 *pSourceSurface, const RECT* pSourceRect, IDirect3DSurface9 *pDestSurface,
	const RECT *pDestRect, D3DTEXTUREFILTERTYPE Filter)
{
	//if(FAILED(d3ddev->StretchRect(pSourceSurface, pSourceRect, pDestSurface,
	//	pDestRect, Filter)))
	//{
	//	return E_FAIL;
	//}//End if.

	//return S_OK;
	d3ddev->StretchRect(pSourceSurface, pSourceRect, pDestSurface, pDestRect, Filter);
	return S_OK;
}


/*==============================================================
	HRESULT Renderer::CreateOffScreenPlainSurface(UINT Width,
		UINT Height, D3DFORMAT Format, D3DPOOL Pool, IDirect3DSurface9 **ppSurface,
		HANDLE *pSharedHandle)
	---------------------
	Create an off screen plain surface.
==============================================================*/
HRESULT Renderer::CreateOffScreenPlainSurface(UINT Width, UINT Height, D3DFORMAT Format, D3DPOOL Pool, 
	IDirect3DSurface9 **ppSurface, HANDLE *pSharedHandle)
{
   if(!SUCCEEDED(d3ddev->CreateOffscreenPlainSurface(Width, Height, Format, Pool, ppSurface, pSharedHandle)))
		return E_FAIL;

   return D3D_OK;
}


/*==============================================================
	void Renderer::DrawTile2D(LPDIRECT3DSURFACE9 SourceSurface,
		int TileNum, int TileWidth, int TileHeight, int ColumnTiles,
		LPDIRECT3DSURFACE9 DestSurface, int DestX, int DestY)
	---------------------
	Draw a 2D tile.
==============================================================*/
void Renderer::DrawTile2D(LPDIRECT3DSURFACE9 SourceSurface, RECT r1, int TileWidth, int TileHeight,
	int ColumnTiles, LPDIRECT3DSURFACE9 DestSurface, int DestX, int DestY)
{
/*
	//Source image.
	RECT r1;
	r1.left		= 3 * TileWidth;
	r1.top		= 0;
	r1.right	= r1.left + TileWidth;
	r1.bottom	= r1.top + TileHeight;
*/
	//Destination image.
	RECT r2 = {DestX, DestY, DestX + TileWidth, DestY + TileHeight};

	if(d3ddev == NULL)
		MessageBox(NULL, "I am still NULL", "Damn", MB_OK);

	//Draw the tile.
	d3ddev->StretchRect(SourceSurface, &r1, DestSurface, &r2, D3DTEXF_NONE);
}