//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//>> Copyright (C) 2008 - 2012 Execute Software
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __SYSTEM_H__
#define __SYSTEM_H__

//System header us used a lot, so let's include some commonly needed
//header files here.
#include "Platform.h"
#include "Debug.h"

class __declspec(dllexport) System
{
public:
	System(){}
	~System(){}

	virtual void CreateDialogBox(const char *msg, ...){}
	
	//Error dialogs.
	virtual void CreateDialogFatal(const char *msg){}

	virtual void WriteToDebugConsole(const char *msg){}
	virtual void PrintDbgStr(const char *msg){};
	virtual void PrintStr(const char *msg){};

	//Default text color is white, but other colors can be specified.
	virtual void PrintUsrStr(const char *msg, unsigned int color = 0x0000){};

	virtual void dbgprintf(const char *format, ...){};
};

static System *ddxsystem;

#endif /*__SYSTEM_H__*/