//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Sprite.h"


/*=================================================
	Sprite::Sprite()
	----------------
	Consturctor.
=================================================*/
Sprite::Sprite()
{
//	localinput = new Input;
}


/*=================================================
	Sprite::~Sprite()
	----------------
	Destructor.
=================================================*/
Sprite::~Sprite()
{
}


/*=================================================
	int Sprite::Init(IDirect3DDeivce9 *id3ddevice)
	----------------
	Initialize the sprite class.  This funciton
	is incharge of doing stuff like setting up
	the timer.
=================================================*/
//int Sprite::Init(IDirect3DDevice9 *id3ddevice,Renderer _Graphics,int type);

/*=================================================
	void Sprite::Setup(int x,int y,int frame,int status)
	----------------
	Setups sprite coordinates, frame and status
=================================================*/
void Sprite::Setup(int x,int y,int frame,int status)
{
	SetPosition( x, y );
	spriteAttributes.frame = frame;
	spriteAttributes.status = status;
}

void Sprite::SetPosition( int x, int y )
{
	spriteAttributes.x = x;
	spriteAttributes.y = y;
	this->PositionObjectMask();
}

void Sprite::PositionObjectMask()
{
	this->ObjectMask.above.left   = this->spriteAttributes.x + ObjectMaskValues.above.left;
	this->ObjectMask.above.right  = this->spriteAttributes.x + ObjectMaskValues.above.right;
	this->ObjectMask.above.top    = this->spriteAttributes.y + ObjectMaskValues.above.top;
	this->ObjectMask.above.bottom = this->spriteAttributes.y + ObjectMaskValues.above.bottom;

	this->ObjectMask.below.left   = this->spriteAttributes.x + ObjectMaskValues.below.left;
	this->ObjectMask.below.right  = this->spriteAttributes.x + ObjectMaskValues.below.right;
	this->ObjectMask.below.top    = this->spriteAttributes.y + ObjectMaskValues.below.top;
	this->ObjectMask.below.bottom = this->spriteAttributes.y + ObjectMaskValues.below.bottom;
}

/*=================================================
	void Sprite::Cleanup()
	----------------
	Cleanup sprites when program ends.
=================================================*/
/*
void Sprite::Show(RECT camera)
{
	RECT srcRect;

	spriteHandler->Begin(D3DXSPRITE_ALPHABLEND);

	if( sprite_type == 1 )
	{
		D3DXVECTOR3 position(
			(float) (spriteAttributes.x - camera.left), 
			(float) (spriteAttributes.y - camera.top),
			0);

		srcRect.left   = spriteAttributes.frame * spriteAttributes.width;
		srcRect.top    = spriteAttributes.status * spriteAttributes.height;
		srcRect.right  = srcRect.left + spriteAttributes.width;
		srcRect.bottom = srcRect.top + spriteAttributes.height;
	
		spriteHandler->Draw(lpTexPlayer, &srcRect, NULL, &position, D3DCOLOR_XRGB(255, 255, 255));
	}
	
	
	if ( sprite_type == 2 )
	{
		D3DXVECTOR3 position(
			(float) spriteAttributes.x - camera.left, 
			(float) spriteAttributes.y - camera.top,
			0);

		srcRect.left   = 0;
		srcRect.top    = 0;
		srcRect.right  = srcRect.left + spriteAttributes.width;
		srcRect.bottom = srcRect.top + spriteAttributes.height;

		spriteHandler->Draw(lpTexPlayer, &srcRect, NULL, &position, D3DCOLOR_XRGB(255, 255, 255));
	}


	if ( sprite_type == 3 )
	{
		D3DXVECTOR3 position(
			(float) spriteAttributes.x, 
			(float) spriteAttributes.y,
			0);

		srcRect.left   = 0;
		srcRect.top    = 0;
		srcRect.right  = srcRect.left + spriteAttributes.width;
		srcRect.bottom = srcRect.top + spriteAttributes.height;

		spriteHandler->Draw(lpTexPlayer, &srcRect, NULL, &position, D3DCOLOR_XRGB(255, 255, 255));
	}

	
	if ( sprite_type == 4 )
	{
		D3DXVECTOR3 position(
			(float) spriteAttributes.x - camera.left, 
			(float) spriteAttributes.y - camera.top,
			0);

		srcRect.left   = (spriteAttributes.frame % spriteAttributes.status) * spriteAttributes.width;
		srcRect.top    = 0;
		srcRect.right  = srcRect.left + spriteAttributes.width;
		srcRect.bottom = srcRect.top + spriteAttributes.height;

		spriteHandler->Draw(lpTexPlayer, &srcRect, NULL, &position, D3DCOLOR_XRGB(255, 255, 255));
	}
	
	spriteHandler->End();
}
*/

/*=================================================
	void Sprite::Cleanup()
	----------------
	Cleanup sprites when program ends.
=================================================*/
void Sprite::Cleanup()
{

	if(lpTexPlayer != NULL)
		lpTexPlayer->Release();

	if(spriteHandler != NULL)
		spriteHandler->Release();
}

/*

	HRESULT hResult;
	lpd3ddev = id3ddevice;

	//Create sprite handler object.
	hResult = D3DXCreateSprite(lpd3ddev, &spriteHandler);
	if(hResult != D3D_OK)
	{
		DemxMsgBox("Failed to create sprite handler.");
		return 0;
	}//End if.


	switch ( type )
	{

		// 1-99 player,npc
		case 1:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_PAWN_PLAYERSPRITE_PNG, D3DCOLOR_XRGB(0, 255, 255));
			
			spriteAttributes.width	= 40;
			spriteAttributes.height	= 50;
			sprite_type=1;

			ObjectMask.below.top =     10;
			ObjectMask.below.left =    40;
			ObjectMask.below.right =   ObjectMask.below.left + 30;
			ObjectMask.below.bottom =  ObjectMask.below.top + 10;
		}break;
		case 2:{
			lpTexPlayer = _Graphics.LoadTexture(DATA_MOB_PLAYERSPRITE_PNG, D3DCOLOR_XRGB(0, 255, 255));
			
			spriteAttributes.width	= 50;
			spriteAttributes.height	= 39;
			sprite_type=1;
		}break;

		case 100:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_TREE_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 126;
			spriteAttributes.height	= 126;
			sprite_type=2;

			ObjectMask.below.top =     112;
			ObjectMask.below.left =    57;
			ObjectMask.below.right =   ObjectMask.below.left + 13;
			ObjectMask.below.bottom =  ObjectMask.below.top + 10;
			ObjectMask.above.top =     85;
			ObjectMask.above.left =    56;
			ObjectMask.above.right =   ObjectMask.above.left + 13;
			ObjectMask.above.bottom =  ObjectMask.above.top + 10;
		}break;
		case 101:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_WALL_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 94;
			spriteAttributes.height	= 39;
			sprite_type=2;

			ObjectMask.below.top =     15;
			ObjectMask.below.left =    0;
			ObjectMask.below.right =   ObjectMask.below.left + 94;
			ObjectMask.below.bottom =  ObjectMask.below.top + 24;
			ObjectMask.above.top =     0;
			ObjectMask.above.left =    0;
			ObjectMask.above.right =   ObjectMask.above.left + 94;
			ObjectMask.above.bottom =  ObjectMask.above.top + 10;
		}break;
		case 102:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_STATUE_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 96;
			spriteAttributes.height	= 128;
			sprite_type=2;

			ObjectMask.below.top =     90;
			ObjectMask.below.left =    13;
			ObjectMask.below.right =   ObjectMask.below.left + 70;
			ObjectMask.below.bottom =  ObjectMask.below.top + 35;
			ObjectMask.above.top =     75;
			ObjectMask.above.left =    13;
			ObjectMask.above.right =   ObjectMask.above.left + 70;
			ObjectMask.above.bottom =  ObjectMask.above.top + 10;
		}break;
		case 103:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_ROCK1_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 65;
			spriteAttributes.height	= 68;
			sprite_type=2;

			ObjectMask.below.top =     40;
			ObjectMask.below.left =    5;
			ObjectMask.below.right =   ObjectMask.below.left + 50;
			ObjectMask.below.bottom =  ObjectMask.below.top + 10;
			ObjectMask.above.top =     30;
			ObjectMask.above.left =    5;
			ObjectMask.above.right =   ObjectMask.above.left + 50;
			ObjectMask.above.bottom =  ObjectMask.above.top + 10;
		}break;
		
		// spells 
		case 500:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_SHIELD_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 32;
			spriteAttributes.height	= 50;
			sprite_type=2;
		}break;
		case 501:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_SCORCH_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 40;
			spriteAttributes.height	= 40;
			sprite_type=4;
		}break;
		case 502:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_CONFLAG_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 40;
			spriteAttributes.height	= 40;
			sprite_type=4;
		}break;
	
		
		case 1000:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_TEXTURE_CASTBAR_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 80;
			spriteAttributes.height	= 20;
			sprite_type=3;
		}break;
		case 1001:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_GUI_INVENTORY_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 290;
			spriteAttributes.height	= 374;
			sprite_type=3;
		}break;
		case 1002:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_GUI_HPBAR_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 133;
			spriteAttributes.height	= 40;
			sprite_type=3;
		}break;
		case 1003:
		{
			lpTexPlayer = _Graphics.LoadTexture(DATA_GUI_CASTBAR_PNG, D3DCOLOR_XRGB(0, 255, 255));
			spriteAttributes.width	= 180;
			spriteAttributes.height	= 23;
			sprite_type=3;
		}break;
		
		

	}

	
	if(lpTexPlayer == NULL)
	{
		DemxMsgBox("(Data::PlayerSprite)Failed to load.");
	}//End if.



	return 1;
	*/