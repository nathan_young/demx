//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"
#include "debug.h"

#include "System.h"
#include "Win32/System_Win32.h"
#include "Platform.h"

#define AUTORUN_SERVER false

#include "debug_io_console.h"

HRESULT AppFramework::GameInit(HWND winHandle)
{
#if defined (WIN32)
	System_Win32 winsys32;
	ddxsystem = &winsys32;
#endif
	Assert(ddxsystem);


#ifdef _DEBUG
	RedirectIOToDebugConsole("Debug Console");
#endif
	ddxsystem->WriteToDebugConsole("----- Debug I/Oo redirection successful. -----");
	
#ifdef _DEBUG
	ddxsystem->PrintDbgStr("Beginning console test...");

	//Test some functions and colors.
	unsigned int scanToColor[] =
	{
		0x0001, 0x0002, 0x0003, 0x0004, 0x0005,
		0x0006, 0x0007, 0x0008, 0x0009//, 0x00010,
		//0x0011, 0x0012, 0x0013, 0x0014, 0x0015,
		//0x0016, 0x0017, 0x0018, 0x0019, 0x0020
	};

	int sindex;
	for(sindex = 0; sindex < 9; sindex++)
	{
		unsigned int tcolor = scanToColor[sindex];
		ddxsystem->PrintUsrStr("\t\t\tNow testing color index...", tcolor);
	}
	
	ddxsystem->PrintDbgStr("Console test successful.\n\n");
#endif


	HRESULT hr;

#ifdef _DEBUG
	ddxsystem->PrintDbgStr("Audio system: initializing.");
#endif
	//Initialize sound.
//	_Audio.Init(winHandle);

#ifdef _DEBUG
	ddxsystem->PrintDbgStr("Audio system initialization successful.");
#endif

#ifdef _DEBUG
	ddxsystem->PrintDbgStr("Renderer: initializing.");
#endif
	//Initialize the graphics system.
	_Graphics.Initialize(winHandle, WINDOW_WIDTH, WINDOW_HEIGHT, WINDOWEDMODE);
#ifdef _DEBUG
	ddxsystem->PrintDbgStr("Renderer initialization successful.");
#endif



	LPDIRECT3DDEVICE9 tempDevice = _Graphics.ExposePrivateDevice();
	Assert(tempDevice);




#if defined (AUTORUN_SERVER)
	//Little hack to launch server automatically.  Sadly, this isn't portable.
	//WinExec("server.exe", SW_SHOW);
#endif


	/*========================================
			Connect to server
	========================================*/

	ddxsystem->PrintDbgStr("\nConnecting to server...");
	char buf[4096];
	FILE *fp = fopen("../../config/server.ini","r");
	string sServerAddress;
	while((fgets(buf,4096,fp)) != NULL)
	{
		if(buf[0] == '#')
			continue;
		sServerAddress = buf;
	}
	fclose(fp);
	Init(sServerAddress.c_str(),6000);

	if ( !m_bIsConnected )
	{
		MessageBox(NULL, "Could not connect to server!", "Warning", MB_OK);
		ddxsystem->PrintUsrStr("Error: Server not detected", CTC_RED);
	}
	else
	{
		ddxsystem->PrintUsrStr("Server detected... attaching server debug output to I/O console stream", CTC_BRIGHTBLUE);
	}



	

	/*========================================
			Load font
	========================================*/
	hr = D3DXCreateFont( _Graphics.GetDevice(),     //D3D Device
                     22,               //Font height
                     0,                //Font width
                     FW_NORMAL,        //Font Weight
                     1,                //MipLevels
                     false,            //Italic
                     DEFAULT_CHARSET,  //CharSet
                     OUT_DEFAULT_PRECIS, //OutputPrecision
                     ANTIALIASED_QUALITY, //Quality
                     DEFAULT_PITCH|FF_DONTCARE,//PitchAndFamily
                     "Arial",          //pFacename,
                     &m_pFont);         //ppFont
	if(FAILED(hr))
		return hr;

	hr = D3DXCreateFont(
		_Graphics.GetDevice(),
		15,
		0, 
		FW_NORMAL, 
		1, 
		FALSE, 
		DEFAULT_CHARSET,
        OUT_DEFAULT_PRECIS, 
		PROOF_QUALITY  , 
		DEFAULT_PITCH | FF_DONTCARE,
        "Arial", 
		&s_pFont);

	if(FAILED(hr))
		return hr;



	hr = _Graphics.CreateOffScreenPlainSurface(200,
											   250,
											   D3DFMT_X8R8G8B8,
											   D3DPOOL_DEFAULT,
											   &box, //Function returns pointer to surface which is shoved here.
											   NULL);
	if(hr != FSTATUS_HEALTHY)
	{
		MessageBox(NULL, "Error creating working surface!", "ERROR::Internal", MB_OK);
		return E_FAIL;
	}//End if.

	ConstructWorld();
	ConstructArena();

	player.Init();

	InitUiElements();

	int i;

	//for( i=0; i<5; i++ )
	//	playersdata.at(i)->characterAttributes.id = 0;

	uiMgr.setPanel( 0 );


	int ICONWIDTH = 34;
	int ICONHEIGHT = 34;

	int sword=1;
	int chest=2;
	int robe=3;
	int magicsword=4;


	ClipIcons[sword].left   = 0;
	ClipIcons[sword].top    = ICONHEIGHT*4;
	ClipIcons[sword].right  = ClipIcons[sword].left + ICONWIDTH;
	ClipIcons[sword].bottom = ClipIcons[sword].top  + ICONHEIGHT;
	ClipIcons[chest].left   = ICONWIDTH*2;
	ClipIcons[chest].top    = ICONHEIGHT*10;
	ClipIcons[chest].right  = ClipIcons[chest].left + ICONWIDTH;
	ClipIcons[chest].bottom = ClipIcons[chest].top  + ICONHEIGHT;
	ClipIcons[robe].left   = 0;
	ClipIcons[robe].top    = ICONHEIGHT*10;
	ClipIcons[robe].right  = ClipIcons[robe].left + ICONWIDTH;
	ClipIcons[robe].bottom = ClipIcons[robe].top  + ICONHEIGHT;
	ClipIcons[magicsword].left   = 0;
	ClipIcons[magicsword].top    = ICONHEIGHT*4;
	ClipIcons[magicsword].right  = ClipIcons[magicsword].left + ICONWIDTH;
	ClipIcons[magicsword].bottom = ClipIcons[magicsword].top  + ICONHEIGHT;



	casting = false;
	cast_finished = false;

	cast_spell=0;




	camera.left = 0;
	camera.top = 0;



	for ( i=0;i<5;i++ ){
		log[i].color = 0;
		log[i].msg = "";
		dam[i].color = 0;
		dam[i].msg= "";
	}






	/*
	head = 0;
	chest = 0;
	legs = 0;
	weapon = 0;
	*/

	change_status = -1;


//	Inventory_Init();

	login = false;



	icons = _Graphics.LoadSurface(DATA_GUI_ICONS_PNG, D3DCOLOR_XRGB(0, 0, 0));

	//gui_hpbar = _Graphics.LoadSurface(DATA_GUI_HPBAR_PNG, D3DCOLOR_XRGB(0, 0, 0));
	gui_hp = _Graphics.LoadSurface(DATA_GUI_HP_PNG, D3DCOLOR_XRGB(0, 0, 0));
	gui_cast = _Graphics.LoadSurface(DATA_GUI_CAST_PNG, D3DCOLOR_XRGB(0, 0, 0));

	introscreen = _Graphics.LoadSurface(DATA_INTROSCREEN_PNG, D3DCOLOR_XRGB(0, 0, 0));
	


	srand( time(NULL) ); 
//
	gui_hpbar.Init( _Graphics, 10, 30 );
//	gui_hpbar.Setup( 10, 30, 0, 0 );

//	gui_target_hpbar.Init(tempDevice,_Graphics,1002);
//	gui_target_hpbar.Setup(200, 30, 0, 0);

//	gui_castbar.Init(tempDevice,_Graphics,1003);

//	sprite_player.Init(tempDevice,_Graphics,1);
//	sprite_mob.Init(tempDevice,_Graphics,2);
	
	/*
	shield.Init(tempDevice,_Graphics,500);
	scorch_spell.Init(tempDevice,_Graphics,501);
	conflag_spell.Init(tempDevice,_Graphics,502);
	castbar.Init( tempDevice,_Graphics,1000 );
	*/

	gameSprites.LoadWorld( _Graphics );
	cobj.Init ( gameSprites );
	
	//Rendering.
	SetRect(&TextPos,        20,  450,  0,  0);
	
	// player info
	SetRect(&PlayerNamePos,  10,   5,  0,  0);
	SetRect(&PlayerCurHpPos, 50,   15,  0,  0);
	
	// target info
	SetRect(&TargetNamePos,	 200,  5,  0,  0);
	SetRect(&TargetCurHpPos, 200,  40,  0,  0);
	
	SetRect(&ClassPos,       10,   70,  0,  0);

	//SetRect(&ClassPos, 10, 70, 0, 0);

	//ClassPos;

	// chat position
	SetRect(&chat[0],  10, 400, 0 ,0);
	SetRect(&chat[1],  10, 425, 0, 0);
	SetRect(&chat[2],  10, 450, 0, 0);
	SetRect(&chat[3],  10, 475, 0, 0);
	SetRect(&chat[4],  10, 500, 0, 0);
	
	SetRect(&ability[0],  200, 420, 0, 0);
	SetRect(&ability[1],  200, 450, 0, 0);
	SetRect(&ability[2],  340, 420, 0, 0);
	SetRect(&ability[3],  340, 450, 0, 0);


	

	//playersdata.at(0) = new Player();

	//Sprite.lpd3ddev = _Graphics.ExposePrivateDevice();

	//BackgroundLoop01 = AudioEntity.LoadSound("data/Audio/music.wav");

	ddxsystem->PrintStr("Game data successfully loaded.");

	return D3D_OK;
}
