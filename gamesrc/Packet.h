//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef _PACKET_H
#define	_PACKET_H

#include <stdint.h>
#include "GlobalHeader.h"
#include <string>

struct Avatar
{
	//
  	uint8_t id; // 0-255
	
	std::string username;
	//
	uint16_t x;
	uint16_t y;

	uint8_t plane; // what istance is the player in  -- default = 0
	int8_t xvel;
	int8_t yvel;
	uint8_t status;

	int frame;
	int vetpos;

	int16_t curhp;
	int16_t hp;

	Target target;
	uint8_t combat;

	bool stunned;
	bool slowed;
	bool stealthed;

	float shield;
	int scorch_effect;
	int conflag_effect;
};

struct UpdatePartial
{
	uint8_t id;

	uint16_t x;
	uint16_t y;
	uint8_t plane;
	int8_t xvel;
	int8_t yvel;

	int16_t curhp;
	int16_t hp;

	uint8_t combat;
	uint8_t slowed;

	//char username[10];
};

struct Update
{
	uint8_t id;

	uint16_t x;
	uint16_t y;
	uint8_t plane;
	int8_t xvel;
	int8_t yvel;

	int16_t curhp;
	int16_t hp;

	uint8_t combat;
	uint8_t slowed;

	char username[10];
};

struct PortPacket
{
	uint8_t msg;
	std::string text;
	uint8_t xvel;
	uint8_t yvel;
	uint8_t ability;
};

struct ClientPacketAttemptLogin
{
	uint8_t msg;
	char username[20];
};

struct ClientPacketLogin
{
	uint8_t msg;
  	uint8_t id;
};

struct ClientPacketMovement
{
	uint8_t msg;
  	uint8_t id;	
	int8_t xvel;
	int8_t yvel;
};

struct ClientPacketAbility
{
	uint8_t msg;
	uint8_t id;
	uint8_t ability;
	Target target;
};

struct ClientPacketChat
{
	uint8_t msg;
	uint8_t id;
	char message[20];
};
#endif	/* _PACKET_H */