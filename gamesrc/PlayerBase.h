//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef _PLAYER_H
#define	_PLAYER_H

#include "Rogue.h"
#include "Mage.h"
#include "GlobalHeader.h"

#include <string>
#include "Gear.h"
#include "Inventory.h"

class PlayerBase
{
public:

	int client_id;
	int vetpos;

	Inventory pInv;
	Stats     pStats;
	Gear      pGear;
	
	int pclass;

	int client_curhp;
	int client_hp;

	int tarx;
	int tary;
	uint8_t vel;

	int target_curhp;
	int target_hp;

	bool combat;

	std::string name;
	std::string PlayerNameStr;
	std::string TargetNameStr;
	std::string PlayerCurHpStr;
	std::string PlayerHpStr;
	std::string TargetCurHpStr;
	std::string TargetHpStr;

/*=================================
	Target Info
		-> type of target
		-> ID of target
==================================*/
	Target target;

	ClassRogue   Rogue;
	ClassMage    Mage;

	

	//Stats playerstats;

	bool player_stunned;
	bool player_slowed;
	bool charging;

	int8_t xvel;
	int8_t yvel;

	int plane;

public:

	void Init();

	void InventoryInsert( int );
	void InventoryEquip( int );
	void InventoryUse();
	void InventoryDelete( int );
	int getInventorySlot( int );

	void setStats( int, int, int, int );
	
	bool EquipItem( int );

	int getGearHead();
	int getGearChest();
	int getGearLegs();
	int getGearWeapon();

	int getAttribute_MeleeDamage();
	int getAttribute_MeleePower();
	int getAttribute_MagicPower();
	int getAttribute_MeleeCritChance();
	int getAttribute_MagicCritChance();
	int getAttribute_WeaponDamage();
	float getAttribute_Hp();
	int getStat_Endurance();
	int getStat_Strength();
	int getStat_Intelligence();
	int getStat_Agility();

	void setClass(int);
	int getClass();

	void setLevel(float);
	float getLevel();

	void setExp(float);
	float getExp();

};

#endif	/* _PLAYER_H */