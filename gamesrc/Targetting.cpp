//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"


bool AppFramework::setTarget(int x, int y)
{
	if( login )
	{
		player.target.type=0;
		player.target.id=0;

		for(int i=0;i<playersdata.size();i++)
		{	
			if( playersdata.at( player.vetpos )->characterAttributes.id != -1 )
			{

				if( ( x + camera.left > playersdata.at( player.vetpos )->spriteAttributes.x ) && 
					( x + camera.left < playersdata.at( player.vetpos )->spriteAttributes.x + 32 ) && 
					( y + camera.top >  playersdata.at( player.vetpos )->spriteAttributes.y ) && 
					( y + camera.top <  playersdata.at( player.vetpos )->spriteAttributes.y + 50 ) )
				{
					if( (player.plane == playersdata.at( player.vetpos )->characterAttributes.plane) )
					{
						player.TargetNameStr = playersdata.at( player.vetpos )->characterAttributes.username;
						player.target.type = 1;
						player.target.id = playersdata.at( player.vetpos )->characterAttributes.id;
						return true;
					}
				}

			}
		}

		return false;
	}

	return false;
}
