//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//#include "AppFramework.h"
#include "Gear.h"
#include "Item.h"

void Gear::Init()
{
	head=0;
	chest=0;
	legs=0;
	feet=0;
	weapon=0;
}

bool Gear::Equip( int item, Stats& playerstats )
{
	Item itemeq,itemuneq;

	switch ( itemeq.type )
	{
	
	case 1:
	{

		switch( itemeq.part )
		{

			case 2:	
			{
				if( chest != 0 )
				{
					if ( item != chest )
					{
						playerstats.Endurance -= itemuneq.Endurance;
						playerstats.Strength -= itemuneq.Strength;
						playerstats.Agility -= itemuneq.Agility;
						playerstats.Intelligence -= itemuneq.Intelligence;
			
						playerstats.Endurance += itemeq.Endurance;
						playerstats.Strength += itemeq.Strength;
						playerstats.Agility += itemeq.Agility;
						playerstats.Intelligence += itemeq.Intelligence;

						//cout <<"chest endurance:" << itemeq.Endurance << endl;

						chest = itemeq.id;
					
						playerstats.SetAttributes();

						return true;
					}
					else
						return false;
				}
				else
				{
					playerstats.Endurance += itemeq.Endurance;
					playerstats.Strength += itemeq.Strength;
					playerstats.Agility += itemeq.Agility;
					playerstats.Intelligence += itemeq.Intelligence;

					chest = itemeq.id;

					playerstats.SetAttributes();

					return true;
				}
			}break;


			case 4:
			{
				
				if ( weapon != 0 )
				{
					
					if( item != weapon )
					{
						playerstats.Endurance -= itemuneq.Endurance;
						playerstats.Strength -= itemuneq.Strength;
						playerstats.Agility -= itemuneq.Agility;
						playerstats.Intelligence -= itemuneq.Intelligence;
			
						playerstats.Endurance += itemeq.Endurance;
						playerstats.Strength += itemeq.Strength;
						playerstats.Agility += itemeq.Agility;
						playerstats.Intelligence += itemeq.Intelligence;

						playerstats.WeaponDamage = itemeq.damage;

						

						weapon = itemeq.id;

						playerstats.SetAttributes();

						return true;
					}
					else
						return false;
				}
				else
				{
			
					playerstats.Endurance += itemeq.Endurance;
					playerstats.Strength += itemeq.Strength;
					playerstats.Agility += itemeq.Agility;
					playerstats.Intelligence += itemeq.Intelligence;

					playerstats.WeaponDamage = itemeq.damage;

					

					weapon = itemeq.id;

					playerstats.SetAttributes();

					

					return true;
				}

			

			}break;	



		}

	}break;
	
	}


	return false;
}
