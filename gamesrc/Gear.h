//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef _GEAR_H
#define	_GEAR_H

#include "Stats.h"

class Gear
{

public:

	int head;
	int chest;
	int legs;
	int feet;
	int weapon;

	void Init();

	bool Equip( int, Stats& );
};

#endif	/* _PLAYER_H */