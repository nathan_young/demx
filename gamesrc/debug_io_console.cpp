//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

//This console I/O redirection implementation is
//Windows specific.
#include <Windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>

#include "debug_io_console.h"

#include "debug.h"

#ifndef _USE_OLD_IOSTREAMS
	using namespace std;
#endif

static const WORD MAX_CONSOLE_LINES = 500;

#if defined _DEBUG || defined DEBUG_OVERRIDE
#ifdef CONTITLE_OVERRIDE
void RedirectIOToDebugConsole(const char *title)
#else
void RedirectIOToDebugConsole()
#endif
{
	int console_handle;
	int std_handle;
	CONSOLE_SCREEN_BUFFER_INFO console_info;
	FILE *file_pointer;

	AllocConsole();
	
#ifdef CONTITLE_OVERRIDE
	SetConsoleTitle(title);
#endif

	//Allocate enough space for text scrolling.
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),
		&console_info);

	console_info.dwCursorPosition.Y = MAX_CONSOLE_LINES;

	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE),
		console_info.dwSize);

	//Redirect unbuffered output to console.
	std_handle = (LONG)GetStdHandle(STD_OUTPUT_HANDLE);
	console_handle = _open_osfhandle(std_handle, _O_TEXT);

	file_pointer = _fdopen(console_handle, "w");

	*stdout = *file_pointer;

	setvbuf(stdout, NULL, _IONBF, 0);

	//Redirect STDERR to console.
	std_handle = (LONG)GetStdHandle(STD_ERROR_HANDLE);
	console_handle = _open_osfhandle(std_handle, _O_TEXT);
	file_pointer = _fdopen(console_handle, "w");

	*stderr = *file_pointer;

	setvbuf(stderr, NULL, _IONBF, 0);

	//Force stream functions (cout, cin, etc) redirect to debug console.
	ios::sync_with_stdio();
}

//-----------------------------------
//>> ChangeConsoleTextColor
//>>
//>> Function only gets defined if 
//>> building in debug beacuse debug
//>> console is not shown in release.
//-----------------------------------
void ChangeConsoleTextColor(unsigned long colorcode)
{
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
	Assert(console);

	SetConsoleTextAttribute(console, colorcode);
}
#endif