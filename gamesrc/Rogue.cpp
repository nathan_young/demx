//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Rogue.h"

ClassRogue::ClassRogue()
{
	srand( (unsigned)time(NULL) ); 

	isLacerateCD = false;
	isColdBloodCD = false;
	isStunCD = false;

    LacerateCD    = 1000;
	ColdBloodCD   = 26000;
	StunCD		  = 60000;
	StealthCD     = 6000;
}

void ClassRogue::StartLacerateCD()
{
	isLacerateCD = true;
	TimerLacerateCD.start();
}

void ClassRogue::StartColdBloodCD()
{
	isColdBloodCD = true;
	TimerColdBloodCD.start();
}
	
/*=====================
	base abilities 	
=====================*/

int ClassRogue::CheckCooldowns()
{	
	if( TimerLacerateCD.get_ticks() > LacerateCD )
	{
		isLacerateCD = false;
		TimerLacerateCD.stop();
		return 1;
	}
	if( TimerColdBloodCD.get_ticks() > ColdBloodCD )
	{
		isColdBloodCD = false;
		TimerColdBloodCD.stop();
		return 2;
	}
	if( TimerStunCD.get_ticks() > StunCD )
	{
		isStunCD = false;
		TimerStunCD.stop();
		return 3;
	}
	if( TimerStealthCD.get_ticks() > StealthCD )
	{
		isStealthCD = false;
		TimerStealthCD.stop();
		return 4;
	}

	return 0;
}

string ClassRogue::getCooldownRemaining( int skill )
{
	float remain;
	std::string s="";

	switch( skill )
	{
		case 1:
		{
			if( TimerLacerateCD.get_ticks() > 0 )
			{
				remain = LacerateCD - TimerLacerateCD.get_ticks();
				s = FloatConversion( remain );
			}
		}break;

		case 2:
		{
			if( TimerColdBloodCD.get_ticks() > 0 )
			{
				remain = ColdBloodCD - TimerColdBloodCD.get_ticks();		
				s = FloatConversion( remain );
			}
		}break;

		case 3:
		{
			if( TimerStunCD.get_ticks() > 0 )
			{
				remain = StunCD - TimerStunCD.get_ticks();
				s = FloatConversion( remain );
			}
		}break;

		case 4:
		{
			if( TimerStealthCD.get_ticks() > 0 )
			{
				remain = StealthCD - TimerStealthCD.get_ticks();
				s = FloatConversion( remain );
			}
		}break;	
	}

	return s;
}

string ClassRogue::FloatConversion(float remain)
{
	int m,aux,c;
	char value[20];
	std::string s;
	s="";

	if( remain < 10000 )
	{	
		m = remain / 1000;
		aux = remain - m * 1000;
		c = aux / 100;
		_itoa_s(m,value,10);
		s = s + value;
		s = s + ".";
		_itoa_s(c,value,10);
		s = s + value;
	}
	else
	{
		remain = remain / 1000;
		_itoa_s(remain,value,10);
		s = s + value;
	}

	return s;
}

char* ClassRogue::DefaultButton(int ability)
{
	switch( ability )
	{
		case 1:
			return DATA_GUI_BUTTON_ROGUE_ABILITY1_DEFAULT_PNG;
		break;
		case 2:
			return DATA_GUI_BUTTON_ROGUE_ABILITY2_DEFAULT_PNG;
		break;
	}

	return "";
}