//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __APPFRAMEWORK_H__

#define __APPFRAMEWORK_H__

#pragma warning(disable:4244) //Conversion from time_y to uint
#pragma warning(disable:4996) //POSIX name for item is deprecated
#pragma warning(disable:4244) //Conversion from int to float
#pragma warning(disable:4251) //MySQL dll-interface warning
#pragma warning(disable:4800) //Forcing int to boolean value.

//Never change Microsoft header files.
#include <Windows.h>
#define _WINSOCK2API_

#include <WinSock2.h>

#include <process.h>

//#include "D3D9Graphics.h"
#include "Draw.h"
#include "Audio.h"
#include "Sprite.h"
#include "GameSprites.h"
#include "SharedHeader.h"
#include "GameData.h"
#include <fstream>
#include "Packet.h"
#include "Stats.h"
#include "Item.h"
#include "PlayerBase.h"
#include "Gui_hpbar.h"
#include "Collision.h"
#include "Player.h"
#include "external\boost\boost\function.hpp"
#include "external\boost\boost\bind.hpp"

/*==================
	GUI
==================*/
#include "DemxGUI\GuiManager.h"



#include "Questing.h"

#include <string>
#include <sstream>



#include "Input.h"



#include <stdlib.h>
#include <memory.h>
#include <tchar.h>

#include "debug.h"
#include "System.h"
#include "Win32/System_Win32.h"
#include "Platform.h"

#include "Timer.h"

using namespace std;



class AppFramework
{

private:

	// camera
	RECT camera;

	//Text Font.
	ID3DXFont		*m_pFont;
	ID3DXFont		*s_pFont;

	// players data
	//Avatar players[5];
	Avatar creeps[2];

	std::vector<std::shared_ptr<Character>> playersdata;

	GameObjectSprites gameSprites;


	int change_status;


	/*========================
		RENDERING 
	========================*/
	RECT TextPos;
	RECT PlayerNamePos, PlayerCurHpPos, PlayerHpPos, TargetNamePos, TargetCurHpPos, TargetHpPos;
	RECT chat[5];
	RECT ClassPos;
	RECT ability[4];








	RECT ClipIcons[5];

	DamageDisplay dam[5];
	void InsertValue( uint16_t, int, int, int );

	ChatLog log[5];





public:
	/**
	* Constructor
	**/
	AppFramework();

	/*=======================================================
		Initialize the game. Get everything ready to run.
		---------------------
		@PARAM
		HWND: Handle to window.
	========================================================*/
	HRESULT GameInit(HWND winHandle);

	/**
	* Render.
	**/
	void GameRender();

	//Networking   _Network;

	/**
	* Main game loop.
	**/
	void GameLoop();

	/**
	* Cleanup after the game ends.
	**/
	void GameCleanup();

	//Flag to determine if game is running.
	bool GameRunning;

	/*===================================
		ProcessInput
	===================================*/
	void ProcessInput( HWND, UINT, WPARAM, LPARAM );

	/*==========================
		login 
	==========================*/
	bool login;

	/*===================
		NETWORKING
	===================*/
public:
	int RecMessagePort();
private:
	// true - connected false - not connected
	bool m_bIsConnected; 
	string m_sServerIPAddress;
	int m_iServerPort;
	SOCKET conn; // socket connected to server
	bool IsConnected(){return m_bIsConnected;}
	int SendMessagePort( PortPacket );
	void Init(string sIpAddress, int iPort);

protected:
	Renderer		_Graphics;





	/*====================================
				DemxGUI
	====================================*/
public:
	DemxGUI::GuiManager uiMgr;
	
private:
	DemxGUI::PushButton* ability1;
	DemxGUI::PushButton* ability2;
	DemxGUI::PushButton* ability3;
	DemxGUI::PushButton* ability4;

	DemxGUI::TextBoxInput* loginbox;
	DemxGUI::TextBoxInput* chatbox;

	void InitUiElements();




	LPDIRECT3DSURFACE9 gameworld; //Buffer for map.
	LPDIRECT3DSURFACE9 arenaworld; //Buffer for arena map.
	LPDIRECT3DSURFACE9 introscreen;
	LPDIRECT3DSURFACE9 box;
	LPDIRECT3DSURFACE9 icons;

	/*======================================
		Build the game world.
		Use the tile map to splice
		all tiles, and align 
		them on the map for rendering.
	======================================*/
	HRESULT ConstructWorld();
	HRESULT ConstructArena();


	// HUD
	Gui_hpbar gui_hpbar;

	//StaticElement gui_target_hpbar;
	
	//StaticElement gui_castbar;

	LPDIRECT3DSURFACE9 gui_hp;
	LPDIRECT3DSURFACE9 gui_cast;

	/*=========================
		ScrollScreen
	=========================*/
	void ScrollScreen();


	/*===================================
		checks the user input command
	===================================*/
	void CheckCommand( string );

	/*=========================
		collision detection 
	=========================*/
	Collision cobj;


	/*=====================
		CHARACTER
	=====================*/
	PlayerBase player;


	///===============================///
	///           Gameplay            ///
	///===============================///


	// casting
	bool casting;
	bool cast_finished;
	int cast_spell;
	Timer cast;

	/*=========================
		line of sight
	=========================*/
	bool LOS( int, int, int, int );


	/*========================================================
		This function recieves coordinates from Win32 input 
		and checks if any player or mob corrisponds to the 
		mouse position. In this case the target is setted 
		to the entity the mouse clicked.
	========================================================*/
	bool setTarget( int, int );


	/*===================	
		Use Ability
	===================*/
	int UseAbility( int );


	/*===================================
		Casting
	===================================*/
	bool AllowCast();


	/*===================================
		castSpell
	===================================*/
	void castSpell( int );






	/*======================
		utility
	=======================*/
	int ABS( int );

};

#endif //__CAPPFRAMEWORK_H__