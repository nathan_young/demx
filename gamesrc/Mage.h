//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __MAGE_H__
#define __MAGE_H__

#include <Windows.h>
#include <time.h>
#include "Timer.h"
#include "GlobalHeader.h"
#include <stdio.h>
#include <stdlib.h>
#include "GameData.h"

#include <string>
using namespace std;

class ClassMage
{

public:

	bool isFireBlastCD;
	bool isSlowCD;

	int FireBlastCD;
	int SlowCD;

	Timer TimerFireBlastCD;
	Timer TimerSlowCD;

	void StartFireBlastCD();
	void StartSlowCD();
	
	int FireBlastCastTime;

	ClassMage();
	
	int CheckCooldowns();

	string getCooldownRemaining(int skill);
	string FloatConversion(float);

	char* DefaultButton(int);

	int getCurrentCastTime();
};

#endif //__MAGE_H__