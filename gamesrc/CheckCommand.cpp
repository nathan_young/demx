//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"


bool checkIsDigit(std::string str)
{
	for(unsigned int i=0;i<str.length();i++)
	{
		if( str.at(i) != '0' && str.at(i) != '1' && str.at(i) != '2' && str.at(i) != '3' && str.at(i) != '4'
		    && str.at(i) != '5' && str.at(i) != '6' && str.at(i) != '7' && str.at(i) != '8' && str.at(i) != '9')
		{
			return false;
		}
		
	}
	return true;
}

void AppFramework::CheckCommand(string command)
{
	
	if ( command.length() > 0 )
	{	


		/// SEND CHAT
		if( ( command.length() > 1 && command.at(0) == '/' && command.at(1) == 'w')  || 
			( command.length() > 1 && command.at(0) == '/' && command.at(1) == 's' && command.at(2) == ' ')  ||
			command.at(0) != '/' )
		{
			

		//	Send( SEND_CHAT, command );
		}
		else
		{
		
			std::string s;

			/// COMMANDS
			if( command.find("/inv equip ",0) != string::npos )
			{
				s="";
				for ( unsigned int i = 11 ; i < command.length(); i++ )
					s += command.at(i);
					
				if( checkIsDigit(s) )
					if( atoi(s.c_str()) < 5)
					{
						player.InventoryEquip( atoi(s.c_str()) );
						cout << endl << "hp:"<<player.getAttribute_Hp() << endl;
						//Send( SEND_STATS, "" );
					}
					
			}
			else if( command.find("/inv delete ",0) != string::npos )
			{
				s="";
				for ( unsigned int i = 12 ; i < command.length(); i++ )
					s += command.at(i);	

				if( checkIsDigit(s) )
					if( atoi(s.c_str()) < 5)
						player.InventoryDelete( atoi(s.c_str()) );
			}
			else if( command.find("/inv show",0) != string::npos )
			{
			}
			else if( command.find("/char show",0) != string::npos )
			{
			}
			else if( command.find("/join",0) != string::npos )
			{
//				Player p;
//				p.id = client_id;
//				SendMessagePort(p,6, command.c_str());
			}
			else if( command.find("/quest",0) != string::npos )
			{
			//	if( command.at(6)=='1' )
			//		_Quests.StartQuest(0);
			}
			else if( command.find("/party invite ",0) != string::npos )
			{
				s="";
				for ( unsigned int i = 13 ; i < command.length(); i++ )
					s += command.at(i);	
			}
			else if( command.find("/rogue",0) != string::npos )
			{
				player.pclass = 1;
			}
			else if( command.find("/mage",0) != string::npos )
			{
				player.pclass = 2;
			}
				
		}
	}
}