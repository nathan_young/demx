//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"

/*=======================================
		Use Ability
=======================================*/
int AppFramework::UseAbility(int ability)
{
	PortPacket p;

	switch( player.pclass )
	{
		case 1:
		{
			switch( ability )
			{
				case 1:
				{	
					if( !player.Rogue.isLacerateCD )
					{
						player.Rogue.StartLacerateCD();
						ability1->ChangeImage( _Graphics, DATA_GUI_BUTTON_ROGUE_ABILITY1_CD_PNG );
						p.msg = SEND_USEABILITY;
						p.ability = 10;
						SendMessagePort( p );
					}
				}break;
				case 2:
				{
					if( !player.Rogue.isColdBloodCD )
					{
						player.Rogue.StartColdBloodCD();
						ability2->ChangeImage( _Graphics, DATA_GUI_BUTTON_ROGUE_ABILITY2_CD_PNG );
						p.msg = SEND_USEABILITY;
						p.ability = 11;
						SendMessagePort( p );
					}
				}break;
				case 3:
				{
					
				}break;
				case 4:
				{
					
				}break;
			}
		}break;

		case 2:
		{
			switch( ability )
			{
				case 1:
				{	
					if( !player.Mage.isFireBlastCD )
					{
						if( AllowCast() )
						{
							castSpell( 20 );
						}
					}
				}break;
				case 2:
				{
					
				}break;
				case 3:
				{
					if( !player.Mage.isSlowCD )
					{
						if( AllowCast() )
						{
							/*
							Mage.StartSlowCD();
							AbilityButtons[2].ChangeImage( _Graphics, DATA_GUI_BUTTON_MAGE_ABILITY3_CD_PNG );
							p.msg = SEND_USEABILITY;
							p.ability = 22;
							SendMessagePort( p );
							*/
						}
					}
				}break;
				case 4:
				{
					
				}break;
			}
		}break;

		case 3:
		{
		
		}break;
	}	

	return 0;
}