//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef _QUESTING_H
#define _QUESTING_H

#include <Windows.h>
#include <d3dx9.h>
#include <string>

using namespace std;

class Questing
{
private:

	unsigned int questnumber;

	/*=============
	Quests[]
		0 -> not started
		1 -> in progress
		2 -> concluded
	=============*/

	int Quests[2];

	int Kills[2];


	RECT Text;

public:

	Questing();
	void ResetQuests();
	void ResetKills();

	void LoadQuests();

	void StartQuest(int);
	void ConcludeQuest(int);
	
	int CheckKill(int);

	void Show(ID3DXFont *m_pFont);
};

#endif /* _QUESTING_H */