//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "GlobalHeader.h"

#include "AppFramework.h"
#include <Windowsx.h> 

//Required for resources.
#include "resource.h"
 
LRESULT CALLBACK MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

AppFramework app;

DWORD WINAPI RecMessageThread(LPVOID lpParameter)
{
	while( true )
	{
		if(app.RecMessagePort())
				break;
	}
	return 0;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Entry point and main window creation.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	HWND hWnd;
	MSG msg;
	WNDCLASSEX wcex;

	ZeroMemory(&wcex, sizeof(WNDCLASSEX));

	wcex.cbSize				= sizeof(WNDCLASSEX);
	wcex.cbClsExtra			= NULL;
	wcex.cbWndExtra			= NULL;
	wcex.hbrBackground		= (HBRUSH)GetStockObject(WHITE_BRUSH); //Paint the window background white (gets overrided by DirectX).
	wcex.hCursor			= LoadCursor(NULL, IDC_ARROW); 
	//wcex.hCursor			= LoadCursor(NULL, MAKEINTRESOURCE(IDB_PNG1));
	wcex.hIcon				= LoadIcon(NULL, IDI_APPLICATION);
	wcex.hIconSm			= LoadIcon(NULL, IDI_APPLICATION);
	wcex.hInstance			= hInstance;
	wcex.lpfnWndProc		= MsgProc;
	wcex.lpszClassName		= GAME_NAME;
	wcex.lpszMenuName		= NULL;
	wcex.style				= CS_HREDRAW | CS_VREDRAW; //Redraw the window everytime the window is moved.

	RegisterClassEx(&wcex);

	RECT wr = {0, 0, WINDOW_WIDTH, WINDOW_HEIGHT};    // set the size, but not the position
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);    // adjust the size

	hWnd = CreateWindowEx(NULL,
						  GAME_NAME,
						  GAME_NAME,
						  WS_OVERLAPPEDWINDOW,
						  0,
						  0,
						  wr.right - wr.left,
						  wr.bottom - wr.top,
						  NULL,
						  NULL,
						  hInstance,
						  NULL); //Reserved byte.

	
	if(!hWnd)
		return -1;

	ShowWindow(hWnd, SW_SHOW);
	app.GameInit(hWnd);
	Timer fps;

	unsigned int Counter = 0;
	DWORD ThreadID;
	HANDLE Handle = CreateThread(0, 0, RecMessageThread, &Counter, 0, &ThreadID);

	while( app.GameRunning )
	{
		fps.start();

		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
				app.GameRunning = false;
			
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}//End while.

		app.GameLoop();
		app.GameRender();		

        if( fps.get_ticks() < 66 )
		{
			//cout << ( 66 ) - fps.get_ticks() << endl;
            Sleep( ( 66 ) - fps.get_ticks() );
		}
	}//End while.

	app.GameCleanup();

	return 0;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Default Windows message procedure.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
LRESULT CALLBACK MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	PortPacket p;

	if( !app.uiMgr.ProcessUiEvents( hWnd, msg, wParam, app.login ) )
	{
		app.ProcessInput( hWnd, msg, wParam, lParam );
	}

	//All other input.
	switch( msg )
	{		
		case WM_DESTROY:
			PostQuitMessage(0);
		return 0;
	};

	//If we received a message that was not defined above, ignore it.
	return DefWindowProc(hWnd, msg, wParam, lParam);
}