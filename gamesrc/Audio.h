//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __AUDIO_H__
#define __AUDIO_H__

#include "dsutil.h"


class AudioEntity
{
private:
	CSoundManager *dsound;

public:

	/**
	* Constructor.
	**/
	AudioEntity();

	/**
	* Destructer.
	**/
	~AudioEntity();

	/**
	* Initialize directsound.
	* ---------------------
	* @PARAM
	*	HWND: Handle to window.
	**/
	int Init(HWND winhandle);

	/**
	* Load a sound from a file.
	* ---------------------
	* @PARAM
	*	char *: Name of file to load.
	**/
	CSound *LoadSound(char * Filename);

	//SoundEvents.
	void PlaySound(CSound *sound);
	void LoopSound(CSound *sound);
	void StopSound(CSound *sound);
};

#endif //__AudioEntity_H__