//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include <sstream>

//Convert int to string.
template <class T>
inline std::string IntToString(const T &t)
{
	std::stringstream out;
	out << t;

	return out.str();
}

#endif //__UTILITIES_H__