#pragma once
#include "entity.h"

class Character : public Entity
{

struct Attributes
{
	//
  	uint8_t id; // 0-255
	
	std::string username;

	uint8_t plane; // what istance is the player in  -- default = 0
	int8_t xvel;
	int8_t yvel;

	int vetpos;

	int16_t curhp;
	int16_t hp;

	Target target;
	uint8_t combat;

	bool stunned;
	bool slowed;
	bool stealthed;

	float shield;
	int scorch_effect;
	int conflag_effect;
};

public:
	Character(void);
	~Character(void);

	Attributes characterAttributes;

};

