//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"

void AppFramework::InitUiElements()
{
	PortPacket p;

	uiMgr.InitGraphicsDevice( _Graphics.ExposePrivateDevice(), _Graphics, m_pFont );

	ability1 = new DemxGUI::PushButton();
	ability1->setPosition( 320, 500 );
	ability1->setPanel( 1 );
	ability1->RegisterEvent( boost::bind(&AppFramework::UseAbility, this , 1) );
	uiMgr.addButton( ability1 );

	ability2 = new DemxGUI::PushButton();
	ability2->setPosition( 365, 500 );
	ability2->setPanel( 1 );
	ability2->RegisterEvent( boost::bind(&AppFramework::UseAbility, this , 2) );
	uiMgr.addButton( ability2 );

	ability3 = new DemxGUI::PushButton();
	ability3->setPosition( 410, 500 );
	ability3->setPanel( 1 );
	ability3->RegisterEvent( boost::bind(&AppFramework::UseAbility, this , 3) );
	uiMgr.addButton( ability3 );

	ability4 = new DemxGUI::PushButton();
	ability4->setPosition( 455, 500 );
	ability4->setPanel( 1 );
	ability4->RegisterEvent( boost::bind(&AppFramework::UseAbility, this , 4) );
	uiMgr.addButton( ability4 );

	loginbox = new DemxGUI::TextBoxInput();
	loginbox->setPosition( 385, 275 );
	loginbox->setPanel( 0 );
	loginbox->activate();
	p.msg = 0;
	loginbox->RegisterEvent( boost::bind(&AppFramework::SendMessagePort, this , p) );
	uiMgr.addTextBox( loginbox );

	chatbox = new DemxGUI::TextBoxInput();
	chatbox->setPosition( 5, 540 );
	chatbox->setPanel( 1 );
	chatbox->deactivate();
	p.msg = 4;
	chatbox->RegisterEvent( boost::bind(&AppFramework::SendMessagePort, this , p) );
	uiMgr.addTextBox( chatbox );
}




