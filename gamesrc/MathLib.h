//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __MATH_H__
#define __MATH_H__

#define INLINE __forceinline

//On 64-bit machines 64 and 32 flags are defined.
//Need a better way of checking this.
#ifdef _WIN32 || _WIN64
	#ifdef _WIN32
		//Naked attribute is x86 only.
		#define ASM __forceinline __declspec(naked) __fastcall
		#define ASMFUNC __forceinline __fastcall
	#else // defined _WIN64
		#define ASM __forceinline __fastcall
		#define ASMFUNC __forceinline __fastcall
	#endif
#endif

static const float Pi = 3.14159265358979323846f;
static const float Two_Pi = 2.0 * Pi;

template<typename T>
INLINE T Min(T a, T b)
{
	return(a < b ? a : b);
}


template<typename T>
INLINE T Max(T a, T b)
{
	return(a > b ? a : b);
}

template<typename T>
INLINE T Sqr(T x)
{
	return(x * x);
}

template<typename T>
INLINE T Cube(T x)
{
	return(x * x * x);
}

template<typename T>
INLINE T Abs(T x)
{
	T z;
	T shift_size;

	//Generally 0 -> 8 bits - 1 to drop sign bit.
	shift_size = sizeof(T) * 8 - 1;
	z = x >> shift_size;

	return(z ^ (x + z));
}

double ASMFUNC Sqrt(const double x)
{
	__asm fld qword ptr [esp + 4] ;Load floating value onto stack.
	__asm fsqrt					  ;Calculate floating square root.
	__asm ret 8					  ;Return parameters off stack, esp + 4 = 8 bits (byte).
}
#endif /*__MATH_H__*/