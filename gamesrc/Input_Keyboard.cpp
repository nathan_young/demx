//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "Input.h"

/*==============================================================
	int Input::InitKeyboard(HWND winhandle)
	---------------------
	Initialize keyboard.
==============================================================*/
int Input::InitKeyboard(HWND winhandle)
{
	HRESULT hr;

	//Set data format.
	hr = dikeyboard->SetDataFormat(&c_dfDIKeyboard);
	if(hr != FSTATUS_HEALTHY)
		return 0;

	//Set cooperative level.
	hr = dikeyboard->SetCooperativeLevel(winhandle, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	if(hr != FSTATUS_HEALTHY)
		return 0;

	//Aquire keyboard.
	hr = dikeyboard->Acquire();
	if(hr != FSTATUS_HEALTHY)
		return 0;

	return 1;
}


/*==============================================================
	void Input::PollKeyboard()
	---------------------
	Poll for keyboard input.
==============================================================*/
void Input::PollKeyboard()
{
	//Keys stored in 256 char array.
	dikeyboard->GetDeviceState(sizeof(keys), (LPVOID)&keys);
}


/*==============================================================
	int Input::KeyDown(int key)
	---------------------
	Check to see if a key is down.
==============================================================*/
int Input::KeyDown(int key)
{
	return(keys[key] & 0x80);
}


/*==============================================================
	void Input::KillKeyboard()
	---------------------
	Kill keyboard input.
==============================================================*/
void Input::KillKeyboard()
{
	dikeyboard->Unacquire();
	dikeyboard->Release();
	dikeyboard = NULL;
}