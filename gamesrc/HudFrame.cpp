#include "HudFrame.h"

DemxGUI::HudFrame::HudFrame()
{
	x = 0;
	y = 0;
	width = 0;
	height = 0;
}

DemxGUI::HudFrame::~HudFrame()
{

}

int DemxGUI::HudFrame::Init( IDirect3DDevice9 *id3ddevice, Renderer _Graphics, char* path, int width, int height )
{
	HRESULT hResult;
	lpd3ddev = id3ddevice;

	//Create sprite handler object.
	hResult = D3DXCreateSprite(lpd3ddev, &spriteHandler);
	if(hResult != D3D_OK)
	{
		return 0;
	}//End if.

	lpTexHud = _Graphics.LoadTexture( path, D3DCOLOR_XRGB(0, 255, 255));
			
	this->width = width;
	this->height = height;

	if(lpTexHud == NULL)
	{
		return 0;
	}//End if.

	return 1;
}

void DemxGUI::HudFrame::SetPosition( int x, int y )
{
	this->x = x;
	this->y = y;
}

void DemxGUI::HudFrame::Show( RECT )
{
	spriteHandler->Begin(D3DXSPRITE_ALPHABLEND);

	D3DXVECTOR3 position(
		(float) x, 
		(float) y,
		0);

	RECT srcRect;

	srcRect.left   = 0;
	srcRect.top    = 0;
	srcRect.right  = srcRect.left + width;
	srcRect.bottom = srcRect.top + height;

	spriteHandler->Draw(lpTexHud, &srcRect, NULL, &position, D3DCOLOR_XRGB(255, 255, 255));

	spriteHandler->End();
}