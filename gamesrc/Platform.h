//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// http://sourceforge.net/apps/mediawiki/predef/index.php?title=Main_Page
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#ifdef _WIN32
//If windef.h is included we need to undef this to redef this... sigh.
#pragma once
//#pragma message("\tWindef.h is included and WIN32 is defined... undefining WIN32\n")
#undef WIN32

	#define WIN32 1
#endif

#if defined (WIN32) && defined _MSC_VER
	#define COMPILER_MSVC 1
#endif

#if defined MACOS_X || defined __APPLE__
	#define OSX 1
#endif

#ifdef __linux__
	#define LINUX 1
#endif

#ifdef _DEBUG
#define DEBUG 1
#endif

#ifndef NULL
#define NULL 0
#endif

#endif /*__PLATFORM_H__*/