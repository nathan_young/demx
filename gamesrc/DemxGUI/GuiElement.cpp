//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "GuiElement.h"
#include "../Sprite.h"

DemxGUI::GuiElement::GuiElement()
{
	path = NULL;
	width  = 0;
	height = 0;
	Action = NULL;
}

void DemxGUI::GuiElement::setPosition( int x, int y )
{
	this->x = x;
	this->y = y;
}

void DemxGUI::GuiElement::setPanel( int panel )
{
	this->panel = panel;
}

int DemxGUI::GuiElement::getPanel()
{
	return this->panel;
}

bool DemxGUI::GuiElement::isPressed(int x, int y)
{
	if( ( x > this->x && x < (this->x + width ) ) &&
		( y > this->y && y < (this->y + height) ) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

void DemxGUI::GuiElement::Cleanup()
{
	if(image != NULL)
		image->Release();
}

void DemxGUI::GuiElement::RegisterEvent( boost::function <void()> function )
{
	Action = function;
}

void DemxGUI::GuiElement::RunAction()
{
	if( Action != NULL )
		Action();
}

void DemxGUI::GuiElement::ChangeImage( Renderer renderer, char * path )
{
	this->path = path;
	LoadTexture( renderer );
}

void DemxGUI::GuiElement::LoadTexture( Renderer renderer )
{
	image = renderer.LoadTexture( path, D3DCOLOR_XRGB(255, 255, 255));	
}

int DemxGUI::GuiElement::getWidth()
{
	return this->width;
}

int DemxGUI::GuiElement::getHeight()
{
	return this->height;
}

int DemxGUI::GuiElement::getX()
{
	return this->x;
}

int DemxGUI::GuiElement::getY()
{
	return this->y;
}

LPDIRECT3DTEXTURE9 DemxGUI::GuiElement::getImage()
{
	return this->image;
}