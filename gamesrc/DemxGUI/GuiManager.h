#pragma once

#include "Draw.h"
#include <Windows.h>
#include <vector>

#include "PushButton.h"
#include "TextBoxInput.h"

namespace DemxGUI
{

class GuiManager
{
	std::vector<PushButton*> buttonVector;
	std::vector<TextBoxInput*> textboxVector;

	int panel;
	
	IDirect3DDevice9 *id3ddevice;
	Renderer renderer; 
	LPD3DXSPRITE spriteHandler;
	ID3DXFont* font;

public:
	GuiManager(void);
	~GuiManager(void);

	int InitGraphicsDevice( IDirect3DDevice9 *, Renderer, ID3DXFont* );
	void ShowGuiElements();

	void addButton( PushButton* );
	void addTextBox( TextBoxInput* );

	bool ProcessUiEvents( HWND, UINT, WPARAM, int );

	void setPanel( int );

	void CleanUp();
};

}