//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DEMXGUI_HUDFRAME_H__
#define __DEMXGUI_HUDFRAME_H__

#include "Draw.h"


#include <string>
using namespace std;

namespace DemxGUI
{

	class HudFrame
	{

		//Direct3D device.
		LPDIRECT3DDEVICE9 lpd3ddev;
		//Texture to hold sprite data.
		LPDIRECT3DTEXTURE9 lpTexPlayer;

		//Sprite handler.
		LPD3DXSPRITE spriteHandler;

		int x;
		int y;
		int width;
		int height;

	public:

		int Init(IDirect3DDevice9 *,Renderer,int, int, char * );
		void SetPosition(int,int);
		void Show(RECT);


	};

}


#endif //__DEMXGUI_PUSHBUTTON_H__