//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DEMXGUI_GUIELEMENT_H__
#define __DEMXGUI_GUIELEMENT_H__

#include "external\boost\boost\function.hpp"
#include "external\boost\boost\bind.hpp"

#include <Windows.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "GameData.h"
#include "../Draw.h"
#include <string>
using namespace std;

namespace DemxGUI
{
	class GuiElement
	{

	protected:
		// image path
		char * path;	
		// pointer to function to be run
		boost::function <void()> Action;

		LPDIRECT3DTEXTURE9 image;
		int x;
		int y;
		int width;
		int height;
		int panel;

	public:

		GuiElement();

		void setPosition( int, int );
		void setPanel( int );
		int getPanel();

		int getWidth();
		int getHeight();
		int getX();
		int getY();
		LPDIRECT3DTEXTURE9 getImage();

		bool isPressed(int,int);
	
		void ChangeImage( Renderer, char * );
		virtual void LoadTexture( Renderer renderer );

		void RegisterEvent( boost::function <void()> );
		void RunAction();

		virtual void Cleanup();
	};
}


#endif //__DEMXGUI_GUIELEMENT_H__