//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DEMXGUI_TEXTBOXINPUT_H__
#define __DEMXGUI_TEXTBOXINPUT_H__

//#include "external\boost\boost\function.hpp"
//#include "external\boost\boost\bind.hpp"

#include "GuiElement.h"
#include <Windows.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "GameData.h"
#include "../Draw.h"
#include <string>
using namespace std;

namespace DemxGUI
{
	class TextBoxInput : public GuiElement
	{
	
	private:
		
		LPDIRECT3DTEXTURE9 lpTextureActive;
		LPDIRECT3DTEXTURE9 lpTextureDeactive;
		std::string text;
	
	public:

		TextBoxInput();

		void addChar(char);
		void deleteChar();
		void deleteText();
		string getText();

		void handleInput( UINT, WPARAM );

		void activate();
		void deactivate();
		bool isActive();

		bool active;

		void Cleanup();
		void LoadTexture( Renderer renderer );
	};
}


#endif //__DEMXGUI_TEXTBOX_H__