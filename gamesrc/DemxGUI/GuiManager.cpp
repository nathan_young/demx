#include "GuiManager.h"


DemxGUI::GuiManager::GuiManager(void)
{
	buttonVector.clear();
	textboxVector.clear();
}


DemxGUI::GuiManager::~GuiManager(void)
{

}

void DemxGUI::GuiManager::addButton( PushButton *button )
{
	button->LoadTexture( renderer );
	buttonVector.push_back( button );
}

void DemxGUI::GuiManager::addTextBox( TextBoxInput *textbox )
{
	textbox->LoadTexture( renderer );
	textboxVector.push_back( textbox );
}


int DemxGUI::GuiManager::InitGraphicsDevice( IDirect3DDevice9 *id3ddevice, Renderer _Graphics, ID3DXFont* font )
{
	HRESULT hResult;

	this->id3ddevice = id3ddevice;
	
	hResult = D3DXCreateSprite(id3ddevice, &spriteHandler);
	if(hResult != D3D_OK)
		return 0;
	
	this->renderer = _Graphics;
	this->font = font;

	return 1;
}

bool DemxGUI::GuiManager::ProcessUiEvents( HWND hWnd, UINT msg, WPARAM wParam, int panel )
{
	unsigned int i;
	POINT mouse;
	GetCursorPos(&mouse);
	ScreenToClient(hWnd, &mouse); 
	int x = mouse.x;
	int y = mouse.y;

	if( msg == WM_LBUTTONDOWN )
	{
		for( i=0; i<buttonVector.size(); i++ )
		{
			if( panel == buttonVector[i]->getPanel() )
			{
				if( buttonVector[i]->isPressed( x, y ) )
				{
					buttonVector[i]->RunAction();
					return true;
				}
			}	
		}
	}

	for( i=0; i<textboxVector.size(); i++ )
	{
		if( panel == textboxVector[i]->getPanel() )
		{
			if( msg == WM_LBUTTONDOWN )
			{
				if( textboxVector[i]->isPressed( x, y ) )
				{	
					if( !textboxVector[i]->isActive() )
						textboxVector[i]->activate();
					return true;
				}
				else 
				{
					textboxVector[i]->deactivate();
				}
			}
			if( msg == WM_CHAR && wParam == 0x0D )
			{
				if( textboxVector[i]->isActive() )
				{
					textboxVector[i]->RunAction();
					textboxVector[i]->deactivate();
					textboxVector[i]->deleteText();
					return true;
				}
				else
				{
					if( panel == 1 )
						textboxVector[i]->activate();
				}
			}
			if( textboxVector[i]->isActive() )
			{
				textboxVector[i]->handleInput( msg, wParam );
				return true;
			}
		}
	}
	
	return false;
}

void DemxGUI::GuiManager::setPanel( int panel )
{
	this->panel = panel;
}

void DemxGUI::GuiManager::ShowGuiElements()
{
	RECT srcRect;
	int i;

	for( i=0; i<buttonVector.size(); i++ )
	{
		if( buttonVector[i]->getPanel() == panel )
		{
			spriteHandler->Begin(D3DXSPRITE_ALPHABLEND);

			D3DXVECTOR3 position( (float) buttonVector[i]->getX(), (float) buttonVector[i]->getY(), 0);

			srcRect.left   = 0;
			srcRect.top    = 0;
			srcRect.right  = srcRect.left + buttonVector[i]->getWidth();
			srcRect.bottom = srcRect.top + buttonVector[i]->getHeight();

			spriteHandler->Draw( buttonVector[i]->getImage(), &srcRect, NULL, &position, D3DCOLOR_XRGB(255,255,255) );

			spriteHandler->End();
		}
	}
	
	for( i=0; i<textboxVector.size(); i++ )
	{
		if( textboxVector[i]->getPanel() == panel )
		{
			
			spriteHandler->Begin(D3DXSPRITE_ALPHABLEND);

			D3DXVECTOR3 position(
				(float) textboxVector[i]->getX(), 
				(float) textboxVector[i]->getY(),
				0);

			srcRect.left   = 0;
			srcRect.top    = 0;
			srcRect.right  = srcRect.left + textboxVector[i]->getWidth();
			srcRect.bottom = srcRect.top + textboxVector[i]->getHeight();

			spriteHandler->Draw(textboxVector[i]->getImage(), &srcRect, NULL, &position, D3DCOLOR_XRGB(255 ,255 , 255));

			spriteHandler->End();
		
			RECT fontpos = { textboxVector[i]->getX()+3, textboxVector[i]->getY()+3, 0, 0 };

			font->DrawTextA( 
				NULL, 
				textboxVector[i]->getText().c_str(), 
				-1, 
				&fontpos, 
				DT_LEFT | DT_NOCLIP, 
				D3DXCOLOR(255, 255, 255, 1.0f) );
		}
	}
}

void DemxGUI::GuiManager::CleanUp()
{
	for(unsigned int i=0; i< buttonVector.size(); i++ )
	{
		buttonVector[i]->Cleanup();
		delete buttonVector[i];
	}
	for(unsigned int i=0; i< textboxVector.size(); i++ )
	{
		textboxVector[i]->Cleanup();
		delete textboxVector[i];
	}
}