//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DEMXGUI_PUSHBUTTON_H__
#define __DEMXGUI_PUSHBUTTON_H__

//#include "external\boost\boost\function.hpp"
//#include "external\boost\boost\bind.hpp"

#include "GuiElement.h"
#include <math.h>
#include <Windows.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "GameData.h"
#include "../Draw.h"
#include <string>
using namespace std;

namespace DemxGUI
{
	class PushButton : public GuiElement
	{

	public:

		PushButton();
	};
}


#endif //__DEMXGUI_PUSHBUTTON_H__