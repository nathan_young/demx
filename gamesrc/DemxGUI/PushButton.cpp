//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "PushButton.h"

DemxGUI::PushButton::PushButton()
{
	path = DATA_GUI_BUTTON_DEFAULT_PNG;
	width  = 45;
	height = 45;
	Action = NULL;
}
