//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "TextBoxInput.h"
#include "../Sprite.h"

DemxGUI::TextBoxInput::TextBoxInput()
{
	text="";
	active = false;
	width  = 300;
	height = 34;
	image = NULL;
	lpTextureActive = NULL;
	lpTextureDeactive = NULL;
}

void DemxGUI::TextBoxInput::addChar(char c)
{
	text += c;
}
void DemxGUI::TextBoxInput::deleteChar()
{
	if( text.length() != 0 )
		text.erase( text.length() - 1 );
}
void DemxGUI::TextBoxInput::deleteText()
{
	text="";
}

void DemxGUI::TextBoxInput::handleInput( UINT msg, WPARAM wParam )
{
	if( msg == WM_CHAR && wParam != 0x0D )
	{
		switch (wParam)
		{
			// backspace input
			case 0x08:	
				deleteChar();
			break;
			
			// character input
			default:
				addChar( wParam );
			break;
		};
	}
}

string DemxGUI::TextBoxInput::getText()
{
	return text;
}

void DemxGUI::TextBoxInput::activate()
{
	active = true;
	
	if( image != NULL )
		image = lpTextureActive;
}
void DemxGUI::TextBoxInput::deactivate()
{
	active = false;
	
	if( image != NULL )
		image = lpTextureDeactive;
}
bool DemxGUI::TextBoxInput::isActive()
{
	return active;
}

/*=================================================
	void Sprite::Cleanup()
	----------------
	Cleanup sprites when program ends.
=================================================*/
void DemxGUI::TextBoxInput::Cleanup()
{
	if( image == lpTextureActive )
	{
		image->Release();
		lpTextureDeactive->Release();
	}
	else
	{
		image->Release();
		lpTextureActive->Release();
	}
}

void DemxGUI::TextBoxInput::LoadTexture( Renderer renderer )
{	
	lpTextureActive   = renderer.LoadTexture( DATA_GUI_TEXTBOX_PNG, D3DCOLOR_XRGB(0, 255, 255));	
	lpTextureDeactive = renderer.LoadTexture( DATA_GUI_TEXTBOX_DEACTIVE_PNG, D3DCOLOR_XRGB(0, 255, 255));	

	if( active )
		image = lpTextureActive;
	else
		image = lpTextureDeactive;
}