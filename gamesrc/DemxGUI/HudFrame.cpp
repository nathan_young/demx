//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "HudFrame.h"


int DemxGUI::HudFrame::Init(IDirect3DDevice9 *id3ddevice,Renderer _Graphics,int w, int h, char * path )
{
	HRESULT hResult;
	lpd3ddev = id3ddevice;

	//Create sprite handler object.
	hResult = D3DXCreateSprite(lpd3ddev, &spriteHandler);
	
	if(hResult != D3D_OK)
	{
		return 0;
	}



	lpTexPlayer = _Graphics.LoadTexture(path, D3DCOLOR_XRGB(255, 255, 255));

	width  = w;
	height = h;

	// ok
	return 1;
}

void DemxGUI::HudFrame::SetPosition( int x, int y )
{
	this->x = x;
	this->y = y;
}

void DemxGUI::HudFrame::Show( RECT camera )
{
	RECT srcRect;

	spriteHandler->Begin(D3DXSPRITE_ALPHABLEND);

	D3DXVECTOR3 position( (float) x, (float) y, 0);

	srcRect.left   = 0;
	srcRect.top    = 0;
	srcRect.right  = srcRect.left + width;
	srcRect.bottom = srcRect.top + height;

	spriteHandler->Draw( lpTexPlayer, &srcRect, NULL, &position, D3DCOLOR_XRGB(255,255,255) );

	spriteHandler->End();
}
