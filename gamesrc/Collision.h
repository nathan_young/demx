//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __COLLISION_H__
#define __COLLISION_H__

#include <Windows.h>
#include "GameSprites.h"
#include "Entity.h"
#include <vector>


class Collision
{
	std::vector<std::shared_ptr<Entity>> sprites;

public:

	Collision();

	void Init( GameObjectSprites );
	
	bool CheckCollision(RECT, RECT);

	bool ObjectCollision(int,RECT);
	bool LosObjectCollision( RECT );

	

	void Cleanup();
};


#endif //__COLLISION_H__