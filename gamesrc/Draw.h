//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DRAW_H__
#define __DRAW_H__

#include <d3d9.h>
#include <d3dx9.h>

#include <string>

#include "Vector.h"

//Sick of seeing this in long 
//paramater lists ;D
typedef unsigned int uint;

class Renderer
{
protected:
	//Device context.
	LPDIRECT3D9 d3d;
	//Device.
	LPDIRECT3DDEVICE9 d3ddev;
	//Presentation parameters.
	D3DPRESENT_PARAMETERS d3dpp;
public:
	//Backbuffer.
	LPDIRECT3DSURFACE9 backbuffer;
	//Back surface.
	LPDIRECT3DSURFACE9 backSurface;
public:
	/**
	* Constructor.
	**/
	Renderer();

	/**
	* Destrcutor.
	**/
	~Renderer();

	/**
	* Initialize Direct3D9.
	* ---------------------
	* @PARAM
	*	HWND: Handle to window.
	* @PARAM
	*	INT: Width of window.
	* @PARAM
	*	INT: Height of window.
	* @PARAM
	*	BOOL: Flag to determine if window is to run in fullscreen.
	**/
	HRESULT Initialize(HWND winHandle, int iWidth, int iHeight, bool bIsFullscreen);

	/**
	* Set the backbuffer clear color
	* ---------------------
	* @PARAM
	*	float: Red value.
	* @PARAM
	*	float: Green value.
	* @PARAM
	*	float: Blue value.
	* @PARAM
	*	float: Alpha value.
	**/
	void SetClearColorTo(float fRed, float fGreen, float fBlue, float fAlpha);

	/**
	* Begin the rendering of a scene.
	**/
	void BeginRendering();

	/**
	* End the rendering of a scene.
	**/
	void EndRendering();

	/**
	* Cleanup D3D9 when program exits.
	**/
	void Cleanup();

	/**
	* Load a surface from a file.
	* ---------------------
	* @PARAM
	*	char*: Name of file to load surface from.
	* @PARAM
	*	D3DCOLOR: Transform color.
	**/
	LPDIRECT3DSURFACE9 LoadSurface(std::string Filename, D3DCOLOR transcolor);

	/**
	* Load a texture from a file.
	* ---------------------
	* @PARAM
	*	char *: Filename of texture to load.
	* @PARAM
	*	D3DCOLOR: Transform color.
	**/
	LPDIRECT3DTEXTURE9 LoadTexture(char *Filename, D3DCOLOR transcolor);
	
	/**
	* Copy the contents of the source rectangle to the destination
	* rectangle.  The source rectangle can be strecthed and 
	* filtered by the copy.
	* ---------------------
	* @PARAM
	*	LPDIRECT3DSURFACE9: Sprite in.
	* @PARAM
	*	RECT *: Source rect.
	* @PARAM
	*	LPDIRECT3DSURFACE9: backbuffer.
	* @PARAM
	*	RECT *: Destination rectangle.
	* @PARAM
	*	D3DTEXTUREFILTERTYPE:  Type of filter -- if any -- to filter source through.
	**/
	HRESULT DemxStrechRect(IDirect3DSurface9 *pSourceSurface, const RECT *pSourceRect, IDirect3DSurface9 *pDestSurface,
		const RECT *pDestRect, D3DTEXTUREFILTERTYPE Filter);


	/**
	* Create an offscreen plane.
	* ---------------------
	* @PARAM
	*	unsigned int: Width of plane.
	* @PARAM
	*	unsigned int: height of plane.
	* @PARAM
	*	D3DFORMAT: Format of plane.
	* @PARAM
	*	D3DPOOL: Memory pool type.
	* @RETURN
	*	IDIrect3DSurface9: Place to store pointer.
	* @PARAM
	*	HANDLE: Shared handle.
	**/
	HRESULT CreateOffScreenPlainSurface(UINT Width, UINT Height, D3DFORMAT Format, D3DPOOL Pool,
		IDirect3DSurface9 **ppSurface, HANDLE *pSharedHandle);

	/**
	* Draw a 2D tile.
	* ---------------------
	* @PARAM
	*	LPDIRECT3DSURFACE9: Source surface image.
	* @PARAM
	*	int: Tile number.
	* @PARAM
	*	int: Width of tile.
	* @PARAM
	*	int: Height of tile.
	* @PARAM
	*	int: Columns of tiles.
	* @PARAM:
	*	LPDIRECT3DSURFACE9: Destination surface.
	* @PARAM
	*	int: Destination X.
	* @PARAM
	*	int: Destination y.
	**/
	void DrawTile2D(LPDIRECT3DSURFACE9 SourceSurface, RECT r1, int TileWidth, int TileHeight,
		int ColumnTiles, LPDIRECT3DSURFACE9 DestSurface, int DestX, int DestY);


	/**
	* Expose private direct3d device.
	* Use this call with caution, and
	* only use when no other option
	* is present.
	* ---------------------
	* @NOTE
	*	This is only temporary.
	* @RETURN
	*	Pointer to d3ddevice.
	**/
	LPDIRECT3DDEVICE9 ExposePrivateDevice(){return d3ddev;};

	//Get the d3ddevice.
	IDirect3DDevice9 *GetDevice()
	{
		//Goodbye type saftey!
		//DO NOT DEREFRENCE THIS!
		return reinterpret_cast<IDirect3DDevice9*>(d3ddev);
	}

	//Get UncastedDevice.
	LPDIRECT3DDEVICE9 GetUncastedDevice()
	{
		return d3ddev;
	}
};

#endif //__DRAW_H__