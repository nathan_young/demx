//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __CFONTHANDLER_H__
#define __CFONTHANDLER_H__

#include <engine/renderer/DirectX9Core.h>
#include <engine/EngineInternal.h>

class DXAPI D3DGraphcis9;

class CFontHandler : Renderer
{
	friend class Renderer;

private:

	ID3DXFont		*ppFont;

public:
	/**
	* Constructor
	**/
	CFontHandler(void);

	/**
	* Destructor
	**/
	~CFontHandler(void);

	/**
	* Create a font from a file.
	* ---------------------
	* @PARAM
	*	LPDIRECT3DDEVICE9: pointer to d3d device.
	* @PARAM
	*	uint: Height of font.
	* @PARAM
	*	uint: Width of font.
	* @PARAM
	*	uint: Thickness of font.
	* @PARAM
	*	uint: Mipmaps.
	* @PARAM
	*	bool: Is font italic?
	* @PARAM
	*	DWORD: Character set.
	* @PARAM
	*	DWORD: How precisely the outmust must match the font.
	* @PARAM
	*	DWORD: Quality of font.
	* @PARAM
	*	DWORD: Pitch and font family.
	* @PARAM
	*	pFacename: Face name of font.
	* @PARAM
	*	LPD3DXFONT: Font.
	* @RETURN
	*	HRESULT - SUCCEEDED if OK, otherwise
	*			  return FAILED.
	**/
	HRESULT LoadFont(LPDIRECT3DDEVICE9 pDevice, uint uiHeight, uint uiWidth,
		uint uiWeight, uint uiMipLevels, bool Italic, DWORD CharSet,
		DWORD OutputPrecision, DWORD Quality, DWORD PitchAndFamily, 
		LPCTSTR pFacename);

	int DrawFontA(LPD3DXSPRITE pSprite, LPCTSTR pString, int count, RECT rct, DWORD Format, D3DCOLOR Color);

	/**
	* Cleanup and release
	* all objects.
	**/
	void OnRelease(void);
};

#endif //__CFONTHANDLER_H__