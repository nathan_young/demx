#include "StaticElement.h"


StaticElement::StaticElement(void)
{
}


StaticElement::~StaticElement(void)
{
}


void StaticElement::Show( RECT camera )
{
	RECT srcRect;

	spriteHandler->Begin( D3DXSPRITE_ALPHABLEND );

	D3DXVECTOR3 position(
		(float) spriteAttributes.x, 
		(float) spriteAttributes.y,
		0);

	srcRect.left   = 0;
	srcRect.top    = 0;
	srcRect.right  = srcRect.left + spriteAttributes.width;
	srcRect.bottom = srcRect.top + spriteAttributes.height;

	spriteHandler->Draw(lpTexPlayer, &srcRect, NULL, &position, D3DCOLOR_XRGB(255, 255, 255));

	spriteHandler->End();
} 
