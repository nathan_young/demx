//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//>> Copyright (C) 2008 - 2012 Execute Software
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "debug.h"
#include <stdio.h>
#include <stdlib.h>
#include "debug_io_console.h"
#include <iostream>

#if defined (WIN32)
#include <Windows.h>

void _Assert(const char *filename, int _linenumber, const char *_expression)
{
	char msgbuffer[1024];
	sprintf(msgbuffer, " File name.......: %s\n Line number.....: %d\n Expression......: %s", filename, _linenumber, _expression);

	//Should find a way to output to stream that does not require redirecting
	//stream first and does not bind to system.

	//Console redirect needs to happen first.  Assume that we have not redirected
	//I/O, and do it here.
	RedirectIOToDebugConsole("Debug Console");

	ChangeConsoleTextColor(CTC_BRIGHTBLUE);
	std::cout << "============================== Assertion Failed ==============================" << std::endl;
	ChangeConsoleTextColor(CTC_BRIGHTGREEN);
	std::cout << msgbuffer << std::endl;
	ChangeConsoleTextColor(CTC_BRIGHTBLUE);
	std::cout << "==============================================================================" << std::endl;
	ChangeConsoleTextColor(CTC_WHITE);

	if(MessageBox(NULL, msgbuffer, "Assertion Failed", MB_YESNO) == IDYES)
		__asm int 0x03; //Toggle breakpoint.

}

#endif