#include "Entity.h"


Entity::Entity(void)
{
	this->spriteAttributes.x = 0;
	this->spriteAttributes.y = 0;
	this->spriteAttributes.frame = 0;
	this->spriteAttributes.status = 0;
}


Entity::~Entity(void)
{
}

void Entity::Show( RECT camera )
{
	RECT srcRect;

	spriteHandler->Begin( D3DXSPRITE_ALPHABLEND );

	D3DXVECTOR3 position(
		(float) (spriteAttributes.x - camera.left), 
		(float) (spriteAttributes.y - camera.top),
		0);

	//std::cout << spriteAttributes.status;

	srcRect.left   = spriteAttributes.frame * spriteAttributes.width;
	srcRect.top    = spriteAttributes.status * spriteAttributes.height;
	srcRect.right  = srcRect.left + spriteAttributes.width;
	srcRect.bottom = srcRect.top + spriteAttributes.height;
	
	spriteHandler->Draw( lpTexPlayer, &srcRect, NULL, &position, D3DCOLOR_XRGB(255, 255, 255) );

	spriteHandler->End();
} 

