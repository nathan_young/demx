//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// This file is part of code that is being licensed from Execute
// Software.  This code is not public and can not be released.
//
//	Copyright (C) 2011 - 2012 Execute Software.  All rights reserved.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __SYSTEM_WIN32_H__
#define __SYSTEM_WIN32_H__

#include <System.h>

class System_Win32 : public System
{
public:
	System_Win32();
	~System_Win32();

	virtual void CreateDialogBox(const char *msg, ...);
	virtual void CreateDialogFatal(const char *msg);

	//Console functions.
	virtual void WriteToDebugConsole(const char *msg);
	virtual void PrintDbgStr(const char *msg);
	virtual void PrintStr(const char *msg);

	//Default text color is white, but other colors can be specified.
	virtual void PrintUsrStr(const char *msg, unsigned int color = 0x0000);


	virtual void dbgprintf(const char *format, ...);
};

#endif /*__SYSTEM_WIN32_H__*/