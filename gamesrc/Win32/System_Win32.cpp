//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// This file is part of code that is being licensed from Execute
// Software.  This code is not public and can not be released.
//
//	Copyright (C) 2011 - 2012 Execute Software.  All rights reserved.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "System_Win32.h"
#include <debug.h>
#include <Windows.h>
#include <stdarg.h>

#include <iostream>
#include <debug_io_console.h>

//-----------------------------------
//>> System_Win32::System_Win32
//-----------------------------------
System_Win32::System_Win32()
{
}

//-----------------------------------
//>> System_Win32::~System_Win32
//-----------------------------------
System_Win32::~System_Win32()
{
}

//-----------------------------------
//>> System_Win32::CreateDialogBox
//>>
//>> Create a win32 dialog box with
//>> a text message.  Flags can be
//>> specified for title change and
//>> DWORD params.
//-----------------------------------
void System_Win32::CreateDialogBox(const char *msg, ...)
{
	//These needed to be zero's out or weird flags can be called.
	LPSTR va_boxtitle;
	unsigned int uiflags;

	va_list vaptr;
	va_start(vaptr, msg);
		va_boxtitle = va_arg(vaptr, LPSTR); //Dialog box title if specified.
		uiflags = va_arg(vaptr, unsigned int); //DWORD flags.

		//If va parameters don't exist we need to zero out flags or we get everything
		//ranging from unhandled exceptions to weird message boxes.
		if(va_boxtitle || uiflags == 0)
		{
			va_boxtitle = 0; 
			uiflags = 0;
		}//End if.

	va_end(vaptr);

	::MessageBoxA(NULL, msg, va_boxtitle, uiflags);
}

//-----------------------------------
//>> System_Win32::CreateDialogFatal
//-----------------------------------
void System_Win32::CreateDialogFatal(const char *msg)
{
	CreateDialogBox(msg, "Fatal Error", NULL);
}

//-----------------------------------
//>> System_Win32::WriteToDebugConsole
//>>
//>> Requires STD stream override be
//>> called first.
//-----------------------------------
void System_Win32::WriteToDebugConsole(const char *msg)
{
	ChangeConsoleTextColor(CTC_BRIGHTRED);

	//Requires IO reroute function to be called first.
	std::cout << msg << std::endl;

	ChangeConsoleTextColor(CTC_WHITE);
}

//-----------------------------------
//>> System_Win32::PrintDbgStr
//>>
//>> Print string in red text.
//-----------------------------------
void System_Win32::PrintDbgStr(const char *msg)
{
	ChangeConsoleTextColor(CTC_BRIGHTRED);
	std::cout << msg << std::endl;
	ChangeConsoleTextColor(CTC_WHITE);
}

//-----------------------------------
//>> System_Win32::PrintStr
//>>
//>> Print string in normal white.
//-----------------------------------
void System_Win32::PrintStr(const char *msg)
{
	//Console buffer does not flush so let's be sure it's white.
	ChangeConsoleTextColor(CTC_WHITE);
	std::cout << msg << std::endl;
	ChangeConsoleTextColor(CTC_WHITE);
}

//-----------------------------------
//>> System_Win32::PrintUsrStr
//>>
//>> Print string with a user defined
//>> color.
//-----------------------------------
void System_Win32::PrintUsrStr(const char *msg, unsigned int colorcode)
{
	ChangeConsoleTextColor(colorcode);
	std::cout << msg << std::endl;
	ChangeConsoleTextColor(CTC_WHITE);
}

//-----------------------------------
//>> System_Win32::dbgprintfA
//>>
//>> System printf writes output to
//>> redirected stream -- debug console.
//-----------------------------------
void System_Win32::dbgprintf(const char *format, ...)
{
}