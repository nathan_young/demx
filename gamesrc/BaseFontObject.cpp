//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include <renderer\BaseFontObject.h>

/*==============================================================
	CFontHandler::CFontHandler(void)
	---------------------
	Constructor.
==============================================================*/
CFontHandler::CFontHandler(void)
{
	ppFont = NULL;
}


/*==============================================================
	CFontHandler::~CFontHandler(void)
	---------------------
	Destructor.
==============================================================*/
CFontHandler::~CFontHandler(void)
{
}


/*==============================================================
	ID3DXFont *CFontHandler::LoadFont(LPDIRECT3DDEVICE9 pDevice, uint uiHeight, uint uiWidth,
		uint uiWeight, uint uiMipLevels, bool Italic, DWORD CharSet,
		DWORD OutputPrecision, DWORD Quality, DWORD PitchAndFamily, 
		LPCTSTR pFacename, LPD3DXFONT **ppFont)
	---------------------
	Load a font
==============================================================*/
HRESULT CFontHandler::LoadFont(LPDIRECT3DDEVICE9 pDevice, uint uiHeight, uint uiWidth,
		uint uiWeight, uint uiMipLevels, bool Italic, DWORD CharSet,
		DWORD OutputPrecision, DWORD Quality, DWORD PitchAndFamily, 
		LPCTSTR pFacename)
{
	HRESULT hr;

	if(FAILED( hr = D3DXCreateFont(pDevice, uiHeight, uiWidth, uiWeight, uiMipLevels, Italic,
		DEFAULT_CHARSET, OutputPrecision, Quality, PitchAndFamily, pFacename, &ppFont)));
	
	if(FAILED(hr))
		return hr;

	return D3D_OK;
}


/*==============================================================
	int DrawFont(LPD3DXSPRITE pSprite, LPCTSTR pString, int count,
	LPRECT pRect, DWORD Format, D3DCOLOR Color)
	---------------------
	Render text to screen.
==============================================================*/
int CFontHandler::DrawFontA(LPD3DXSPRITE pSprite, LPCTSTR pString, int count, RECT rct, DWORD Format, D3DCOLOR Color)
{
	//Set color of font.
	D3DCOLOR fontColor = D3DCOLOR_ARGB(0, 0, 0, 0); //Black.

	
	//Rectangle to indicate were on the screen it should
	//be drawn.
	RECT r;
	rct.left = 2;
	rct.right = 780;
	rct.top = 10;
	rct.bottom = rct.top + 20;
	
	/*
		m_pFont->DrawTextA(NULL, PlayerNameStr.c_str(), -1, &PlayerNamePos, DT_NOCLIP,
					D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
	*/

	ppFont->DrawText(NULL, "", -1, &r, DT_NOCLIP, D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));

	return 0;
}


/*==============================================================
	void CFontHandler::OnRelease(void)
	---------------------
	Cleanup.
==============================================================*/
void CFontHandler::OnRelease(void)
{
	ppFont->Release();
}