//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef _ITEM_H
#define	_ITEM_H

#include <iostream>
using namespace std;

struct Item
{
	int id;

	std::string name;

	/*
		1- armour
	
	*/
	int type;

	/*
		1- head
		2- chest
		3- legs
		4- weapon
	*/
	int part;
	

	int Endurance;
	int Strength;
	int Intelligence;
	int Agility;
	int Dexterity;
	int Wisdom;

	int damage;


};

#endif	/* _ITEM_H */