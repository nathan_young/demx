//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __INPUT_H___
#define __INPUT_H__

//Generate DI8 based headers.
//MUST COME BEFORE THE FILE INCLUDE!
#define DIRECTINPUT_VERSION 0x0800 //Dx9

#include <dinput.h>
#include "GlobalHeader.h"

#define BUTTON_DOWN(obj, button) (obj.rgbButtons[button] & 0x80)
//#define KEYDOWN(name, key) (name[key] & 0x80)

class Input
{
private:	
	LPDIRECTINPUT8 dinput;
	LPDIRECTINPUTDEVICE8 dimouse;
	LPDIRECTINPUTDEVICE8 dikeyboard;
	DIMOUSESTATE mouse_state;

	char keys[256];
public:
	/**
	* Constructor.
	**/
	Input();

	/**
	* Destructor.
	**/
	~Input();

	/**
	* Initialize DirectInput
	* ---------------------
	* @PARAM
	*	HWND: Handle to window.
	**/
	int Init(HWND winhandle);

	/**
	* Initialize keyboard
	* ---------------------
	* @PARAM
	*	HWND: Handle to window.
	**/
	int InitKeyboard(HWND winhandle);

	/**
	* Poll for keyboar input.
	**/
	void PollKeyboard(void);

	/**
	* Check for key down.
	* ---------------------
	* @PARAM
	*	int: Key.
	**/
	int KeyDown(int key);

	/**
	* Kill the keyboard.
	**/
	void KillKeyboard(void);

	/**
	* Poll for mouse movement.
	**/
	void PollMouse(void);

	/**
	* Initialize the mouse
	* ---------------------
	* @PARAM
	*	HWND: Handle to window.
	**/
	int InitMouse(HWND winhandle);

	/**
	* Check for mouse button down.
	* ---------------------
	* @PARAM
	*	int: Mouse button.
	**/
	int MouseButton(int button);

	/**
	* Grab mouse state.
	* ---------------------
	* @RETURN
	*	X mouse state.
	**/
	int MouseX();

	/**
	* Grab mouse state.
	* ---------------------
	* @RETURN
	*	Y mouse state.
	**/
	int MouseY();

	int mouseKeyIsDown( unsigned char ucKey );

	/**
	* Kill the mouse.
	**/
	void KillMouse(void);
};

//extern LPDIRECTINPUT8 dinput;
//extern LPDIRECTINPUTDEVICE8 dimouse;
//extern LPDIRECTINPUTDEVICE8 dikeyboard;
//extern DIMOUSESTATE mouse_state;


#endif //__D3DINPUT_H__