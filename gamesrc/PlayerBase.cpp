#include "PlayerBase.h"


void PlayerBase::Init()
{
	pInv.Init();
	pStats.Init();
	pGear.Init();

	pclass = 0;
	client_hp = 1;
	client_curhp = 1;
	target_hp = 1;
	target_curhp = 1;
	tarx=0;
	tary=0;
	vel = 5;
	plane = 0;
	xvel=0;
	yvel=0;
	player_stunned = false;
	charging = false;

	// set target to NULL
	target.type = 0;
	target.id = 0;

}


void PlayerBase::InventoryInsert( int item )
{
	pInv.ItemInsert( item );
}
void PlayerBase::InventoryEquip( int slot )
{
	if( EquipItem( getInventorySlot(slot) ) )
		pInv.ItemDelete( slot );
}
void PlayerBase::InventoryUse()
{
	// define use item
}
void PlayerBase::InventoryDelete( int slot )
{
	pInv.ItemDelete( slot );
}
int PlayerBase::getInventorySlot( int slot )
{
	return pInv.getSlot( slot );
}

void PlayerBase::setStats( int Endurance, int Strength, int Agility, int Intelligence )
{
	pStats.Endurance = Endurance;
	pStats.Strength = Strength;
	pStats.Agility = Agility;
	pStats.Intelligence = Intelligence;
}

bool PlayerBase::EquipItem( int item )
{
	if( item != 0 )
	{
		bool r = pGear.Equip( item, pStats );
	
		//cout <<"hpp:"<< pStats.Hp<<endl;

		return r;
	}	


	return false;
}


int PlayerBase::getGearHead()
{
	return pGear.head;
}
int PlayerBase::getGearChest()
{
	return pGear.chest;
}
int PlayerBase::getGearLegs()
{
	return pGear.legs;
}
int PlayerBase::getGearWeapon()
{
	return pGear.weapon;
}

int PlayerBase::getAttribute_MeleeDamage()
{
	return pStats.MeleeDamage;
}
int PlayerBase::getAttribute_MeleePower()
{
	return pStats.MeleePower;
}
int PlayerBase::getAttribute_MagicPower()
{
	return pStats.MagicPower;
}
int PlayerBase::getAttribute_MeleeCritChance()
{
	return pStats.MeleeCritChance;
}
int PlayerBase::getAttribute_MagicCritChance()
{
	return pStats.MagicCritChance;
}
int PlayerBase::getAttribute_WeaponDamage()
{
	return pStats.WeaponDamage;
}
float PlayerBase::getAttribute_Hp()
{
	return pStats.Hp;
}
int PlayerBase::getStat_Endurance()
{
	return pStats.Endurance;
}
int PlayerBase::getStat_Strength()
{
	return pStats.Strength;
}
int PlayerBase::getStat_Intelligence()
{
	return pStats.Intelligence;
}
int PlayerBase::getStat_Agility()
{
	return pStats.Agility;
}

void PlayerBase::setClass( int pclass )
{
	this->pclass = pclass;
}
int PlayerBase::getClass()
{
	return pclass;
}




