//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __EXCEPTION_H__
#define __EXCEPTION_H__

#include <exception>
#include <string>

class Exception : std::exception
{
public:
	/**
	* Constructor.
	**/
	Exception(std::string m = "Exception caught.") 
		: msg(m)
	{
	}

	/**
	* Destructor.
	**/
	~Exception() throw()
	{
	}

	/**
	* Override of std::what.
	**/
	const char *what() const throw()
	{
		return msg.c_str();
	}

private:
	std::string msg;
};

#endif //__EXCEPTION_H__