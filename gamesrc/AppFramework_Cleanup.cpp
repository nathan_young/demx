//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "AppFramework.h"

/*==============================================================
	void AppFramework::GameCleanup()
	---------------------
	Clean up all data, etc after the game exits.
==============================================================*/
void AppFramework::GameCleanup()
{
	//Clean up graphics.
	_Graphics.Cleanup();
	//Cleanup sprites.
	//sprite_player.Cleanup();
	//sprite_mob.Cleanup();
	//mob.Cleanup();
	/*
	shield.Cleanup();
	scorch_spell.Cleanup();
	conflag_spell.Cleanup();
	castbar.Cleanup();
	*/
	//sprites.CleanUp();
	

//	Font->OnRelease();
	DEMX_SAFE_RELEASE(m_pFont);
	DEMX_SAFE_RELEASE(s_pFont);

	uiMgr.CleanUp();
	/*
	ability1.Cleanup();
	ability2.Cleanup();
	ability3.Cleanup();
	ability4.Cleanup();
	tb.Cleanup();
	loginbox.Cleanup();
	*/
	//Delete dxgui element interface.
	//igui->Delete();
}