-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 13, 2012 at 11:43 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `example`
--

-- --------------------------------------------------------

--
-- Table structure for table `creeps`
--

use demx;

CREATE TABLE IF NOT EXISTS `creeps` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `lvl` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `creeps`
--

INSERT INTO `creeps` (`id`, `name`, `lvl`) VALUES
(1, 'Maka', NULL),
(2, 'Konga', NULL),
(3, 'Kola', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `type` int(11) NOT NULL,
  `part` int(11) NOT NULL,
  `endurance` int(11) NOT NULL,
  `strength` int(11) NOT NULL,
  `agility` int(11) NOT NULL,
  `intelligence` int(11) NOT NULL,
  `damage` int(11) NOT NULL,
  `speed` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `type`, `part`, `endurance`, `strength`, `agility`, `intelligence`, `damage`, `speed`) VALUES
(1, 'Sword of Might', 1, 4, 1, 100, 1, 0, 210, 4),
(2, 'Chest of Power', 1, 2, 1, 3, 0, 0, 0, 0),
(3, 'Mystic''s Robe', 1, 2, 1, 0, 0, 2, 0, 0),
(4, 'Sword of Magic', 1, 4, 1, 0, 0, 38, 5, 2),
(5, 'Thief''s Blade', 1, 4, 10, 0, 50, 0, 95, 2);

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE IF NOT EXISTS `players` (
  `id` int(11) unsigned NOT NULL,
  `class` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `level` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `endurance` int(11) NOT NULL,
  `strength` int(11) NOT NULL,
  `agility` int(11) NOT NULL,
  `intelligence` int(11) NOT NULL,
  `head` int(11) NOT NULL,
  `chest` int(11) NOT NULL,
  `legs` int(11) NOT NULL,
  `weapon` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `class`, `name`, `level`, `exp`, `endurance`, `strength`, `agility`, `intelligence`, `head`, `chest`, `legs`, `weapon`) VALUES
(1, 1, 'demx', 1, 0, 10, 0, 0, 0, 0, 0, 0, 1),
(2, 2, 'lol', 1, 0, 10, 0, 0, 0, 0, 0, 0, 4),
(3, 3, 'nathan', 1, 0, 10, 0, 0, 0, 0, 0, 0, 1),
(4, 0, 'test', 1, 0, 10, 0, 0, 0, 0, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
