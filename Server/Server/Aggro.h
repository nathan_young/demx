#ifndef _AGGRO_H_
#define _AGGRO_H_

#include <vector>
#include "Packets.h"
#include "DataConfig.h"

class Aggro
{
	
	std::vector<float> AggroToPlayers;

public:

	Aggro();

	void setAggro( int, float );
	void emptyAggro();
	int getHighestAggro();
	int getAggro(int);
	void ResetPlayerAggro( int, ServerPlayer[] );
};

#endif