#include "stdafx.h"
#include "ServerApp.h"

ServerApp::ServerApp()
{
	g_pClientContextHead = NULL;
	ListenSocket = NULL;
}
ServerApp::~ServerApp()
{

}

void ServerApp::ServerInitGame()
{
	unsigned int i;
	numPlayers = 0;
	newplayer = false;

	for( i=0; i<MAX_PLAYERS; i++ )
	{
		player_server_data[i].id=0;
		strcpy( player_server_data[i].username, "");
		player_server_data[i].x=0;
		player_server_data[i].y=0;
		player_server_data[i].plane=0;
		player_server_data[i].xvel=0;
		player_server_data[i].yvel=0;
		player_server_data[i].curhp=0;
		player_server_data[i].hp=0;
		player_server_data[i].combat=false;
		player_server_data[i].slowed=false;
	}

	for( i=0; i<MAX_CREEPS; i++ )
	{
		creep_server_data[i].id=i+1;
		creep_server_data[i].x=300;
		creep_server_data[i].y=300;
		creep_server_data[i].plane=0;
		creep_server_data[i].xvel=0;
		creep_server_data[i].yvel=0;
		creep_server_data[i].curhp=10000;
		creep_server_data[i].hp=10000;
		creep_server_data[i].combat=false;
		creep_server_data[i].slowed=false;
	}
}

int ServerApp::SetupServer()
{


	WSADATA wsaData;

	int nResult;

	nResult = WSAStartup(MAKEWORD(2,2), &wsaData);

	if (NO_ERROR != nResult)
	{
		printf("\nError occurred while executing WSAStartup().");
		return 1; //error
	}
	else
	{
		printf("\nWSAStartup() successful.");
	}


	struct sockaddr_in ServerAddress;

	//Create a socket
	ListenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (INVALID_SOCKET == ListenSocket) 
	{
		printf("\nError occurred while opening socket: %ld.", WSAGetLastError());
		return 1;
	}
	else
	{
		printf("\nsocket() successful.");
	}

	//Cleanup and Init with 0 the ServerAddress
	ZeroMemory((char *)&ServerAddress, sizeof(ServerAddress));

	//Port number will be supplied as a commandline argument
	//nPortNo = atoi(argv[1]);

	//Fill up the address structure
	ServerAddress.sin_family = AF_INET;
	ServerAddress.sin_addr.s_addr = INADDR_ANY; //WinSock will supply address
	ServerAddress.sin_port = htons(6000);    //comes from commandline
	 
	//Assign local address and port number
	if ( SOCKET_ERROR == ::bind(ListenSocket, (struct sockaddr *) &ServerAddress, sizeof(ServerAddress))) 
	{
		closesocket(ListenSocket);

		printf("\nError occurred while binding.");
		return 1;
	}
	else
	{
		printf("\nbind() successful.");
	}

	//Make the socket a listening socket
	if (SOCKET_ERROR == listen(ListenSocket,SOMAXCONN))
	{
		closesocket(ListenSocket);

		printf("\nError occurred while listening.");
		return 1;
	}
	else
	{
		printf("\nlisten() successful.");
	}
}

void ServerApp::InitSets(SOCKET ListenSocket)
{
     //Initialize
     FD_ZERO(&g_ReadSet);
     FD_ZERO(&g_WriteSet);
     FD_ZERO(&g_ExceptSet);

     //Assign the ListenSocket to Sets
     FD_SET(ListenSocket, &g_ReadSet);
     FD_SET(ListenSocket, &g_ExceptSet);

     //Iterate the client context list and assign the sockets to Sets
     ClientContext   *pClientContext  = GetClientContextHead();
	
     while(pClientContext)
     {
          //if(pClientContext->GetSentBytes() < pClientContext->GetTotalBytes())
          //{
               //We have data to send
          //     FD_SET(pClientContext->GetSocket(), &g_WriteSet);
          //}
          //else
          //{
               //We can read on this socket
          FD_SET(pClientContext->GetSocket(), &g_ReadSet);
          //}

          //Add it to Exception Set
          FD_SET(pClientContext->GetSocket(), &g_ExceptSet); 

          //Move to next node on the list
          pClientContext = pClientContext->GetNext();
     }
}

int ServerApp::GetSocketSpecificError(SOCKET Socket)
{
     int nOptionValue;
     int nOptionValueLength = sizeof(nOptionValue);

     //Get error code specific to this socket
     getsockopt(Socket, SOL_SOCKET, SO_ERROR, (char*)&nOptionValue, &nOptionValueLength);

     return nOptionValue;
}