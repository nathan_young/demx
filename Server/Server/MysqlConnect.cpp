#include "stdafx.h"
#include "ServerApp.h"

void ServerApp::MysqlConnect()
{
	const char *dbName = "demx";
	
	driver = get_driver_instance();
	std::string ip;
	ip.append( "tcp://" );
	ip.append( "127.0.0.1" );
	ip.append( ":3306" );

	con = driver->connect( ip, "root", "" );
	//Assert(con);

	con->setSchema(dbName);

	printf("\nConnection to Mysql server %s located on @ %s successful\n", dbName, ip.c_str());
	
}
