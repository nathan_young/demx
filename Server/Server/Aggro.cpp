#include "stdafx.h"
#include "Aggro.h"


Aggro::Aggro()
{
	AggroToPlayers.reserve( MAX_PLAYERS );
	AggroToPlayers.clear();
}

void Aggro::setAggro(int i,float damage)
{
	AggroToPlayers[i] += damage;
}

void Aggro::emptyAggro()
{
	int i;
	for( i=0; i<MAX_PLAYERS; i++ )
	{
		AggroToPlayers[i]=0;
	}
}

int Aggro::getHighestAggro()
{
	float max;
	max=0;
	int ret=-1,i;

	for(i=0; i<MAX_PLAYERS; i++)
	{
		if ( AggroToPlayers[i] > max )
		{
			max = AggroToPlayers[i];
			ret = i;
		}
	}
	return ret;
}

int Aggro::getAggro(int pos)
{
	return AggroToPlayers[pos];
}

void Aggro::ResetPlayerAggro( int id, ServerPlayer player_server_data[] )
{
	int pos=0;
		
	bool exit = false;
	for ( unsigned int i = 0; i<5 && !exit; i++ )
	{
		if ( player_server_data[i].id == id )
		{
			pos = i;
			exit = true;
		}
	}
	AggroToPlayers[pos] = 0;
}
