#include "stdafx.h"
#include "ServerApp.h"

void ServerApp::RecievePort()
{
	 while (true)
     {
          InitSets(ListenSocket);

          if (select(0, &g_ReadSet, NULL, &g_ExceptSet, 0) > 0) 
          {
               //One of the socket changed state, let's process it.

               //ListenSocket?  Accept the new connection
               if (FD_ISSET(ListenSocket, &g_ReadSet)) 
               {
                    sockaddr_in ClientAddress;
                    int nClientLength = sizeof(ClientAddress);

                    //Accept remote connection attempt from the client
                    SOCKET Socket = accept(ListenSocket, (sockaddr*)&ClientAddress, &nClientLength);

                    if (INVALID_SOCKET == Socket)
                    {
                         printf("\nError occurred while accepting socket: %ld.", GetSocketSpecificError(ListenSocket));
                    }

                    //Display Client's IP
                    printf("\nClient connected from: %s", inet_ntoa(ClientAddress.sin_addr)); 

                    //Making it a non blocking socket
                    u_long nNoBlock = 1;
                    ioctlsocket(Socket, FIONBIO, &nNoBlock);

                    ClientContext   *pClientContext  = new ClientContext;
                    pClientContext->SetSocket(Socket);

                    //Add the client context to the list
                    AddClientContextToList(pClientContext);
               }

               //Error occured for ListenSocket?
               if (FD_ISSET(ListenSocket, &g_ExceptSet)) 
               {
                    printf("\nError occurred while accepting socket: %ld.", GetSocketSpecificError(ListenSocket));
                    continue;
               }

               //Iterate the client context list to see if any of the socket there has changed its state
               ClientContext   *pClientContext  = GetClientContextHead();

               while (pClientContext)
               {
                    //Check in Read Set
                    if (FD_ISSET( pClientContext->GetSocket(), &g_ReadSet ))
                    {

						char buf[4096];
						unsigned int i;
						bool exit;

						//int nBytes = recv(pClientContext->GetSocket(), (char*)&p, sizeof(p), 0);

						int nBytes = recv( pClientContext->GetSocket(), buf, sizeof(buf), 0 );

						if ((0 == nBytes) || (SOCKET_ERROR == nBytes))
                        {
                            if (0 != nBytes) //Some error occured, client didn't close the connection
                                printf("\nError occurred while recieving on the socket: %d.", GetSocketSpecificError(pClientContext->GetSocket()));
                            //In either case remove the client from list
                            pClientContext = DeleteClientContext(pClientContext);
                            continue;
                        }
						else
						{	
							int vetpos;
							uint8_t id;
							char* messageToProcess = (char*) buf;
							
							uint8_t msg = extractUnsignedInt8( &messageToProcess );
							

							cout << "msg: " << (int)msg << endl;

							switch( msg )
							{
								case 0:
								{
									char* str = (char*) messageToProcess;

									if( Login( str ) )
									{
										PortPacket port;
										port.msg = 0;
										SendPort( pClientContext->GetSocket(), port );
										pClientContext->setLogin( true );
										newplayer = true;
									}	
								}break;


								case 1:
								{
									/*
									unsigned int id = extractUnsignedInt( &messageToProcess );

									bool exit=false;
									for( i=0; i<MAX_PLAYERS && !exit; i++ )
									{
										if( player_server_data[i].id == -1 )
										{
											player_server_data[i].id = id;
											player_server_data[i].x=50;
											player_server_data[i].y=50;
											player_server_data[i].plane = 0;
											player_server_data[i].xvel=0;
											player_server_data[i].yvel=0;
											player_server_data[i].hp= 10000;
											player_server_data[i].curhp= 10000;
											player_server_data[i].level = 1;
											player_server_data[i].exp = 0;
							
											exit = true;
										}
									}

									numPlayers++;
									*/
								}break;

								case 2:
								{
									id = extractUnsignedInt8( &messageToProcess );

									
									//cout << "ID " << static_cast<int16_t>(player_server_data[0].id) << endl;

									for( int i=0; i<numPlayers; i++ )
									{
										if( player_server_data[i].id == id )
										{

											vetpos = i;
										}
									}

									//cout << "MOVEMENTE ID " << static_cast<int16_t>(id) << " vetpos " << vetpos <<endl;

									int8_t xvel = extractInt8( &messageToProcess );
									int8_t yvel = extractInt8( &messageToProcess );


									//cout << xvel << yvel << endl;

									player_server_data[vetpos].xvel = xvel;
									player_server_data[vetpos].yvel = yvel;
								}break;

								case 3:
								{
									uint16_t damage = 0;

									id = extractUnsignedInt8( &messageToProcess );
									
									uint8_t ability = extractUnsignedInt8( &messageToProcess );
									uint8_t targettype = extractUnsignedInt8( &messageToProcess );
									uint8_t targetid = extractUnsignedInt8( &messageToProcess );

									damage = Abilities( ability, targetid, id );

									cout << (int)targetid << endl;

									unsigned int i,tarpos=0;
									for( i = 0; i < numPlayers; i++ )
									{
										if( player_server_data[i].id == targetid )
										{
											tarpos = i;
											break;
										}
									}

									player_server_data[tarpos].curhp -= damage;

									if( player_server_data[tarpos].curhp <= 0 )
									{
										player_server_data[tarpos].x = 50;
										player_server_data[tarpos].y = 50;
										player_server_data[tarpos].curhp = player_server_data[vetpos].hp;
									}


									if( damage != 0 )
									{
										// send damage back to client
										PortPacket port;
										port.msg = 3;
										port.id = id;
										port.damage = damage;
										SendPort( pClientContext->GetSocket(), port );
									}

								}break;

								case 4:
								{
									id = extractUnsignedInt8( &messageToProcess );
									char* str = (char*) messageToProcess;

									PortPacket port;
									port.id = id;
									port.text = str;

									BroadcastChat( port );
								}break;

							}

						}

                    }
				
                    //Check in Exception Set
                    if (FD_ISSET(pClientContext->GetSocket(), &g_ExceptSet))
                    {
                         printf("\nError occurred on the socket: %d.", GetSocketSpecificError(pClientContext->GetSocket()));

                         pClientContext = DeleteClientContext(pClientContext);
                         continue;
                    }

                    //Move to next node on the list
                    pClientContext = pClientContext->GetNext();
               }//while
          }
          else //select
          {
               printf("\nError occurred while executing select(): %ld.", WSAGetLastError());
               return; //Get out of this function
          }
     }
}
