//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __SPRITES_H__
#define __SPRITES_H__

#include "Masks.h"

class Sprite
{
public:
	int x;
	int y;

	void Setup(int x,int y)
	{
		this->x = x;
		this->y = y;
	}

};

class SpriteObjects
{

	public:

		Masks *ObjectMasks;

		int TreeNum;
		int WallNum;     
		int StatueNum;  
		int Rock1Num;
		int totalobj;

		Sprite *Dynamic_tree;
		Sprite *Dynamic_wall;
		Sprite *Dynamic_statue;
		Sprite *Dynamic_rock1;

		void Init();

		void LoadWorld();
		void LoadArena();

		Masks * CreateObjectMasks();
};

#endif //__SPRITES_H__