#include "StdAfx.h"

#include "ClientContext.h"


ClientContext::ClientContext(void)
{
	m_Socket =  SOCKET_ERROR;
	login = false;
	m_pNext = NULL;
}

ClientContext::~ClientContext(void)
{
	closesocket(m_Socket);
}

void ClientContext::SetSocket(SOCKET s)
{
    m_Socket = s;
}

SOCKET ClientContext::GetSocket()
{
    return m_Socket;
}

ClientContext* ClientContext::GetNext()
{
	return m_pNext;
}

void ClientContext::SetNext(ClientContext *pNext)
{
	m_pNext = pNext;
}

bool ClientContext::isLoggedIn()
{
	return login;
}
void ClientContext::setLogin( bool in )
{
	login = in;
}