//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#ifndef __DATACONFIG_H__
#define __DATACONFIG_H__


#define MOB_NUM 2

#define MAX_PLAYERS 5
#define MAX_CREEPS 2

#define MAX_MATCHES 5

#endif //__DATACONFIG_H__