#include "stdafx.h"


#include "Matches.h"

void Matches::Init()
{
	joinArray.reserve( MAX_PLAYERS );
	matches.reserve( MAX_MATCHES );
	joinArray.clear();

	for( unsigned i=0; i<MAX_MATCHES; i++ )
	{
		matches[i].exist = false;
		matches[i].player1 = 0;
		matches[i].player2 = 0;
	}
}

int Matches::AddQueuePlayer(int id)
{
	int i;
	for(i=0;i<MAX_PLAYERS;i++)
	{
		if( joinArray[i] == 0 )
		{
			joinArray[i] = id;
			return 1; // successfully entered player in arena queue
		}
	}
	return 0;
}

bool Matches::SearchMatches( int arr[], ServerPlayer player_server_data[], int *plane )
{
	unsigned int i,j;

	if( joinArray[0] != 0 && joinArray[1] != 0 )
	{
		*plane ++;

		arr[0] = joinArray[0];
		arr[1] = joinArray[1];
				
		bool exit=false;
		for( i=0; i<MAX_MATCHES && !exit; i++ )
		{
			if( !matches[i].exist )
			{
				matches[i].exist = true;
				matches[i].player1 = joinArray[0];
				matches[i].player2 = joinArray[1];
				matches[i].plane = *plane;

				for( j=0; j<MAX_PLAYERS; j++ )
				{
					if( player_server_data[j].id == matches[i].player1 )
					{
						matches[i].savep1x = player_server_data[j].x;
						matches[i].savep1y = player_server_data[j].y;
					}
					if( player_server_data[j].id == matches[i].player2 )
					{
						matches[i].savep2x = player_server_data[j].x;
						matches[i].savep2y = player_server_data[j].y;
					}
				}
				exit = true;
			}
		}

	
		for( i=0; i<MAX_PLAYERS; i++ )
		{
			joinArray[i] = joinArray[i+2];
		}
		joinArray[MAX_PLAYERS-2]=0;
		joinArray[MAX_PLAYERS-1]=0;
				

		return true;
	}
		
	return false;
}

void Matches::Debug()
{

}

