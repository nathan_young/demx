#include "stdafx.h"
#include "ServerApp.h"

uint16_t ServerApp::Abilities( int ability, int target, int player )
{
	float damage=0;
	float DamageBonus = 1;
	unsigned int plapos,tarpos;
	unsigned int i;
	
	for( i = 0; i < numPlayers; i++ )
	{
		if( player_server_data[i].id == player )
		{
			plapos = i;
			break;
		}
	}

	for( i = 0; i < numPlayers; i++ )
	{
		if( player_server_data[i].id == target )
		{
			tarpos = i;
			break;
		}
	}

	switch( ability )
	{
		case 10:
		{
			if( player_server_data[plapos].ColdBlood == 1 )
			{
				DamageBonus = 1.2;
			}

			damage = 1000 * DamageBonus;
		}break;

		case 11:
		{
			player_server_data[plapos].ColdBlood = 1;
			player_server_data[plapos].TimerColdBlood.start();
		}break;

		case 20:
		{
			damage = 1500;
		}break;

		case 22:
		{
			player_server_data[tarpos].slowed = 1;
		}break;
	}

	

	return damage;
}