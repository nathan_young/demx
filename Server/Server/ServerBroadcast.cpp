#include "stdafx.h"
#include "ServerApp.h"

void ServerApp::Broadcast()
{
	int nBytesSent;
	unsigned int i;
	
	ServerPacket spacket;
	spacket.msg = 1;
	spacket.numPlayers = numPlayers;

	for( i = 0; i < numPlayers; i++ )
	{
		spacket.playerArray[i].id = player_server_data[i].id;
		spacket.playerArray[i].x = player_server_data[i].x;
		spacket.playerArray[i].y = player_server_data[i].y;
		spacket.playerArray[i].plane = player_server_data[i].plane;
		spacket.playerArray[i].xvel = player_server_data[i].xvel;
		spacket.playerArray[i].yvel = player_server_data[i].yvel;
		spacket.playerArray[i].curhp = player_server_data[i].curhp;
		spacket.playerArray[i].hp = player_server_data[i].hp;
		spacket.playerArray[i].combat = player_server_data[i].combat;
		spacket.playerArray[i].slowed = player_server_data[i].slowed;
		strcpy( spacket.playerArray[i].username, player_server_data[i].username );
	}

	ClientContext *pClientContext = GetClientContextHead();

	while (pClientContext)
	{
		if( pClientContext->isLoggedIn() )
		{
			nBytesSent = send( pClientContext->GetSocket(), (const char*)&spacket, 
							 ( sizeof(uint8_t)*2 + ( sizeof(struct Update) * spacket.numPlayers) ), 0);			
			//cout << sizeof(Player) << " "<< nBytesSent << " " << "broadcast" << endl;
		}

		pClientContext = pClientContext->GetNext();
	}
}

void ServerApp::BroadcastPartial()
{
	int nBytesSent;
	unsigned int i;
	
	ServerPacketPartial spacket;
	spacket.msg = 2;
	spacket.numPlayers = numPlayers;

	for( i = 0; i < numPlayers; i++ )
	{
		spacket.playerArray[i].id = player_server_data[i].id;
		spacket.playerArray[i].x = player_server_data[i].x;
		spacket.playerArray[i].y = player_server_data[i].y;
		spacket.playerArray[i].plane = player_server_data[i].plane;
		spacket.playerArray[i].xvel = player_server_data[i].xvel;
		spacket.playerArray[i].yvel = player_server_data[i].yvel;
		spacket.playerArray[i].curhp = player_server_data[i].curhp;
		spacket.playerArray[i].hp = player_server_data[i].hp;
		spacket.playerArray[i].combat = player_server_data[i].combat;
		spacket.playerArray[i].slowed = player_server_data[i].slowed;
	}

	ClientContext *pClientContext = GetClientContextHead();

	while (pClientContext)
	{
		if( pClientContext->isLoggedIn() )
		{
			nBytesSent = send( pClientContext->GetSocket(), (const char*)&spacket, 
							 ( sizeof(uint8_t)*2 + ( sizeof(struct UpdatePartial) * spacket.numPlayers) ), 0);			
			//cout << sizeof(Player) << " "<< nBytesSent << " " << "broadcast" << endl;
		}

		pClientContext = pClientContext->GetNext();
	}
}

void ServerApp::BroadcastChat( PortPacket p )
{
	int nBytesSent;
	unsigned int i;

	ServerChat spacket;

	spacket.msg = 4;
	spacket.id = p.id;
	strcpy( spacket.message, p.text.c_str() );

	ClientContext *pClientContext = GetClientContextHead();

	while (pClientContext)
	{
		if( pClientContext->isLoggedIn() )
		{
			nBytesSent = send( pClientContext->GetSocket(), (const char*)&spacket, 
							 ( sizeof(uint8_t)*2 + ( sizeof(spacket.message) ) ), 0);			
		}
		pClientContext = pClientContext->GetNext();
	}
}