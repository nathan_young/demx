#ifndef __SERVERAPP_H__
#define __SERVERAPP_H__


#include "DataConfig.h"
#include "ClientContext.h"
#include "Packets.h"

// MySQL headers 
#include <MySQLConn/include/mysql_connection.h>
#include <MySQLConn/include/cppconn/driver.h>
#include <MySQLConn/include/cppconn/exception.h>
#include <MySQLConn/include/cppconn/resultset.h>
#include <MySQLConn/include/cppconn/statement.h>
#include <MySQLConn/include/cppconn/prepared_statement.h>

class ServerApp
{

	
	sql::Driver *driver;
    sql::Connection *con;
    sql::Statement *stmt;
    sql::ResultSet *res;
    sql::PreparedStatement *pstmt;
	
	
	ClientContext    *g_pClientContextHead;
	fd_set g_ReadSet, g_WriteSet, g_ExceptSet;

	SOCKET ListenSocket;
	

	
	//	game data
	
	ServerPlayer player_server_data[MAX_PLAYERS];
	ServerCreep  creep_server_data[MAX_CREEPS];

	int numPlayers;

	bool newplayer;

public:
	ServerApp(void);
	~ServerApp(void);

	int SetupServer();
	void MysqlConnect();

	void RecievePort();
	void Broadcast();
	void BroadcastPartial();
	void BroadcastChat( PortPacket );

	void SendPort( SOCKET, PortPacket );

	void InitSets(SOCKET ListenSocket);
	int GetSocketSpecificError(SOCKET Socket);
	ClientContext* GetClientContextHead();
	void AddClientContextToList(ClientContext *pClientContext);
	ClientContext* DeleteClientContext(ClientContext *pClientContext);


	void ServerInitGame();
	void ServerGameLoop();

	uint16_t Abilities( int ability, int target, int player ); 

	bool Login( char * );

	// extract values


	uint8_t  extractUnsignedInt8 (char**);
	uint16_t extractUnsignedInt16 (char**);
	
	int8_t   extractInt8 (char**);
	int16_t  extractInt16 (char**);
	
};

#endif // SERVERAPP.H