#include "stdafx.h"
#include "ServerApp.h"
#include "Collision.h"


void ServerApp::ServerGameLoop()
{
	Timer fps, sendpacket;
	unsigned int i;
	bool collision;

	Collision cobj;

	sendpacket.start();

	while(true)
	{
		fps.start();

		for(i=0;i<numPlayers;i++)
		{
			if( !player_server_data[i].slowed )
			{
				player_server_data[i].x += player_server_data[i].xvel;
				player_server_data[i].y += player_server_data[i].yvel;
			}
			else
			{
				player_server_data[i].x += (player_server_data[i].xvel*0.4);
				player_server_data[i].y += (player_server_data[i].yvel*0.4);
			}
				
			RECT col;
			col.left = player_server_data[i].x;
			col.top  = player_server_data[i].y;
			col.right = col.left + 32;
			col.bottom = col.top + 50;

			if( cobj.CheckCollision( player_server_data[i].plane, col ) )
			{
				//if( !player_server_data[i].slowed )
				//{
					player_server_data[i].x -= player_server_data[i].xvel;
					player_server_data[i].y -= player_server_data[i].yvel;
				//}
				//else
				//{
				//	player_server_data[i].x -= (player_server_data[i].xvel*0.4);
				//	player_server_data[i].y -= (player_server_data[i].yvel*0.4);
				//}
			}
				
		
		}
		
		for( i=0; i<numPlayers; i++ )
		{
			if( player_server_data[i].TimerColdBlood.get_ticks() > 6000 )
			{
				player_server_data[i].ColdBlood = 0;
				player_server_data[i].TimerColdBlood.stop();
			}
		}

		//cout << player_server_data[0].TimerColdBlood.get_ticks() << endl;

		if( sendpacket.get_ticks() >= 66*3 )
		{
			if( newplayer )
			{
				Broadcast();
				newplayer = false;
			}
			else
			{
				BroadcastPartial();
			}
			sendpacket.start();
		}

		if( fps.get_ticks() < 66 )
		{
            Sleep( ( 66 ) - fps.get_ticks() );
        }
	}
}