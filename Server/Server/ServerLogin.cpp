#include "stdafx.h"
#include "ServerApp.h"

bool ServerApp::Login( char * username )
{
	bool exit;
	int i;

	pstmt = con->prepareStatement("select * from players");
	res = pstmt->executeQuery();

	while (res->next())
	{
		if( strcmp( username, res->getString("name").c_str() ) == 0 )
		{
			exit = false;
			for( i=0; i<MAX_PLAYERS && !exit; i++ )
			{
				if( player_server_data[i].id == 0 )
				{
					player_server_data[i].id = res->getUInt("id");
					player_server_data[i].pclass = res->getUInt("class");
					strcpy( player_server_data[i].username, res->getString("name").c_str() );
					player_server_data[i].x=50;
					player_server_data[i].y=50;
					player_server_data[i].plane = 0;
					player_server_data[i].xvel=0;
					player_server_data[i].yvel=0;
					player_server_data[i].hp= 10000;
					player_server_data[i].curhp= 10000;
					player_server_data[i].ColdBlood = 0;
							
					exit = true;
				}
			}
			numPlayers++;

			cout <<  static_cast<int16_t>(player_server_data[0].id)<< endl;

			return true;
		}
	}

	return false;
}