//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#include "StdAfx.h"
#include "Sprites.h"

void SpriteObjects::LoadWorld()
{
    TreeNum = 3;
	WallNum = 2;     
	StatueNum = 1;  
	Rock1Num = 3;

	totalobj = TreeNum + WallNum + StatueNum + Rock1Num;

	Dynamic_tree   = new Sprite [ TreeNum ];
	Dynamic_wall   = new Sprite [ WallNum ];
	Dynamic_statue = new Sprite [ StatueNum ];
	Dynamic_rock1  = new Sprite [ Rock1Num ];

	//Dynamic_tree[0].Setup(50,50);
	Dynamic_tree[0].Setup(100,150);
	Dynamic_tree[1].Setup(220,100);
	Dynamic_tree[2].Setup(400,500);
	Dynamic_wall[0].Setup(250,250);
	Dynamic_wall[1].Setup(350,250);
	Dynamic_statue[0].Setup(400,60);
	Dynamic_rock1[1].Setup(200,500);
	Dynamic_rock1[2].Setup(300,550);

}

void SpriteObjects::LoadArena()
{
    TreeNum = 0;
	WallNum = 2;     
	StatueNum = 0;  
	Rock1Num = 0;

	totalobj = TreeNum + WallNum + StatueNum + Rock1Num;

	Dynamic_tree   = new Sprite [ TreeNum ];
	Dynamic_wall   = new Sprite [ WallNum ];
	Dynamic_statue = new Sprite [ StatueNum ];
	Dynamic_rock1  = new Sprite [ Rock1Num ];

	Dynamic_wall[0].Setup(250,250);
	Dynamic_wall[1].Setup(345,250);
}

Masks * SpriteObjects::CreateObjectMasks()
{
	int i,j;

	ObjectMasks = new Masks [ totalobj ];

	for( i=0; i<TreeNum; i++ )
	{
		ObjectMasks[i].below.top =     112 + Dynamic_tree[i].y;
		ObjectMasks[i].below.left =    57  + Dynamic_tree[i].x;
		ObjectMasks[i].below.right =   ObjectMasks[i].below.left + 13;
		ObjectMasks[i].below.bottom =  ObjectMasks[i].below.top + 10;

		ObjectMasks[i].above.top =     85 + Dynamic_tree[i].y;
		ObjectMasks[i].above.left =    56 + Dynamic_tree[i].x;
		ObjectMasks[i].above.right =   ObjectMasks[i].above.left + 13;
		ObjectMasks[i].above.bottom =  ObjectMasks[i].above.top + 10;
	}
	j=0;
	for(i=i;i< TreeNum + WallNum ;i++)
	{
		ObjectMasks[i].below.top =     15 + Dynamic_wall[j].y;
		ObjectMasks[i].below.left =    0 +  Dynamic_wall[j].x;
		ObjectMasks[i].below.right =   ObjectMasks[i].below.left + 94;
		ObjectMasks[i].below.bottom =  ObjectMasks[i].below.top + 24;

		ObjectMasks[i].above.top =     0  + Dynamic_wall[j].y;
		ObjectMasks[i].above.left =    0  + Dynamic_wall[j].x;
		ObjectMasks[i].above.right =   ObjectMasks[i].above.left + 94;
		ObjectMasks[i].above.bottom =  ObjectMasks[i].above.top + 10;
		j++;
	}
	j=0;
	for(i=i;i<TreeNum+WallNum+StatueNum;i++)
	{
		ObjectMasks[i].below.top =     90 + Dynamic_statue[j].y;
		ObjectMasks[i].below.left =    13 + Dynamic_statue[j].x;
		ObjectMasks[i].below.right =   ObjectMasks[i].below.left + 70;
		ObjectMasks[i].below.bottom =  ObjectMasks[i].below.top + 35;

		ObjectMasks[i].above.top =     75  + Dynamic_statue[j].y;
		ObjectMasks[i].above.left =    13  + Dynamic_statue[j].x;
		ObjectMasks[i].above.right =   ObjectMasks[i].above.left + 70;
		ObjectMasks[i].above.bottom =  ObjectMasks[i].above.top + 10;
		j++;
	}
	j=0;
	for(i=i;i<TreeNum+WallNum+StatueNum+Rock1Num;i++)
	{
		ObjectMasks[i].below.top =     40 + Dynamic_rock1[j].y;
		ObjectMasks[i].below.left =    5  + Dynamic_rock1[j].x;
		ObjectMasks[i].below.right =   ObjectMasks[i].below.left + 50;
		ObjectMasks[i].below.bottom =  ObjectMasks[i].below.top + 10;

		ObjectMasks[i].above.top =     30 + Dynamic_rock1[j].y;
		ObjectMasks[i].above.left =    5  + Dynamic_rock1[j].x;
		ObjectMasks[i].above.right =   ObjectMasks[i].above.left + 50;
		ObjectMasks[i].above.bottom =  ObjectMasks[i].above.top + 10;
		j++;
	}

	return ObjectMasks;
}
