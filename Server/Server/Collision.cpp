#include "StdAfx.h"
#include "Collision.h"


Collision::Collision()
{
	WorldObj.LoadWorld();
	ArenaObj.LoadArena();

	WorldObjectMasks = WorldObj.CreateObjectMasks();
	ArenaObjectMasks = ArenaObj.CreateObjectMasks();
}

int Collision::RectCollision(RECT r1, RECT r2)
{
	RECT dest;
	return IntersectRect(&dest, &r1, &r2);
}

bool Collision::CheckCollision(int plane, RECT col)
{
	unsigned int i;

	switch( plane )
	{
		case 0:
		{
			for( i=0; i< WorldObj.totalobj; i++ )
			{	
				if( col.bottom > WorldObjectMasks[i].below.bottom )
				{
					if( RectCollision(col,WorldObjectMasks[i].above) )
						return true;
				}
				else
				{
					if( RectCollision(col,WorldObjectMasks[i].below) )
						return true;
				}
			}
		}break;

		default:
		{
			for( i=0; i< ArenaObj.totalobj; i++ )
			{	
				if( col.bottom > ArenaObjectMasks[i].below.bottom )
				{
					if( RectCollision(col,ArenaObjectMasks[i].above) )
						return true;
				}
				else
				{
					if( RectCollision(col,ArenaObjectMasks[i].below) )
						return true;
				}
			}
		}break;
	}

	return false;
}