#ifndef __MATCHES_H__
#define __MATCHES_H__

#include <vector>
#include "Packets.h"
#include "DataConfig.h"

struct Match
{
	bool exist;
	int player1;
	int player2;
	int plane;

	int savep1x;
	int savep1y;
	int savep2x;
	int savep2y;
};

class Matches
{
	
private:
	
	std::vector<int>    joinArray;
	std::vector<Match>  matches;

public:

	void Init();

	int AddQueuePlayer( int );
	bool SearchMatches( int[], ServerPlayer[], int* );
	void Debug();
	
};

#endif /*MATCHES*/