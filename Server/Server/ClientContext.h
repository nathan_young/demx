#ifndef __CLIENTCONTEXT_H__
#define __CLIENTCONTEXT_H__

class ClientContext
{
    SOCKET          m_Socket;  //accepted socket
	bool			login;
    ClientContext   *m_pNext; //this will be a singly linked list

public:
	ClientContext(void);
	~ClientContext(void);

     void SetSocket(SOCKET);
     SOCKET GetSocket();
	 ClientContext* GetNext();
	 void SetNext(ClientContext *);

	 bool isLoggedIn();
	 void setLogin( bool );

};

#endif

