#include "stdafx.h"
#include "ServerApp.h"

uint8_t ServerApp::extractUnsignedInt8(char** msg)
{
    uint8_t* value;
    value = (uint8_t*) *msg;
    *msg += sizeof(uint8_t);
    return *value;
}

uint16_t ServerApp::extractUnsignedInt16(char** msg)
{
    uint16_t* value;
    value = (uint16_t*) *msg;
    *msg += sizeof(uint16_t);
    return *value;
}

int8_t ServerApp::extractInt8(char** msg)
{
    int8_t* value;
    value = (int8_t*) *msg;
    *msg += sizeof(int8_t);
    return *value;
}

int16_t ServerApp::extractInt16(char** msg)
{
    int16_t* value;
    value = (int16_t*) *msg;
    *msg += sizeof(int16_t);
    return *value;
}
