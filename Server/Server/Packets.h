#ifndef _PACKETS_H_
#define _PACKETS_H_

#include <stdint.h>
#include "Timer.h"

struct UpdatePartial
{
	uint8_t id;

	uint16_t x;
	uint16_t y;
	uint8_t plane;
	int8_t xvel;
	int8_t yvel;

	int16_t curhp;
	int16_t hp;

	uint8_t combat;
	uint8_t slowed;
};

struct Update
{
	uint8_t id;

	uint16_t x;
	uint16_t y;
	uint8_t plane;
	int8_t xvel;
	int8_t yvel;

	int16_t curhp;
	int16_t hp;

	uint8_t combat;
	uint8_t slowed;

	char username[10];
};


struct ServerPlayer
{
	//
  	uint8_t id;
	
	char username[10];
	//
	uint16_t x;
	uint16_t y;

	uint8_t plane; // what istance is the player in  -- default = 0
	int8_t xvel;
	int8_t yvel;
	uint8_t status;

	uint8_t pclass;
	//
	int16_t curhp;
	int16_t hp;

	uint8_t combat;

	uint8_t ColdBlood;
	Timer TimerColdBlood;

	uint8_t slowed;
};

struct ServerCreep
{
	//
  	uint8_t id;
	
	char username[10];
	//
	uint16_t x;
	uint16_t y;

	uint8_t plane; // what istance is the player in  -- default = 0
	int8_t xvel;
	int8_t yvel;
	uint8_t status;

	//
	int16_t curhp;
	int16_t hp;

	uint8_t combat;

	uint8_t slowed;
};

struct PortPacket
{
	uint8_t id;
	uint8_t msg;
	std::string text;
	uint16_t damage;
};

struct ServerPacketPartial
{
	uint8_t msg;
	uint8_t numPlayers;
	UpdatePartial playerArray[5];
};

struct ServerPacket
{
	uint8_t msg;
	uint8_t numPlayers;
	Update playerArray[5];
};

struct ServerLogin
{
	uint8_t msg;
	uint8_t id;
	uint8_t pclass;
	char username[10];
};

struct ServerUpdate
{
	uint8_t msg;
	uint8_t id;
	uint16_t damage;
};

struct ServerChat
{
	uint8_t msg;
	uint8_t id;
	char message[20];
};

#endif