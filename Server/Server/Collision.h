//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//>> Copyright (C) 2012 Nathan C. Young
//>> Copyright (C) 2012 Demian Floreani
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#include <Windows.h>
#include "Sprites.h"
#include "Masks.h"

#ifndef __COLLISION_H__
#define __COLLISION_H__

class Collision
{
	Masks *WorldObjectMasks;
	Masks *ArenaObjectMasks;

	SpriteObjects WorldObj;
	SpriteObjects ArenaObj;

public:

	Collision();

	int RectCollision(RECT, RECT);

	bool CheckCollision(int, RECT);
};


#endif //__COLLISION_H__