// Server.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ServerApp.h"

ServerApp server;

DWORD WINAPI RecieveThread( LPVOID lpParameter )
{
	server.RecievePort();
	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{	
	if( server.SetupServer() == 1 )
		return 1;

	unsigned int Counter = 0;
	DWORD ThreadID;
	HANDLE myHandle = CreateThread(0, 0, RecieveThread, &Counter, 0, &ThreadID);

	server.ServerInitGame();
	server.MysqlConnect();

	server.ServerGameLoop();
	
	system("pause");
	return 0;
}

